# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV["PORT"] ||= "9000"

$provision = <<SCRIPT

cd /vagrant # This is where the host folder/repo is mounted


# Add firewall rule to redirect 80 to PORT and save
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port #{ENV["PORT"]}
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
sudo apt-get install iptables-persistent -y

# Add packages to build and run Mastodon
sudo apt-get install \
  git-core \
  gcc \
  postgresql \
  postgresql-contrib \
  libpq-dev \
  -y

# Configure database
sudo -u postgres createuser -U postgres vagrant -s
sudo -u postgres createdb -U postgres kibou_dev

# Configure automatic loading of environment variable
echo 'export $(cat "/vagrant/.env.vagrant" | xargs)' >> ~/.bash_profile

curl https://sh.rustup.rs -sSf > rust.install.sh
chmod u+x ./rust.install.sh
./rust.install.sh -y
rustup install nightly
rustup default nightly
source $HOME/.cargo/env

cargo install diesel_cli --no-default-features --features "postgres"
echo DATABASE_URL=postgres://vagrant:vagrant@localhost/kibou_dev > .env
diesel migration run

sudo sh -c 'echo "192.168.42.42 mastodon.local" >> /etc/hosts'

SCRIPT

$start = <<SCRIPT

echo 'To start server'
echo '  $ vagrant ssh -c "cd /vagrant && cargo run"'

SCRIPT

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/bionic64"

  config.vm.provider :virtualbox do |vb|
    vb.name = "kibou"
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    # Increase the number of CPUs. Uncomment and adjust to
    # increase performance
    # vb.customize ["modifyvm", :id, "--cpus", "3"]

    # Disable VirtualBox DNS proxy to skip long-delay IPv6 resolutions.
    # https://github.com/mitchellh/vagrant/issues/1172
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "off"]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "off"]

    # Use "virtio" network interfaces for better performance.
    vb.customize ["modifyvm", :id, "--nictype1", "virtio"]
    vb.customize ["modifyvm", :id, "--nictype2", "virtio"]

  end

  # This uses the vagrant-hostsupdater plugin, and lets you
  # access the development site at http://mastodon.local.
  # If you change it, also change it in .env.vagrant before provisioning
  # the vagrant server to update the development build.
  #
  # To install:
  #   $ vagrant plugin install vagrant-hostsupdater
  config.vm.hostname = "kibou.local"

  if defined?(VagrantPlugins::HostsUpdater)
    config.vm.network :private_network, ip: "192.168.42.43", nictype: "virtio", virtbox__itnet: true
    config.hostsupdater.remove_on_suspend = false
  end

  # if config.vm.networks.any? { |type, options| type == :private_network }
  #   config.vm.synced_folder ".", "/vagrant", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'actimeo=1']
  # else
  config.vm.synced_folder ".", "/vagrant"
  # end

  config.vm.network :forwarded_port, guest: 9000, host: 9000

  # Full provisioning script, only runs on first 'vagrant up' or with 'vagrant provision'
  config.vm.provision :shell, inline: $provision, privileged: false

  # Start up script, runs on every 'vagrant up'
  config.vm.provision :shell, inline: $start, run: 'always', privileged: false

end
