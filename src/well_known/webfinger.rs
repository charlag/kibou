use crate::models::actor::get_actor_by_acct;
use crate::database;
use serde::{Serialize, Deserialize};
use serde_json::json;
use actix_web::{get, web::{self, Json}};
use anyhow::anyhow;

type JsonResponse = Json<serde_json::Value>;

#[derive(Deserialize)]
pub struct WebfingerQuery {
    resource: String,
}

// FIXME: status code maybe????
#[get("/.well-known/webfinger")]
pub async fn webfinger(query: web::Query<WebfingerQuery>) -> Option<JsonResponse> {
    let resource = query.into_inner().resource;

    let database = database::establish_connection();

    let parsed_resource = if resource.starts_with("acct:") {
        &resource["acct:".len()..]
    } else {
        return Some(Json(json!({"error": "Invalid query"})));
    };

    let json = match get_actor_by_acct(&database, parsed_resource) {
        Ok(actor) => {
            if actor.local {
                let res = json!({
                    "subject": parsed_resource,

                    "links": [
                    {
                        "rel": "self",
                        "type": "application/activity+json",
                        "href": actor.actor_uri
                    }
                    ]
                });
                Some(res)
            } else {
                // json!({"error": "User not found."})
                None
            }
        }
        Err(_) => None,
        // json!({"error": "User not found."}),
    };
    json.map(|j| Json(j))
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebfingerLink {
    pub rel: String,
    #[serde(rename = "type")]
    pub content_type: Option<String>,
    pub href: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebfingerResponse {
    pub subject: String,
    pub links: Vec<WebfingerLink>,
}

pub async fn poke(server: &str, resource: &str) -> Result<WebfingerResponse, anyhow::Error> {
    // why http??
    let mut url = url::Url::parse("http://host/.well-known/webfinger").unwrap();
    url.set_host(Some(server))?;
    url.query_pairs_mut().append_pair("resource", resource);

    debug!("Poking {}", url);
    let mut response = actix_web::client::Client::new()
        .get(url.to_string())
        .send()
        .await
        .map_err(|e| anyhow!("Error sending request: {:?}", e))?;

    if response.status().is_success() {
        let body = response.body().await?;
        let string_body = String::from_utf8(body.to_vec())?;
        let response = serde_json::from_str(&string_body)?;
        Ok(response)
    } else {
        Err(anyhow::anyhow!("Request did not succeed: {}", response.status()))
    }
}

