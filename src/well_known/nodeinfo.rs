use crate::models::{
    activity::count_local_ap_notes,
    actor::count_local_actors
};
use crate::database;
use crate::env;

use actix_web::{get, web::Json};
use serde_json::json;

type JsonReeponse = Json<serde_json::Value>;

#[get("/.well-known/nodeinfo")]
pub async fn nodeinfo() -> JsonReeponse {
    let json = json!({
           "links": [
           {
               "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
               "href": format!("{base_scheme}://{base_domain}/nodeinfo/2.0.json",
               base_scheme = env::get_value("endpoint.base_scheme"),
               base_domain = env::get_value("endpoint.base_domain"))
           },
           ]
    });
    Json(json)
}

// NoteInfo protocol version 2.0 according to the schema at
// https://nodeinfo.diaspora.software/docson/index.html#/ns/schema/2.0
#[get("/nodeinfo/2.0.json")]
pub async fn nodeinfo_v2() -> JsonReeponse {
    let json = json!({
        "version": "2.0",
        "software": {
            "version": format!("{version}-testing",
                version = env!("CARGO_PKG_VERSION")),
            "name": env!("CARGO_PKG_NAME")
        },
        "protocols": [
        "activitypub",
        "litepub"
        ],
        "nodeName": env::get_value("node.name"),
        "nodeDescription": env::get_value("node.description"),
        "nodeContactEmail": env::get_value("node.contact_email"),
        "services":{
            "outbound": [

            ],
            "inbound": [

            ]
        },
        "openRegistrations": get_open_registrations(),
        "usage": {
            "users": {
                "total": get_total_users()
            },
            "localPosts": get_local_posts()
        },
        "metadata": {
            "features": [
            "kibou_api",
            "mastodon_api",
            "webfinger"
        ]
        }
    });
    Json(json)
}

fn get_local_posts() -> usize {
    let database = database::establish_connection();
    count_local_ap_notes(&database).unwrap_or_else(|_| 0)
}

fn get_open_registrations() -> bool {
    env::get_value("node.registrations_enabled")
        .parse::<bool>()
        .unwrap_or_else(|_| false)
}

fn get_total_users() -> usize {
    let database = database::establish_connection();
    count_local_actors(&database).unwrap_or_else(|_| 0)
}
