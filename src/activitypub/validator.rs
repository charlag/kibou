use crate::activitypub::{
    activity::{Activity, Object},
    controller::{object_exists},
    actor::fetch_actor,
};
use crate::models::actor;
use crate::database;
use crate::web::{self, html, http_signatures::Signature};

use regex::Regex;
use url::Url;

pub async fn validate_activity(
    mut activity: serde_json::Value,
    signature: Signature,
) -> Result<serde_json::Value, &'static str> {
    let database = database::establish_connection();
    if activity.get("type").is_none() {
        return Err("Missing type on activity");
    }

    let actor_uri = match activity["actor"].as_str() {
        Some(uri) => uri,
        None => return Err("Missing actor on activity")
    };

    // TODO: we should handle the case of actor deleting themselves as they do
    // not exist remotely anymore
    match fetch_actor(actor_uri).await {
        Ok(actor) => actor,
        Err(_) => return Err("Could not fetch actor")
    };

    let valid_signature = signature.verify(
        &mut actor::get_actor_by_uri(&database, activity["actor"].as_str().unwrap()).unwrap(),
    );
    if !valid_signature {
        return Err("signature verification failed");
    }

    let valid_object = if activity["type"].as_str() == Some("Create") {
        if !object_exists(activity["object"]["id"].as_str().unwrap()) {
            match validate_object(activity["object"].clone(), valid_signature).await {
                Ok(object) => {
                    activity["object"] = object;
                    true
                }
                Err(e) => {
                    return Err(e);
                }
            }
        } else {
            return Err("Create but object exists");
        }
    } else {
        true
    };

    if valid_object {
        Ok(normalize_activity(activity))
    } else {
        Err("Activity could not be validated")
    }
}

pub async fn validate_object(
    object: serde_json::Value,
    valid_signature: bool,
) -> Result<serde_json::Value, &'static str> {
    if object.get("type").is_none() {
        return Err("Missing type on object");
    }

    if !valid_signature {
        let parsed_url = object["id"]
            .as_str()
            .ok_or("Missing id on object")
            .and_then(|id| parse_url(id).map_err(|_| "Invalid object id"))?;
        if !valid_self_reference(&object, &parsed_url).await {
            return Err("Invalid self reference on object");
        }
    };

    if let Some(attributed_to) = object["attributedTo"].as_str() {
        if let Err(_) = fetch_actor(attributed_to).await {
            return Err("AttributedTo actor does not exist")
        }
    }

    normalize_object(object)
}

pub fn validate_actor(actor: serde_json::Value) -> Result<super::actor::Actor, &'static str> {
    let known_type = actor["type"]
        .as_str()
        .map(|it| it == "Person")
        .ok_or("Type missing")?;
    if !known_type {
        return Err("Unknown type for actor");
    }

    actor["id"]
        .as_str()
        .ok_or("Id field is missing")
        .and_then(|id| parse_url(&id).map_err(|_| "Id field is invalid"))?;

    let valid_preferred_username = if actor.get("preferredUsername").is_some() {
        let username_regex = Regex::new(r"^[A-Za-z0-9_]{1,32}$").unwrap();
        username_regex.is_match(actor["preferredUsername"].as_str().unwrap())
    } else {
        false
    };
    if !valid_preferred_username {
        return Err("Invalid preferredUsername");
    }

    let valid_inbox = if actor.get("inbox").is_some() {
        match parse_url(actor["inbox"].as_str().unwrap()) {
            Ok(_) => true,
            Err(_) => false,
        }
    } else {
        false
    };
    if !valid_inbox {
        return Err("Invalid inbox");
    }

    let valid_public_key = if actor.get("publicKey").is_some() {
        match pem::parse(actor["publicKey"]["publicKeyPem"].as_str().unwrap()) {
            Ok(_) => true,
            Err(_) => false,
        }
    } else {
        false
    };
    if !valid_public_key {
        return Err("Invalid public key");
    }

    serde_json::from_value(actor).map_err(|_| "Failed to parse actor")
}

fn normalize_activity(mut activity: serde_json::Value) -> serde_json::Value {
    if activity["to"].is_string() {
        activity["to"] = serde_json::json!(vec![activity["to"].as_str().unwrap().to_string()]);
    }

    if activity["cc"].is_string() {
        activity["cc"] = serde_json::json!(vec![activity["cc"].as_str().unwrap().to_string()]);
    }

    let mut new_activity: Activity;

    if activity.get("cc").is_none() {
        let new_cc_tag: Vec<String> = vec![];
        activity["cc"] = serde_json::json!(new_cc_tag);
    }

    new_activity = serde_json::from_value(activity.clone()).unwrap();
    new_activity.context = None;
    new_activity.to = normalize_public_addressing(new_activity.to);
    new_activity.cc = normalize_public_addressing(new_activity.cc);

    serde_json::to_value(new_activity).unwrap()
}

fn normalize_object(mut object: serde_json::Value) -> Result<serde_json::Value, &'static str> {
    if object["to"].is_string() {
        object["to"] = serde_json::json!(vec![object["to"].as_str().unwrap().to_string()]);
    }

    if object["cc"].is_string() {
        object["cc"] = serde_json::json!(vec![object["cc"].as_str().unwrap().to_string()]);
    }

    let mut new_object: Object;

    if object.get("cc").is_none() {
        let new_cc_tag: Vec<String> = vec![];
        object["cc"] = serde_json::json!(new_cc_tag);
    }

    new_object = serde_json::from_value(object.clone()).map_err(|_| "Failed to parse as object")?;
    new_object.content = html::sanitize(&new_object.content);
    new_object.context = None;
    new_object.to = normalize_public_addressing(new_object.to);
    new_object.cc = normalize_public_addressing(new_object.cc);

    Ok(serde_json::to_value(new_object).unwrap())
}

fn normalize_public_addressing(mut collection: Vec<String>) -> Vec<String> {
    let alternative_public_address = vec![
        "https://www.w3.org/ns/activitystreams",
        "Public",
        "as:Public",
    ];

    for address in alternative_public_address {
        if collection.contains(&address.to_string()) {
            let index = collection
                .iter()
                .position(|receipient| receipient == &address.to_string())
                .unwrap();
            collection[index] = String::from("https://www.w3.org/ns/activitystreams#Public");
        }
    }

    return collection;
}

fn parse_url(url: &str) -> Result<String, url::ParseError> {
    let mut parsed_url = String::new();
    let stripped_characters = "\"";
    for character in url.chars() {
        if !stripped_characters.contains(character) {
            parsed_url.push(character);
        }
    }

    match Url::parse(&parsed_url) {
        Ok(remote_url) => Ok(remote_url.to_string()),
        Err(e) => Err(e),
    }
}

async fn valid_self_reference(object: &serde_json::Value, url: &str) -> bool {
    match web::fetch_remote_object(url).await {
        Ok(remote_object) => {
            let json_object: serde_json::Value = serde_json::from_str(&remote_object).unwrap();

            &json_object == object
        }
        Err(_) => false,
    }
}
