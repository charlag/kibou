use crate::activitypub::{
    activity::{self as ap_activity, Activity as ApActivity},
    actor::{self as ap_actor, Actor as ApActor},
    controller, ActivitystreamsResponse, APResponse,
};
use crate::database::PooledConnection;
use crate::web::http_signatures::Signature;

use actix_web::{
    dev::RequestHead,
    get,
    http::StatusCode,
    post,
    web::{Json, Path, Query},
    Responder,
    Either,
};
use serde::Deserialize;
use serde_json::json;

#[derive(Deserialize)]
pub struct Id(String);

impl Id {
    fn into_inner(self) -> String {
        self.0
    }
}

// POST requests (eg. to the inbox) MUST be made with a Content-Type of application/ld+json;
// profile="https://www.w3.org/ns/activitystreams" and GET requests (see also 3.2 Retrieving
// objects) with an Accept header of application/ld+json; profile="https://www.w3.org/ns/activitystreams".
// Servers SHOULD interpret a Content-Type or Accept header of application/activity+json as
// equivalent to application/ld+json; profile="https://www.w3.org/ns/activitystreams" for
// server-to-server interactions.
//
// https://www.w3.org/TR/activitypub/#server-to-server-interactions
const APPLICATION_LD_JSON_HEADER: &str = "application/ld+json";
const APPLICATION_ACTIVITY_JSON_HEADER: &str = "appliation/activity+json";

fn is_ap_header(h: &str) -> bool {
    h.contains(APPLICATION_LD_JSON_HEADER) || h.contains(APPLICATION_ACTIVITY_JSON_HEADER)
}

fn ap_content_type_guard(req: &RequestHead) -> bool {
    let header_name = if req.method == actix_web::http::Method::GET {
        "Accept"
    } else {
        "Content-Type"
    };

    req.headers()
        .get(header_name)
        .and_then(|h| h.to_str().ok())
        .map(is_ap_header)
        .unwrap_or(false)
}

#[get("/activities/{id}", guard = "ap_content_type_guard")]
pub async fn activity(
    db: PooledConnection,
    id: Path<Id>,
) -> Option<ActivitystreamsResponse<ApActivity>> {
    ap_activity::get_activity(&db, &id.into_inner().0).map(ActivitystreamsResponse)
}

#[get("/actors/{handle}", guard = "ap_content_type_guard")]
pub async fn actor(
    db: PooledConnection,
    handle: Path<Id>,
) -> Option<ActivitystreamsResponse<ApActor>> {
    ap_actor::get_actor_by_preferred(&db, &handle.into_inner().0).map(ActivitystreamsResponse)
}

#[post("/actors/{id}/inbox")]
pub async fn actor_inbox(
    signature: Signature,
    _id: Path<Id>,
    payload: Json<serde_json::Value>,
) -> impl Responder {
    controller::prepare_incoming(payload.into_inner(), signature).await;
    (Json(json!({"message": "accepted"})), StatusCode::ACCEPTED)
}

#[post("/inbox")]
pub async fn inbox(signature: Signature, payload: Json<serde_json::Value>) -> impl Responder {
    controller::prepare_incoming(payload.into_inner(), signature).await;
    (Json(json!({"message": "accepted"})), StatusCode::ACCEPTED)
}

#[get("/actors/{id}/outbox")]
pub async fn actor_outobox(id: Path<Id>) -> APResponse<serde_json::Value> {
    controller::outbox(id.into_inner().0)
}

#[get("/objects/{id}", guard = "ap_content_type_guard")]
pub async fn get_object(
    db: PooledConnection,
    id: Path<Id>,
) -> Option<ActivitystreamsResponse<serde_json::Value>> {
    ap_activity::get_object(&db, &id.into_inner().0).map(ActivitystreamsResponse)
}

#[derive(Deserialize)]
pub struct PagedQuery {
    paged: Option<u64>,
}

#[get("/actors/{id}/following")]
pub async fn following(id: Path<Id>, query: Query<PagedQuery>) -> impl Responder {
    match query.paged {
        Some(_) => Either::A(controller::following_page(id.into_inner().into_inner())),
        None => Either::B(controller::following(id.into_inner().into_inner()))
    }
}

#[get("/actors/{id}/followers")]
pub async fn followers(id: Path<Id>, query: Query<PagedQuery>) -> impl Responder {
    match query.paged {
        Some(_) => Either::A(controller::followers_page(id.into_inner().into_inner())),
        None => Either::B(controller::followers(id.into_inner().into_inner()))
    }
}
