use crate::models::activity;
use crate::database;
use crate::env;
use serde::{Deserialize, Serialize};
use serde_json::{self, json};

// ActivityStreams2/AcitivityPub properties are expressed in CamelCase

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct Activity {
    // Properties according to
    // - https://www.w3.org/TR/activitystreams-core/#activities
    #[serde(rename = "@context", skip_serializing_if = "Option::is_none")]
    pub context: Option<serde_json::Value>,
    #[serde(rename = "type")]
    pub _type: String,
    pub id: String,
    pub actor: String,
    pub object: serde_json::Value,
    pub published: Option<String>,
    #[serde(default)]
    pub to: Vec<String>,
    #[serde(default)]
    pub cc: Vec<String>,
}

impl Into<serde_json::Value> for Activity {
    fn into(self) -> serde_json::Value {
        serde_json::to_value(self).unwrap()
    }
}

// This is not loose enough for a generic object currently
#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct Object {
    #[serde(rename = "@context", skip_serializing_if = "Option::is_none")]
    pub context: Option<serde_json::Value>,
    #[serde(rename = "type")]
    pub _type: String,
    pub id: String,
    pub published: String,
    pub attributedTo: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inReplyTo: Option<String>,
    pub summary: Option<String>,
    pub content: String,
    pub to: Vec<String>,
    pub cc: Vec<String>,
    pub tag: Option<Vec<Tag>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attachment: Option<Vec<serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sensitive: Option<bool>,
    #[serde(default)]
    pub like_count: i64,
    #[serde(default)]
    pub announce_count: i64,
    #[serde(default)]
    pub reply_count: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Tag {
    #[serde(rename = "type")]
    pub _type: String,
    pub href: String,
    pub name: String,
}

pub fn get_activity_json_by_id(id: &str) -> serde_json::Value {
    let database = database::establish_connection();
    let activity_id = format!(
        "{}://{}/activities/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        id
    );

    match activity::get_ap_activity_by_id(&database, &activity_id) {
        Ok(activity) => json!(serialize_from_internal_activity(activity).object),
        Err(_) => json!({"error": "Object not found."}),
    }
}

pub fn get_activity(db: &database::PooledConnection, id: &str) -> Option<Activity> {
    let activity_id = format!(
        "{}://{}/activities/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        id
    );

    activity::get_ap_activity_by_id(&db, &activity_id)
        .map(serialize_from_internal_activity)
        .ok()
}

pub fn get_object_json_by_id(id: &str) -> serde_json::Value {
    let database = database::establish_connection();
    let object_id = format!(
        "{}://{}/objects/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        id
    );

    match activity::get_ap_object_by_id(&database, &object_id) {
        Ok(activity) => json!(serialize_from_internal_activity(activity).object),
        Err(_) => json!({"error": "Object not found."}),
    }
}

pub fn get_object(db: &database::PooledConnection, id: &str) -> Option<serde_json::Value> {
    let object_id = format!(
        "{}://{}/objects/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        id
    );

    activity::get_ap_object_by_id(db, &object_id)
        .map(|a| serialize_from_internal_activity(a).object)
        .ok()
}

pub fn serialize_from_internal_activity(activity: activity::Activity) -> Activity {
    return serde_json::from_value(activity.data).unwrap();
}

impl From<activity::Activity> for Activity {
    fn from(activity: activity::Activity) -> Self {
        serialize_from_internal_activity(activity)
    }
}

pub fn create_internal_activity(
    json_activity: &serde_json::Value,
    actor_uri: &str,
) -> activity::Activity {
    let created = match json_activity["published"].as_str() {
        Some(published_string) => {
            // Technically it can be ISO 8601 but we hope that it is just subset of it which is RFC3338
            let created_datetime = chrono::DateTime::parse_from_rfc3339(&published_string).expect(
                &format!("Could not parse published date {}", published_string),
            );
            chrono::NaiveDateTime::from_timestamp(
                created_datetime.timestamp(),
                // Dates are usually sent with second resolution so we have a higher chance
                // of collision. To get proper subsec resolution we use current time. It should
                // not affect ordering much but will prevent collisions.
                chrono::Utc::now().naive_utc().timestamp_subsec_nanos(),
            )
        }
        None => chrono::Utc::now().naive_utc(),
    };

    activity::Activity {
        id: 0,
        data: json_activity.clone().to_owned(),
        actor: actor_uri.to_string(),
        created,
    }
}
