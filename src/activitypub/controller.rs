use super::{
    activity::{create_internal_activity, Activity, Object, Tag},
    actor::{add_follow, create_internal_actor, remove_follow, Actor},
    validator, APError, APResponse, ActivitystreamsResponse, Attachment,
};
use crate::database::{self, PooledConnection};
use crate::env;
use crate::models::{
    activity::{
        self, decrease_reply, delete_ap_object_by_id, get_ap_activity_by_id,
        get_ap_activity_by_object_id, insert_activity, count_ap_notes_for_actor,
    },
    actor::{
        self, count_actor_followees, create_actor, get_actor_by_uri, get_actor_followees,
        get_local_actor_by_preferred_username, is_actor_followed_by,
    },
    notification::{self, Notification},
};
use crate::web::{self, http_signatures::Signature};

use std::collections::HashSet;
use std::fmt;

use chrono::Utc;
use diesel::Connection;
use serde_json::{json, Value as JsonValue};
use url::Url;
use uuid::Uuid;
use std::borrow::Borrow;
use async_recursion;

/// Creates a new `Accept` activity, inserts it into the database and returns the newly created activity
///
/// # Parameters
///
/// * `actor`  -              &str | Reference to an ActivityPub actor
/// * `object` -              &str | Reference to an ActivityStreams object
/// * `to`     -       Vec<String> | A vector of strings that provides direct receipients
/// * `cc`     -       Vec<String> | A vector of strings that provides passive receipients
///
pub fn accept(
    db: &PooledConnection,
    actor: &str,
    object: &str,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    activity_build(db, "Accept", actor, serde_json::json!(object), to, cc)
}

/// Determines whether an ActivityPub actor exists in the database
///
/// # Parameters
///
/// * `actor_id` - &str | Reference to an ActivityPub actor
///
/// # Tests
///
/// Tests for this function are in `tests/activitypub_controller.rs`
/// - actor_exists()
/// - err_actor_exists()
pub fn actor_exists(actor_id: &str) -> bool {
    // TODO: should also accept DB connections
    let database = database::establish_connection();

    get_actor_by_uri(&database, actor_id).is_ok()
}

/// Creates a new `Announce` activity, inserts it into the database and returns the newly created activity
///
/// # Parameters
///
/// * `actor`  -        &str | Reference to an ActivityPub actor
/// * `object` -        &str | Reference to an ActivityStreams object
/// * `to`     - Vec<String> | A vector of strings that provides direct receipients
/// * `cc`     - Vec<String> | A vector of strings that provides passive receipients
///
pub fn announce(
    db: &PooledConnection,
    actor: &str,
    object: &str,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    db.transaction::<_, diesel::result::Error, _>(|| {
        let activity = activity_build(db, "Announce", actor, serde_json::json!(object), to, cc);
        activity::increase_announce(db, object)?;
        Ok(activity)
    })
        .unwrap()
}

/// Creates a new `Create` activity, inserts it into the database and returns the newly created activity
///
/// # Parameters
///
/// * `actor`  -            &str | Reference to an ActivityPub actor
/// * `object` - serde_json::Value | An ActivityStreams object serialized in JSON
/// * `to`     -       Vec<String> | A vector of strings that provides direct receipients
/// * `cc`     -       Vec<String> | A vector of strings that provides passive receipients
///
pub fn create(
    db: &PooledConnection,
    actor: &str,
    object: serde_json::Value,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    activity_build(db, "Create", actor, object, to, cc)
}

/// Creates a new `Follow` activity, inserts it into the database and returns the newly created activity
///
/// # Parameters
///
/// * `actor`  - &str | Reference to an ActivityPub actor
/// * `object` - &str | Reference to an ActivityStreams object
///
pub fn follow(db: &PooledConnection, actor: &str, object: &str) -> Activity {
    activity_build(
        db,
        "Follow",
        actor,
        serde_json::json!(object),
        vec![object.to_string()],
        vec![],
    )
}

/// Creates a new `Like` activity, inserts it into the database and returns the newly created activity
///
/// # Parameters
///
/// * `actor`  -        &str | Reference to an ActivityPub actor
/// * `object` -        &str | Reference to an ActivityStreams object
/// * `to`     - Vec<String> | A vector of strings that provides direct receipients
/// * `cc`     - Vec<String> | A vector of strings that provides passive receipients
///
pub fn like(
    db: &PooledConnection,
    actor: &str,
    object: &str,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    db.transaction::<_, diesel::result::Error, _>(|| {
        // TODO: probably in transaction
        let activity = activity_build(db, "Like", actor, serde_json::json!(object), to, cc);
        activity::increase_like(db, object)?;
        Ok(activity)
    })
        .unwrap()
}

pub fn undo(
    db: &PooledConnection,
    actor: &str,
    object: serde_json::Value,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    activity_build(db, "Undo", actor, object, to, cc)
}

pub fn undo_like(db: &PooledConnection, actor: &str, reaction_activity: Activity) -> Activity {
    db.transaction::<_, diesel::result::Error, _>(|| {
        let orig_activity_json = serde_json::to_value(&reaction_activity).unwrap();
        // We don't actually need them on like undo
        let to = vec![ACTIVITY_STREAMS_PUBLIC.to_string()];
        let cc = vec![];
        let undo_activity = activity_build(db, "Undo", actor, orig_activity_json, to, cc);
        activity::delete_ap_activity_by_id(db, &reaction_activity.id)?;
        activity::decrease_like(db, reaction_activity.object.as_str().unwrap())?;
        Ok(undo_activity)
    })
        .unwrap()
}

pub fn undo_announce(db: &PooledConnection, actor: &str, reaction_activity: Activity) -> Activity {
    db.transaction::<_, diesel::result::Error, _>(|| {
        let orig_activity_json = serde_json::to_value(&reaction_activity).unwrap();
        // We don't actually need them on like undo
        let to = vec![ACTIVITY_STREAMS_PUBLIC.to_string()];
        let cc = vec![];
        let undo_activity = activity_build(db, "Undo", actor, orig_activity_json, to, cc);
        activity::delete_ap_activity_by_id(db, &reaction_activity.id)?;
        activity::decrease_announce(db, reaction_activity.object.as_str().unwrap())?;
        Ok(undo_activity)
    })
        .unwrap()
}

pub fn delete(db: &PooledConnection, actor: &actor::Actor, to_delete: &Activity) -> Activity {
    // TODO: decrease reply count here once we add it
    activity::delete_ap_activity_by_id(db, &to_delete.id).unwrap();
    let object_id = value_or_id(&to_delete.object);
    // We are not doing Undo here (for whatever reason), we "Delete" an *object*.
    let tombstone = json!({
        "type": "Tombstone",
        "id": object_id,
    });

    activity_build(
        db,
        "Delete",
        &actor.actor_uri,
        tombstone,
        vec![ACTIVITY_STREAMS_PUBLIC.into()],
        vec![],
    )
}

pub fn retain_remote_actors(recipients: &mut Vec<String>, host: &str) {
    recipients.retain(|r| {
        let recipient_host = url::Url::parse(r)
            .ok()
            .as_ref()
            .and_then(|r| r.host())
            .map(|h| h.to_string());
        recipient_host != None && recipient_host.unwrap() != host
    });
}

/// Returns a new ActivityStreams object of the type `Note`
///
/// # Parameters
///
/// * `actor`    -                   &str | Reference to an ActivityPub actor
/// * `reply_to` -         Option<String> | An optional reference to another ActivityStreams object this object is a reply to
/// * `content`  -                 String | The content of this note
/// * `to`       -            Vec<String> | A vector of strings that provides direct receipients
/// * `cc`       -            Vec<String> | A vector of strings that provides passive receipients
/// * `tag`      - Vec<serde_json::Value> | A vector of tags to ActivityStreams objects wrapped in JSON
///
pub fn note(
    actor: &str,
    reply_to: Option<String>,
    content: String,
    to: Vec<String>,
    cc: Vec<String>,
    tag: Vec<Tag>,
    attachments: Vec<Attachment>,
    summary: Option<String>,
) -> Object {
    Object {
        context: Some(serde_json::json!(vec![
            String::from("https://www.w3.org/ns/activitystreams"),
            String::from("https://w3id.org/security/v1"),
        ])),
        _type: String::from("Note"),
        id: format!(
            "{base_scheme}://{base_domain}/objects/{uuid}",
            base_scheme = env::get_value("endpoint.base_scheme"),
            base_domain = env::get_value("endpoint.base_domain"),
            uuid = Uuid::new_v4()
        ),
        attributedTo: actor.to_string(),
        inReplyTo: reply_to,
        summary,
        content: content,
        published: Utc::now().to_rfc3339().to_string(),
        to: to,
        cc: cc,
        tag: Some(tag),
        attachment: Some(
            attachments
                .iter()
                .map(|a| serde_json::to_value(a).expect("Failed to serialize attachment {:?}"))
                .collect(),
        ),
        sensitive: Some(false),
        like_count: 0,
        announce_count: 0,
        reply_count: 0,
    }
}

pub fn profile_update(db: &PooledConnection, actor: &crate::models::actor::Actor) -> Activity {
    let to = vec![ACTIVITY_STREAMS_PUBLIC.to_string()];
    let object = serde_json::to_value(super::actor::serialize_from_internal_actor(actor))
        .expect("Failed to serialize actor");
    activity_build(db, "Update", &actor.actor_uri, object, to, vec![])
}

/// Trys to fetch a remote object based on the ActivityStreams id
///
/// # Description
///
/// If the URL was successfully parsed, this function will try to fetch the remote object and
/// determine whether it's a known and valid ActivityStreams object or ActivityPub actor.
///
/// # Parameters
///
/// * `url` - String | Link to an ActivityStreams object
///
#[async_recursion(?Send)]
pub async fn fetch_object_by_id(url: String) {
    match do_fetch_object_if_needed(&url).await {
        Ok(_) => {}
        Err(e) => error!("Error while fetching object: {} {}", url, e)
    }
}

async fn do_fetch_object_if_needed(url: &str) -> Result<(), anyhow::Error> {
    let mut sanitized_url = String::new();
    let stripped_characters = "\"";
    for character in url.chars() {
        if !stripped_characters.contains(character) {
            sanitized_url.push(character);
        }
    }
    let remote_url = Url::parse(&sanitized_url)?;

    if object_exists(&remote_url.to_string()) || actor_exists(&remote_url.to_string()) {
        return Ok({});
    }

    debug!("Trying to fetch document: {}", &url);

    let object = web::fetch_remote_object(&remote_url.to_string()).await?;

    let parsed_object: serde_json::Value =
        serde_json::from_str(&object).unwrap();

    let as2_object = validator::validate_object(parsed_object.clone(), false).await
        .map_err(|e_str| ProcessingError::validation(e_str))?;

    handle_object(as2_object).await;
    debug!("Successfully fetched object: {}", &url);

    let as2_actor = validator::validate_actor(parsed_object.clone())
        .map_err(|e_str| ProcessingError::validation(e_str))?
        ;
    handle_actor(as2_actor);
    debug!("Successfully fetched actor: {}", &url);
    Ok(())
}

async fn fetch_remote_object(url: &str) -> Result<JsonValue, ProcessingError> {
    let response = web::fetch_remote_object(url)
        .await
        .map_err(|e| {
            let stderr: Box<dyn std::error::Error> = e.into();
            ProcessingError::wrap::<dyn std::error::Error, _>(stderr)
        })?;
    serde_json::from_str(&response).map_err(|_| {
        ProcessingError::validation(format!("Parsing remote object failed: {}", response))
    })
}

/// Determines whether an ActivityStreams object exists in the database
///
/// # Parameters
///
/// * `object_id` - &str | Reference to an ActivityStreams object
///
/// # Tests
///
/// Tests for this function are in `tests/activitypub_controller.rs`
/// - object_exists()
/// - err_object_exists()
pub fn object_exists(object_id: &str) -> bool {
    let database = database::establish_connection();

    get_ap_activity_by_object_id(&database, object_id, "Create").is_ok()
}

/// Handles incoming requests of the inbox
///
/// # Parameters
///
/// * `activity`  - serde_json::Value           | An ActivityStreams activity serialized in JSON
/// * `signature` - activitiypub::HTTPSignature | The activity's signature, signed by an actor
///
/// # Tests
///
/// [TODO]
pub async fn prepare_incoming(activity: serde_json::Value, signature: Signature) {
    println!("Incoming activity: {}", activity);
    match validator::validate_activity(activity.clone(), signature).await {
        Ok(sanitized_activity) => match handle_activity(sanitized_activity).await {
            Ok(()) => (),
            Err(e) => error!("Error during processing incoming AP activity: {}", e),
        },
        Err(e) => error!("Validation failed for activity: {} {:?}", e, activity),
    }
}

fn activity_build(
    db: &PooledConnection,
    _type: &str,
    actor: &str,
    object: serde_json::Value,
    to: Vec<String>,
    cc: Vec<String>,
) -> Activity {
    let new_activity = Activity {
        context: Some(serde_json::json!(vec![
            String::from("https://www.w3.org/ns/activitystreams"),
            String::from("https://w3id.org/security/v1"),
        ])),
        _type: _type.to_string(),
        id: format!(
            "{base_scheme}://{base_domain}/activities/{uuid}",
            base_scheme = env::get_value("endpoint.base_scheme"),
            base_domain = env::get_value("endpoint.base_domain"),
            uuid = Uuid::new_v4()
        ),
        actor: actor.to_string(),
        object: object,
        published: Some(Utc::now().to_rfc3339().to_string()),
        to: to,
        cc: cc,
    };

    insert_activity(
        &db,
        create_internal_activity(&serde_json::json!(&new_activity), &new_activity.actor),
    );
    new_activity
}

/// Handles a newly fetched object and wraps it into it's own internal `Create` activity
///
/// # Parameters
///
/// * `object` - serde_json::Value | An ActivityStreams object serialized in JSON
///
/// # Tests
///
/// [TODO]
async fn handle_object(object: serde_json::Value) {
    let serialized_object: Object = serde_json::from_value(object.clone()).unwrap();

    if !serialized_object.inReplyTo.is_none() {
        if !object_exists(&serialized_object.inReplyTo.clone().unwrap()) {
            fetch_object_by_id(serialized_object.inReplyTo.unwrap().to_string()).await;
        }
    }

    if !object_exists(&serialized_object.id) {
        // Wrapping new object in an activity, as raw objects don't get stored
        let _activity = create(
            &database::establish_connection(),
            &serialized_object.attributedTo,
            object,
            serialized_object.to,
            serialized_object.cc,
        );
    }
}

/// Handles a newly fetched actor
///
/// # Parameters
///
/// * `actor` - serde_json::Value | An ActivityPub actor serialized in JSON
///
/// # Tests
///
/// [TODO]
fn handle_actor(actor: Actor) {
    let database = database::establish_connection();

    create_actor(&database, &mut create_internal_actor(actor), true);
}

/// Final handling of incoming ActivityStreams activities which have already been validated
///
/// # Parameters
///
/// * `activity` - serde_json::Value | An ActivityStreams activity serialized in JSON
///
/// # Tests
///
/// [TODO]
async fn handle_activity(activity: serde_json::Value) -> ProcessResult {
    let database = database::establish_connection();
    let actor = activity["actor"].as_str().unwrap().to_string();

    let activity_type = activity["type"]
        .as_str()
        .ok_or_else(|| ProcessingError::validation("Missing type on activtiy"))?;
    match activity_type {
        "Accept" => process_accept(database, &actor, activity),
        "Announce" => process_announce(database, &actor, activity).await,
        "Create" => process_create(database, &actor, activity).await,
        "Follow" => process_follow(&database, &actor, activity),
        "Like" => process_like(database, &actor, activity),
        "Undo" => process_undo(database, &actor, activity),
        "Delete" => process_delete(database, &actor, activity),
        "Update" => process_update(database, &actor, activity).await,
        unknown_activity_type => {
            println!("Unknown activity type: {}", unknown_activity_type);
            Ok(())
        }
    }
}

type ProcessResult = Result<(), ProcessingError>;

#[derive(Debug)]
enum ProcessingError {
    Validation(String),
    NotAuthorized,
    Internal(String),
}

impl ProcessingError {
    fn wrap<T: std::error::Error + ?Sized, R: Borrow<T>>(err: R) -> ProcessingError {
        ProcessingError::Internal(format!("{}", err.borrow()))
    }

    fn validation<T: Into<String>>(msg: T) -> ProcessingError {
        ProcessingError::Validation(msg.into())
    }
}

impl fmt::Display for ProcessingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ProcessingError::Validation(msg) => write!(f, "Validation: {}", msg),
            ProcessingError::NotAuthorized => write!(f, "Not authorized"),
            ProcessingError::Internal(msg) => write!(f, "Internal: {}", msg),
        }
    }
}

impl std::error::Error for ProcessingError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

fn process_accept(
    database: database::PooledConnection,
    actor: &str,
    activity: serde_json::Value,
) -> ProcessResult {
    let mut activity_id: &str = "";

    if activity["object"].is_string() {
        activity_id = activity["object"].as_str().unwrap();
    } else if activity["object"].is_object() {
        activity_id = activity["object"]["id"].as_str().unwrap();
    }

    match get_ap_activity_by_id(&database, activity_id) {
        Ok(original_activity) => match original_activity.data["type"].as_str().unwrap() {
            "Follow" => {
                let sender = original_activity.data["actor"].as_str().unwrap();
                let receipient_actor = get_actor_by_uri(
                    &database,
                    original_activity.data["object"].as_str().unwrap(),
                )
                    .unwrap();

                match is_actor_followed_by(&database, &receipient_actor, sender) {
                    Ok(false) => add_follow(&receipient_actor.actor_uri, sender, activity_id),
                    Ok(true) => (),
                    Err(e) => eprintln!("{}", e),
                }

                insert_activity(&database, create_internal_activity(&activity, &actor));
            }
            &_ => (),
        },
        Err(e) => eprintln!("Unknown object mentioned in `Accept` activity {}", e),
    }

    insert_activity(&database, create_internal_activity(&activity, &actor));
    Ok(())
}

async fn process_announce(
    database: database::PooledConnection,
    actor: &str,
    activity: serde_json::Value,
) -> ProcessResult {
    let object_id = activity["object"].as_str().unwrap().to_string();
    let id = insert_activity(&database, create_internal_activity(&activity, &actor)).id;

    match get_ap_activity_by_object_id(&database, &object_id, "Create") {
        Ok(local_activity) => {
            activity::increase_announce(&database, &object_id).unwrap();
            let actor = get_actor_by_uri(&database, &local_activity.actor)
                .map_err(ProcessingError::wrap)?;
            if actor.local {
                let notification = Notification::new(id, actor.id);
                notification::insert(&database, notification);
            }
        }
        Err(diesel::result::Error::NotFound) => {
            println!("Announced status not found: {:?}", object_id);
            // fire and forget for not
            fetch_object_by_id(object_id).await;
            // If it's remove, we don't need to create notification.
        }
        Err(e) => {
            eprintln!("Failed to load announced activity: {:?}", e);
            return Err(ProcessingError::wrap(e));
        }
    };

    Ok(())
}

async fn process_create(
    database: database::PooledConnection,
    actor: &str,
    activity: serde_json::Value,
) -> ProcessResult {
    if activity["object"]["type"] == "Note" {
        let id = insert_activity(&database, create_internal_activity(&activity, &actor)).id;
        println!("Inserted note");
        // Here we should check "to" which is what is supposed to be used for
        // notifications but Mastodon ignores this completely and just uses
        // Mentions (which should not generate notifications by themselves.
        let mut actors = HashSet::new();
        if let Some(tags) = activity["object"]["tag"].as_array() {
            for tag in tags {
                if tag["type"].as_str() == Some("Mention") {
                    if let Some(href) = tag["href"].as_str() {
                        actors.insert(href);
                    }
                }
            }
        }
        if let Some(to) = activity["to"].as_array() {
            // support only URLs for now, not objects
            for to_uri in to
                .iter()
                .filter_map(|t| t.as_str())
                .filter(|t| t != &ACTIVITY_STREAMS_PUBLIC)
            {
                actors.insert(to_uri);
            }
        }
        println!("Collected mentioned actors: {:?}", actors);
        for actor in actors {
            match get_actor_by_uri(&database, actor) {
                Ok(actor) if actor.local => {
                    let notification = Notification::new(id, actor.id);
                    notification::insert(&database, notification);
                }
                _ => (),
            };
        }

        if let Some(reply_id) = activity["object"]["inReplyTo"].as_str() {
            match get_ap_activity_by_object_id(&database, &reply_id, "Create") {
                Ok(activity) => {
                    activity::increase_reply(&database, extract_object_id(&activity.data).unwrap())
                        .unwrap();
                }
                Err(_) => {
                    let reply_id = reply_id.to_string();
                    // fire and forget for now
                    fetch_object_by_id(reply_id).await;
                }
            }
        }
    }
    Ok(())
}

fn process_follow(
    database: &database::PooledConnection,
    actor: &str,
    activity: serde_json::Value,
) -> ProcessResult {
    let remote_account = get_actor_by_uri(&database, &actor).unwrap();

    let id = insert_activity(&database, create_internal_activity(&activity, &actor)).id;

    let actor = get_actor_by_uri(&database, activity["object"].as_str().unwrap())
        .map_err(ProcessingError::wrap)?;
    if actor.local {
        let notification = Notification::new(id, actor.id);
        notification::insert(&database, notification);
        let is_followed_by = is_actor_followed_by(&database, &actor, &remote_account.actor_uri)
            .map_err(ProcessingError::wrap)?;
        if is_followed_by {
            let accept_activity = serde_json::to_value(accept(
                database,
                &actor.actor_uri,
                activity["id"].as_str().unwrap(),
                vec![remote_account.actor_uri],
                vec![],
            ))
                .unwrap();
            web::federator::enqueue(&actor, accept_activity, vec![remote_account.inbox.unwrap()]);
        } else {
            // *Note*
            //
            // Kibou should still send a `Accept` activity even if one was already sent, in
            // case the original `Accept` activity did not reach the remote server.
            let accept_activity = serde_json::to_value(accept(
                database,
                &actor.actor_uri,
                activity["id"].as_str().unwrap(),
                vec![remote_account.actor_uri.clone()],
                vec![],
            ))
                .unwrap();

            add_follow(
                &actor.actor_uri,
                &remote_account.actor_uri,
                activity["id"].as_str().unwrap(),
            );
            web::federator::enqueue(&actor, accept_activity, vec![remote_account.inbox.unwrap()]);
        }
    }
    Ok(())
}

fn process_like(db: PooledConnection, actor: &str, activity: serde_json::Value) -> ProcessResult {
    let object_id = activity["object"]
        .as_str()
        .ok_or_else(|| ProcessingError::Validation("No object on like activity".into()))?
        .to_string();
    let id = insert_activity(&db, create_internal_activity(&activity, &actor)).id;
    let reacted_activity = get_ap_activity_by_object_id(&db, &object_id, "Create");

    match reacted_activity {
        Ok(reacted_activity) => {
            activity::increase_like(&db, &object_id).unwrap();
            match get_actor_by_uri(&db, &reacted_activity.actor) {
                Ok(actor) if actor.local => {
                    let notification = Notification::new(id, actor.id);
                    notification::insert(&db, notification);
                }
                _ => (),
            };
        }
        Err(_) => {
            eprintln!("Like for the object but failed to load it: {}", &object_id);
            // If we don't know about the object, we don't care about it either.
        }
    }

    actix_rt::spawn(fetch_object_by_id(object_id));

    Ok(())
}

fn process_undo(
    database: database::PooledConnection,
    actor: &str,
    activity: serde_json::Value,
) -> ProcessResult {
    let remote_account = get_actor_by_uri(&database, &actor).map_err(|e| {
        ProcessingError::Internal(format!("Could not load remove account: {:?}", e))
    })?;
    let object_id = extract_object_id(&activity)
        .ok_or_else(|| ProcessingError::Validation("On object id on undo activity".into()))?;

    // "object" in undo case is an activity
    let activity_to_undo =
        get_ap_activity_by_id(&database, object_id).map_err(ProcessingError::wrap)?;
    let undo_type = activity_to_undo.data["type"]
        .as_str()
        .expect("On type on saved object");
    match undo_type {
        "Follow" => {
            let actor_uri = activity_to_undo.data["object"]
                .as_str()
                .expect("No actor on saved object");
            let account =
                get_actor_by_uri(&database, actor_uri).expect("No actor referenced by object");

            let followed_by =
                is_actor_followed_by(&database, &account, &actor).map_err(ProcessingError::wrap)?;
            if followed_by {
                remove_follow(&account.actor_uri, &remote_account.actor_uri)
            }
            insert_activity(&database, create_internal_activity(&activity, &actor));
        }
        "Like" | "Announce" => {
            // activity_to_undo was the reaction activity.
            // Watch my hands:
            // create <--(object)-- like/announce <--(object)-- undo
            // reacted_activity     activity_to_undo            activity
            let reacted_activity_uri = extract_object_id(&activity_to_undo.data).unwrap();
            println!("Undo like for {:?}", reacted_activity_uri);
            let reacted_activity =
                match get_ap_activity_by_object_id(&database, reacted_activity_uri, "Create") {
                    Ok(reacted_activity) => reacted_activity,
                    Err(diesel::result::Error::NotFound) => return Ok(()),
                    Err(e) => panic!("{:?}", e),
                };
            // TODO: delete notification
            // TODO: this looks very similar to what we do when we perform activity
            database
                .transaction::<(), diesel::result::Error, _>(|| {
                    let reacted_object_id = extract_object_id(&reacted_activity.data).unwrap();
                    decreate_count(&database, &undo_type, reacted_object_id);

                    activity::delete_ap_activity_by_id(&database, object_id)?;
                    insert_activity(&database, create_internal_activity(&activity, actor));
                    Ok(())
                })
                .unwrap();
        }
        // TODO: implement more types, like boost and like
        &_ => (),
    }
    Ok(())
}

fn decreate_count(db: &PooledConnection, reaction_type: &str, object_id: &str) {
    match reaction_type {
        "Like" => activity::decrease_like(db, object_id).unwrap(),
        "Announce" => activity::decrease_announce(db, object_id).unwrap(),
        // Now that's why you use strong typing and not strings kids
        _ => panic!("Unknown reacteion type to decrease: {:?}", reaction_type),
    }
}

fn process_delete(
    database: database::PooledConnection,
    actor: &str,
    activity: JsonValue,
) -> ProcessResult {
    let object_id: &str = extract_object_id(&activity)
        .ok_or_else(|| ProcessingError::Validation("Object id is missing".into()))?;
    let original_activity = get_ap_activity_by_object_id(&database, &object_id, "Create")
        .map_err(ProcessingError::wrap)?;
    if original_activity.actor != actor {
        return Err(ProcessingError::NotAuthorized);
    }
    // We implement this for statuses for now
    let obj_type = original_activity.data["object"]["type"]
        .as_str()
        .expect("object type missing");
    if obj_type != "Note" {
        println!("Remove for non-notes is not supported, {:?}", obj_type);
        return Ok(());
    }

    delete_ap_object_by_id(&database, object_id).map_err(ProcessingError::wrap)?;
    if let Some(in_reply_to) = original_activity.data["object"]["inReplyTo"].as_str() {
        if let Err(e) = decrease_reply(&database, in_reply_to) {
            eprintln!(
                "Failed to decrease reply count for {}, {:?}",
                in_reply_to, e
            );
        }
    }
    println!("Removed status {}", object_id);
    // TODO: clean up notifications, timelines
    Ok(())
}

async fn process_update(
    db: database::PooledConnection,
    actor: &str,
    mut activity: JsonValue,
) -> ProcessResult {
    let object = match activity["object"].take() {
        JsonValue::Object(obj) => obj,
        JsonValue::String(id) => match fetch_remote_object(&id).await {
            Ok(JsonValue::Object(map)) => map,
            _ => return Err(ProcessingError::validation("Update: invalid object field")),
        },
        _ => {
            return Err(ProcessingError::Validation(
                "Update: invalid object field".to_string(),
            ));
        }
    };
    if object["type"].as_str() != Some("Person") {
        // We don't have other types yet
        return Ok(());
    }

    let serialized_actor = validator::validate_actor(JsonValue::Object(object.clone()))
        .map_err(|e| ProcessingError::validation(e))?;
    if serialized_actor.id != actor {
        return Err(ProcessingError::NotAuthorized);
    }
    if actor_exists(&serialized_actor.id) {
        crate::models::actor::update(&db, create_internal_actor(serialized_actor));
    } else {
        crate::models::actor::update(&db, create_internal_actor(serialized_actor));
    }

    Ok(())
}

fn extract_object_id(activity: &JsonValue) -> Option<&str> {
    let object = activity.get("object")?;
    value_or_id(object)
}

fn value_or_id(json: &JsonValue) -> Option<&str> {
    match json {
        JsonValue::Object(map) => map["id"].as_str(),
        JsonValue::String(s) => Some(&s),
        _ => None,
    }
}

impl From<diesel::result::Error> for APError {
    fn from(diesel_error: diesel::result::Error) -> Self {
        match diesel_error {
            diesel::result::Error::NotFound => APError::NotFound,
            e => APError::Internal(Box::new(e)),
        }
    }
}

pub fn outbox(handle: String) -> APResponse<JsonValue> {
    let db = database::establish_connection();
    let actor = get_local_actor_by_preferred_username(&db, &handle)?;
    let notes_count = match count_ap_notes_for_actor(&db, &actor.actor_uri) {
        Ok(count) => count,
        Err(e) => {
            eprintln!("Error while counting notes {}", e);
            return Err(APError::Internal(Box::new(e)));
        }
    };

    if !actor.local {
        return Err(APError::NotFound);
    }
    let actor_uri = Url::parse(&actor.actor_uri).expect("invalid actor url");
    let outbox_url = actor_uri.join("/outbox").expect("invalid outbox url");
    let mut first_url = outbox_url.clone();
    first_url.set_query(Some("paged=1"));

    let json = json!({
        "context": "https://www.w3.org/ns/activitystreams",
        "type": "OrderedCollection",
        "id": outbox_url.to_string(),
        "totalItems": notes_count,
        "first": first_url.to_string()
    });
    Ok(ActivitystreamsResponse(json))
}

pub fn following(handle: String) -> APResponse<OrderedCollection> {
    let db = database::establish_connection();
    let actor = get_local_actor_by_preferred_username(&db, &handle)?;
    let following = count_actor_followees(&db, &actor.actor_uri)?;

    let following_url = following_url(&actor.actor_uri);
    let mut first_url = following_url.clone();
    first_url.set_query(Some("paged=1"));

    let collection = OrderedCollection {
        id: following_url,
        total_items: following,
        first: first_url,
    };

    Ok(ActivitystreamsResponse(collection.into()))
}

pub fn following_page(handle: String) -> APResponse<OrderedCollectionPage> {
    let db = database::establish_connection();
    let actor = get_local_actor_by_preferred_username(&db, &handle)?;
    // We should page it later and request urls only
    let followees = get_actor_followees(&db, &actor.actor_uri)?;
    let count = followees.len();
    let following_url = following_url(&actor.actor_uri);
    let mut page_url = following_url.clone();
    page_url.set_query(Some("paged=1"));
    let actor_urls = followees
        .iter()
        .map(|a| a.actor_uri.clone())
        .collect::<Vec<String>>();
    let collection = OrderedCollectionPage {
        id: page_url,
        total_items: count as i64,
        part_of: following_url,
        ordered_items: actor_urls,
    };
    Ok(ActivitystreamsResponse(collection.into()))
}

pub fn followers(handle: String) -> APResponse<OrderedCollection> {
    let db = database::establish_connection();
    let actor = get_local_actor_by_preferred_username(&db, &handle)?;
    let followers_url = followers_url(&actor.actor_uri);
    let mut first = followers_url.clone();
    first.set_query(Some("paged=1"));
    let followers = actor
        .followers()
        .map_err(|e| APError::Internal(Box::new(e)))?;

    let collection = OrderedCollection {
        id: followers_url,
        total_items: followers.len() as i64,
        first: first,
    };
    Ok(ActivitystreamsResponse(collection.into()))
}

pub fn followers_page<'v>(handle: String) -> APResponse<OrderedCollectionPage> {
    let db = database::establish_connection();
    let actor = get_local_actor_by_preferred_username(&db, &handle)?;
    let followers_url = followers_url(&actor.actor_uri);
    let mut page_url = followers_url.clone();
    page_url.set_query(Some("paged=1"));
    let followers: Vec<String> = actor
        .followers()
        .map_err(|e| APError::Internal(Box::new(e)))?
        .into_iter()
        .map(|follower| follower.href)
        .collect();

    let collection = OrderedCollectionPage {
        id: page_url,
        total_items: followers.len() as i64,
        part_of: followers_url,
        ordered_items: followers,
    };
    Ok(ActivitystreamsResponse(collection))
}

fn following_url(actor_uri: &str) -> Url {
    Url::parse(&format!("{}/{}", actor_uri, "following")).expect("invalid following url")
}

fn followers_url(actor_uri: &str) -> Url {
    Url::parse(&format!("{}/{}", actor_uri, "followers")).expect("invalid followers url")
}

static ACTIVITY_STREAMS_CONTEXT: &str = "https://www.w3.org/ns/activitystreams";
static ACTIVITY_STREAMS_PUBLIC: &str = "https://www.w3.org/ns/activitystreams#Public";
static ORDERED_COLLECTION_TYPE: &str = "OrderedCollection";
static ORDERED_COLLECTION_PAGE_TYPE: &str = "OrderedCollectionPage";

pub struct OrderedCollection {
    id: Url,
    total_items: i64,
    first: Url,
}

impl Into<JsonValue> for OrderedCollection {
    fn into(self) -> JsonValue {
        json!({
            "context": ACTIVITY_STREAMS_CONTEXT,
            "type": ORDERED_COLLECTION_TYPE,
            "id": self.id.to_string(),
            "totalItems": self.total_items,
            "first": self.first.to_string(),
        })
    }
}

pub struct OrderedCollectionPage {
    id: Url,
    total_items: i64,
    part_of: Url,
    ordered_items: Vec<String>,
}

impl Into<JsonValue> for OrderedCollectionPage {
    fn into(self) -> JsonValue {
        json!({
            "context": ACTIVITY_STREAMS_CONTEXT,
            "type": ORDERED_COLLECTION_PAGE_TYPE,
            "id": self.id.to_string(),
            "partOf": self.part_of.to_string(),
            "totalItems": self.total_items,
            "orderedItems": self.ordered_items,
        })
    }
}
