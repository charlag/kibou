use crate::models::actor::{get_actor_by_uri, Actor};
use crate::database::PooledConnection;

pub fn distribute_profile_update(db: PooledConnection, actor: Actor) {
    let profile_update = crate::activitypub::controller::profile_update(&db, &actor);
    // TODO: we would like to ideally get shared inboxes but in the absence we
    //  will just send to the followers for now.
    //  This is obviously cursed and we need to change database schema to not
    //  fetch actors one by one.
    let inboxes = actor.followers()
        .unwrap()
        .iter()
        .filter_map(|f| get_actor_by_uri(&db, &f.href).ok())
        .filter_map(|a| a.inbox)
        .collect::<Vec<_>>();
    let activity_json = serde_json::to_value(profile_update).unwrap();
    crate::web::federator::enqueue(&actor, activity_json, inboxes);
}
