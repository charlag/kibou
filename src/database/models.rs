use super::schema::{activities, actors, media_attachments, notifications};
use diesel::sql_types::{Bigint, Jsonb, Timestamp, Text};

use chrono::NaiveDateTime;

#[derive(Queryable, PartialEq, QueryableByName, Clone)]
#[table_name = "activities"]
pub struct QueryActivity {
    #[sql_type = "Bigint"]
    pub id: i64,
    #[sql_type = "Jsonb"]
    pub data: serde_json::Value,
    #[sql_type = "Timestamp"]
    pub created: NaiveDateTime,
    #[sql_type = "Timestamp"]
    pub modified: NaiveDateTime,
    #[sql_type = "Text"]
    pub actor_uri: String,
}

#[derive(Queryable, PartialEq, QueryableByName, Clone)]
#[table_name = "activities"]
pub struct QueryActivityId {
    pub id: i64,
}

#[derive(Insertable)]
#[table_name = "activities"]
pub struct InsertActivity<'a> {
    pub id: i64,
    pub data: &'a serde_json::Value,
    pub actor_uri: &'a String,
}

#[derive(Insertable)]
#[table_name = "notifications"]
pub struct InsertNotification {
    pub activity_id: i64,
    pub actor_id: i64,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime,
}

#[derive(Queryable, PartialEq, QueryableByName, Clone)]
#[table_name = "actors"]
pub struct QueryActor {
    pub id: i64,
    pub email: Option<String>,
    pub password: Option<String>,
    pub actor_uri: String,
    pub username: Option<String>,
    pub preferred_username: String,
    pub summary: Option<String>,
    pub inbox: Option<String>,
    pub icon: Option<String>,
    pub keys: serde_json::Value,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime,
    pub local: bool,
    pub followers: serde_json::Value,
    pub url: Option<String>,
}

#[derive(Queryable, Debug)]
pub struct QueryOAuthApplication {
    pub id: i64,
    pub client_name: Option<String>,
    pub client_id: String,
    pub client_secret: String,
    pub redirect_uris: String,
    pub scopes: String,
    pub website: Option<String>,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime,
}

#[derive(Queryable, Debug)]
pub struct QueryOAuthAuthorization {
    pub id: i64,
    pub application: i64,
    pub actor: String,
    pub code: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}

#[derive(Queryable, Debug)]
pub struct QueryOauthToken {
    pub id: i64,
    pub application: i64,
    pub actor: String,
    pub access_token: String,
    pub refresh_token: String,
    pub created: NaiveDateTime,
    pub modified: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}

#[derive(Queryable)]
pub struct MediaAttachment {
    pub id: i64,
    pub file_name: String,
    pub content_type: String,
    pub size: i32,
    pub description: Option<String>,
    pub actor_id: i64,
    pub created: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "media_attachments"]
pub struct NewMediaAttachment<'a> {
    pub file_name: &'a str,
    pub content_type: &'a str,
    pub size: i32,
    pub description: &'a str,
    pub actor_id: i64,
}
