use crate::activitypub::actor::refresh_actor;
use crate::database::{self, PooledConnection};
use crate::models::actor;

pub async fn refetch_profiles(db: &PooledConnection, older_than: &chrono::NaiveDateTime) {
    let profiles = match actor::select_stale_actors(&db, older_than) {
        Ok(profiles) => profiles,
        Err(e) => {
            error!("Worker refresh_profiles, failed with: {}", e);
            return;
        }
    };
    info!("Refreshing {} profile(s)", profiles.len());
    for profile in profiles {
        info!("Refreshing actor {}", profile);
        match refresh_actor(&db, &profile).await {
            Ok(_) => info!("Successfully refreshed actor {}", profile),
            Err(e) => error!("Failed to refresh actor {}, {}", profile, e),
        };
    }
}

pub fn refresh_profiles() {
    actix_rt::spawn(do_refresh_profiles());
}

async fn do_refresh_profiles() {
    actix_rt::time::delay_for(std::time::Duration::from_secs(5)).await;
    let db = database::establish_connection();
    let older_than = chrono::Utc::now() - chrono::Duration::days(4);
    info!(
        "Worker refresh_profiles: refreshing profiles older than {}",
        older_than
    );
    refetch_profiles(&db, &older_than.naive_utc()).await;
}
