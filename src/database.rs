pub mod models;
pub mod schema;

use crate::env;

use actix_web::{dev, error::ErrorServiceUnavailable, web, Error, FromRequest, HttpRequest};
use diesel::{
    pg::PgConnection,
    r2d2::{self, ConnectionManager},
};
use futures::Future;
use regex::Regex;
use std::pin::Pin;

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;
pub struct PooledConnection(pub r2d2::PooledConnection<ConnectionManager<PgConnection>>);

lazy_static! {
    pub static ref POOL: Pool = initialize_pool();
}

// This is provided mostly for compatibility reasons. We should avoid using it
// because:
// 1. It might acquire and hold connection from pool longer than we
// need. This effectively limits connection number to the database worker number
// 2. We should not use raw database connections in most cases anyway. It makes
// unit tests impossible and breaks abstractions.
impl FromRequest for PooledConnection {
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;
    type Config = ();

    fn from_request(req: &HttpRequest, _: &mut dev::Payload) -> Self::Future {
        // Pool is behind arc so it's safe to clone. Have to clone even
        // if I start Future before the pin because who knows when executor desides
        // to run it? So we have to have our own ref.
        let pool = req.app_data::<&Pool>().unwrap().clone();
        Box::pin(async move {
            if let Ok(conn) = web::block(move || pool.get()).await {
                let pooled_connetion = PooledConnection(conn);
                Ok(pooled_connetion)
            } else {
                Err(ErrorServiceUnavailable("Not available"))
            }
        })
    }
}

impl std::ops::Deref for PooledConnection {
    type Target = PgConnection;
    fn deref(&self) -> &Self::Target {
        return &self.0;
    }
}

impl PooledConnection {
    pub fn conn(&self) -> &PgConnection {
        return &self.0
    }
}

pub fn establish_connection() -> PooledConnection {
    // Originally this always established a new database connection, now this is just gonna
    // return a new connection from the pool.

    return PooledConnection(POOL.get().unwrap());
}

pub fn initialize_pool() -> Pool {
    let connection_manager = ConnectionManager::<PgConnection>::new(prepare_postgres_url());

    // TODO: Eventually replace this with r2d2::Pool::builder()
    return Pool::new(connection_manager).expect("Could not initialize database pool!");
}

pub fn runtime_escape(value: &str) -> String {
    let escape_regex = Regex::new(r"[a-zA-Z0-9_:\\.\-\\/]").unwrap();
    value
        .chars()
        .filter(|&c| escape_regex.is_match(&c.to_string()))
        .collect()
}

fn prepare_postgres_url() -> String {
    return env::get_value("database.connect_url");
}
