pub mod actions;
pub mod activity;
pub mod actor;
pub mod controller;
pub mod routes;
pub mod validator;

use actix_web::{HttpRequest, HttpResponse, Responder, ResponseError, http::StatusCode};
use futures::future::{ok, Ready};
use serde::{Deserialize, Serialize};
use std::fmt;

pub struct ActivitypubMediatype(bool);
type APResponse<T> = Result<ActivitystreamsResponse<T>, APError>;

#[derive(Debug)]
pub enum APError {
    NotFound,
    Internal(Box<dyn std::error::Error>),
}

impl fmt::Display for APError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            APError::NotFound => write!(f, "Not found"),
            APError::Internal(e) => write!(f, "Internal: {:?}", e),
        }
    }
}

impl ResponseError for APError {
    fn status_code(&self) -> StatusCode {
        match self {
            APError::NotFound => StatusCode::NOT_FOUND,
            APError::Internal(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

// ActivityStreams2/AcitivityPub properties are expressed in CamelCase
#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
pub struct Attachment {
    #[serde(rename = "type")]
    pub _type: AttachmentType,
    pub content: Option<String>,
    pub url: String,
    pub name: Option<String>,
    pub mediaType: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub enum AttachmentType {
    Image,
    Video,
    Audio,
    Document,
}

pub fn attachment_type(content_type: &str) -> Option<AttachmentType> {
    if content_type.starts_with("image/") {
        Some(AttachmentType::Image)
    } else if content_type.starts_with("video/") {
        Some(AttachmentType::Video)
    } else if content_type.starts_with("audio/") {
        Some(AttachmentType::Audio)
    } else {
        None
    }
}

// TODO: limit the size
pub struct Payload(serde_json::Value);

/// Wrapper for ActivityPub responses. Will set correct Content-Type.
///
/// Requires `Into<sede_json::Value>` to make sure that types are not merely
/// serialized as-is. Should have special trait for AP responses or should
/// require into some well-defined AP type.
pub struct ActivitystreamsResponse<T: Into<serde_json::Value>>(T);

impl<T: Into<serde_json::Value>> Responder for ActivitystreamsResponse<T> {
    type Error = actix_web::Error;
    type Future = Ready<Result<HttpResponse, Self::Error>>;

    fn respond_to(self, _: &HttpRequest) -> Self::Future {
        let res = HttpResponse::Ok()
            .content_type("application/activity+json")
            .json(self.0.into());
        ok(res)
    }
}
