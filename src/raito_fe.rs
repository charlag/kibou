pub mod api_controller;
pub mod renderer;
pub mod routes;

use crate::models::actor::Actor;
use crate::database::PooledConnection;
use crate::env;
use crate::mastodon_api::{self, Account};

use actix_web::{
    dev,
    web::HttpRequest,
    FromRequest,
};
use futures::Future;
use serde::Deserialize;

use std::pin::Pin;

pub struct Configuration {
    pub account: Option<mastodon_api::Account>,
    pub actor: Option<Actor>,
    pub context: tera::Context,
    pub token: Option<String>,
}

#[derive(Deserialize)]
pub struct LoginForm {
    pub username: String,
    pub password: String,
}

impl FromRequest for Configuration {
    type Error = actix_web::Error;
    type Future = Pin<Box<dyn Future<Output=Result<Self, Self::Error>>>>;
    type Config = ();

    fn from_request(req: &HttpRequest, payload: &mut dev::Payload) -> Self::Future {
        let session_fut = actix_session::Session::from_request(req, payload);
        let db_fut = PooledConnection::from_request(req, payload);

        Box::pin(async move {
            let mut account: Option<mastodon_api::Account> = None;
            let mut context = tera::Context::new();
            let mut token: Option<String> = None;
            let mut actor: Option<Actor> = None;

            context.insert("javascript_enabled", "false");
            let base_url = format!(
                "{base_scheme}://{base_domain}",
                base_scheme = env::get_value("endpoint.base_scheme"),
                base_domain = env::get_value("endpoint.base_domain")
            );
            context.insert("mastodon_api_base_uri", &base_url);
            context.insert("minimalmode_enabled", "false");

            let db = db_fut.await.unwrap();

            let session = session_fut.await.unwrap();
            let token_cookie = session.get::<String>("oauth_token").unwrap();
            if let Some(oauth_token) = token_cookie {
                actor = mastodon_api::controller::actor_by_oauth_token(&db, &oauth_token);
                account = actor.clone().map(|a| Account::from_actor(&db, a, false));
                token = Some(oauth_token);
            }

            match &account {
                Some(local_account) => {
                    context.insert("authenticated_account", &true.to_string());
                    context.insert(
                        "authenticated_account_display_name",
                        &local_account.display_name,
                    );
                    context.insert("authenticated_account_avatar", &local_account.avatar);
                    context.insert("authenticated_account_id", &local_account.id);
                    context.insert("authenticated_account_acct", &local_account.acct());
                }
                None => {
                    context.insert("authenticated_account", &false.to_string());
                    context.insert("authenticated_account_display_name", "Guest");
                    context.insert(
                        "authenticated_account_avatar",
                        "/static/assets/default_avatar.png",
                    );
                }
            }

            // TODO: remove this and possibly change how and when notifications are displayed
            context.extend(renderer::context_notifications(&db, &actor));
            let config = Configuration {
                account,
                actor,
                context,
                token,
            };
            Ok(config)
        })
    }
}
