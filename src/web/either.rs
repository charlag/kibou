use actix_web::{dev::Payload, Error, FromRequest, HttpRequest};
use std::{future::Future, pin::Pin};

/// Helper sum type for cases where data can try to get on or another value from
/// the request, e.g. when data can be passed as form or JSON.
/// Thanks to Asonix for implementing this.
#[derive(Clone, Debug)]
pub enum Either<L, R> {
    Left(L),
    Right(R),
}

impl<L, R> actix_web::FromRequest for Either<L, R>
where
    L: FromRequest,
    <L as FromRequest>::Future: 'static,
    <L as FromRequest>::Error: Into<Error> + 'static,
    R: FromRequest,
    <R as FromRequest>::Future: 'static,
    <R as FromRequest>::Error: Into<Error> + 'static,
{
    type Error = actix_web::Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;
    type Config = ();

    fn from_request(req: &HttpRequest, payload: &mut Payload) -> Self::Future {
        let f1 = L::from_request(req, payload);
        let f2 = R::from_request(req, payload);

        Box::pin(async move {
            if let Ok(l) = f1.await.map(Either::Left) {
                return Ok(l);
            }

            f2.await.map(Either::Right).map_err(|e| e.into())
        })
    }
}
