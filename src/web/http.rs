/// Format date as RFC822
pub fn format_http_date(time: time::Tm) -> String {
    time.to_utc().rfc822().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_date_header() {
        let time = time::at_utc(time::Timespec::new(1021000023, 2));
        assert_eq!(
            format_http_date(time),
            "Fri, 10 May 2002 03:07:03 GMT"
        )
    }
}
