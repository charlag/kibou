use crate::models::actor::Actor;
use crate::web::http;

use actix_web::{http::StatusCode, FromRequest, HttpMessage, HttpRequest, ResponseError};
use futures::future::{ready, Ready};
use openssl::{hash::MessageDigest, pkey::PKey, rsa::Rsa, sign::Verifier};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug)]
pub struct Signature {
    pub algorithm: Option<String>,
    pub content_length: Option<String>,
    pub content_type: Option<String>,
    pub date: String,
    pub digest: Option<String>,
    pub host: String,
    pub key_id: String,
    pub headers: Vec<String>,
    pub request_target: Option<String>,
    pub signature: String,
    pub signature_in_bytes: Option<Vec<u8>>,
}

impl Signature {
    pub fn build_header(self) -> String {
        format!("keyId=\"{key_id}\",algorithm=\"{algorithm}\",headers=\"{headers}\",signature=\"{signature}\"",
                key_id = self.key_id,
                algorithm = self.algorithm.unwrap(),
                headers = self.headers.join(" "),
                signature = self.signature
        )
    }

    pub fn verify(self, actor: &mut Actor) -> bool {
        let mut signature: Vec<String> = Vec::new();

        for header in &self.headers {
            match header.as_str() {
                "(request-target)" => signature.push(format!(
                    "(request-target): {}",
                    self.request_target.as_ref().unwrap()
                )),
                "content-length" => signature.push(format!(
                    "content-length: {}",
                    self.content_length.as_ref().unwrap()
                )),
                "date" => signature.push(format!("date: {}", &self.date)),
                "digest" => signature.push(format!("digest: {}", self.digest.as_ref().unwrap())),
                "host" => signature.push(format!("host: {}", &self.host)),
                "content-type" => signature.push(format!(
                    "content-type: {}",
                    &self.content_type.as_ref().unwrap()
                )),
                _ => (),
            }
        }

        let decoded_pem = pem::parse(actor.get_public_key()).unwrap();
        let public_key =
            PKey::from_rsa(Rsa::public_key_from_der(&decoded_pem.contents).unwrap()).unwrap();

        let mut verifier = Verifier::new(MessageDigest::sha256(), &public_key).unwrap();
        verifier.update(&signature.join("\n").into_bytes()).unwrap();

        return verifier.verify(&self.signature_in_bytes.unwrap()).unwrap();
    }

    pub fn new(
        key_id: &str,
        request_target: &str,
        host: &str,
        date: time::Tm,
        digest: String
    ) -> Signature {
        return Signature {
            algorithm: Some(String::from("rsa-sha256")),
            content_length: None,
            content_type: None,
            date: http::format_http_date(date),
            digest: Some(digest),
            host: host.into(),
            key_id: key_id.into(),
            headers: vec![
                "(request-target)".to_string(),
                "date".to_string(),
                "host".to_string(),
                "digest".to_string(),
            ],
            request_target: Some(request_target.into()),
            signature: String::from(""),
            signature_in_bytes: None,
        };
    }

    pub fn sign(&mut self, actor: &Actor) {
        let string_to_be_signed = format!(
            "(request-target): post {request_target}\ndate: {date}\nhost: {host}\ndigest: SHA-256={digest}",
            request_target = self.request_target.as_ref().unwrap(),
            date = self.date,
            host = self.host,
            digest = self.digest.as_ref().expect("Must have digest to sign"),
        );
        dbg!(&string_to_be_signed);

        self.signature = actor.sign(string_to_be_signed);
    }

    fn parse_request(request: &HttpRequest) -> Result<Self, SignatureParseError> {
        let get_header = |name| {
            request
                .headers()
                .get(name)
                .ok_or_else(|| SignatureParseError::new(format!("No header {}", name)))
                .and_then(|h| {
                    h.to_str().map_err(|_| {
                        SignatureParseError::new(format!("Failed to decode header {}", name))
                    })
                })
        };

        let content_length = get_header("Content-Length").ok();
        let content_type = request.content_type();
        let date = get_header("Date")?;
        let digest = get_header("Digest").ok();
        let host = get_header("Host")?;
        let signature = get_header("Signature")?;
        let parsed_signature = parse_signature(signature)
            .ok_or_else(|| SignatureParseError::new("Failed to parse signature"))?;
        let request_target = format!(
            "{} {}",
            request.method().as_str().to_ascii_lowercase(),
            request.path()
        );
        let key_id =
            parsed_signature.get("keyId")
                .ok_or_else(|| SignatureParseError::new("no keyId"))?;
        let headers = parsed_signature["headers"]
            .split_whitespace()
            .map(|s| s.to_owned())
            .collect();

        let signature = Signature {
            algorithm: None, // Theoretically needed to verify but we assume it for now
            content_length: content_length.map(|l| l.to_string()),
            content_type: Some(content_type.to_string()),
            date: date.to_string(),
            digest: digest.map(|d| d.to_string()),
            headers,
            host: host.to_string(),
            key_id: key_id.to_owned(),
            request_target: Some(request_target),
            signature: String::new(),
            signature_in_bytes: Some(
                base64::decode(&parsed_signature["signature"].to_owned().into_bytes())
                    .map_err(|_| SignatureParseError::new("Failed to decode signature"))?,
            ),
        };
        Ok(signature)
    }
}

#[derive(Debug)]
pub struct SignatureParseError {
    reason: String,
}

impl SignatureParseError {
    fn new<T: Into<String>>(reason: T) -> Self {
        SignatureParseError {
            reason: reason.into(),
        }
    }
}

impl fmt::Display for SignatureParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Signature parse error, {}", self.reason)
    }
}

impl ResponseError for SignatureParseError {
    fn status_code(&self) -> StatusCode {
        StatusCode::BAD_REQUEST
    }
}

impl FromRequest for Signature {
    type Error = SignatureParseError;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(request: &HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        ready(Signature::parse_request(request))
    }
}

// It is still not reliable enough (some comma in the middle could screw it up),
// should probably replace it with some better parser
fn parse_signature(sign: &str) -> Option<HashMap<String, String>> {
    // It's easier to do all the short-circuit things with imperative loop
    // could use map_while here.
    let mut map = HashMap::new();
    for kv in sign.split(',') {
        let mut kv = kv.splitn(2, '=');
        if let (Some(k), Some(v)) = (kv.next(), kv.next()) {
            let k = k.to_string();
            let v = remove_dquotes(v)?.to_string();
            map.insert(k, v);
        } else {
            return None;
        }
    }
    Some(map)
}

fn remove_dquotes(s: &str) -> Option<&str> {
    let bytes = s.as_bytes();
    match bytes {
        &[b'"', .., b'"'] => (),
        _ => return None,
    }
    let without_quotes = &bytes[1..bytes.len() - 1];
    std::str::from_utf8(without_quotes).ok()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_signature() {
        let signature = r#"keyId="http://mastodon.local/users/admin#main-key",algorithm="rsa-sha256",headers="(request-target) host date digest content-type",signature="NUjxhg5p9ijTHEFbMv71TC6btXIKOCJbMLeiKIJio0oY6Ij3Dj9nRkDl+gBVZawJhPmOO6b3ZLRNpV6IJWKRDrD80Rl4Jp+Sajtengur8TRa99TRN8dhTKBMJ6sS4grpiTfMDSqE3ToXQh+iHhpm1T8/+nEtOEl0xZHi8mbGwJ4pIBF+MwyoGbB0ktePQP64iATmHTTfZCKXqXKhoAHSAIWmqtwpEhwwoX/tcvD0t1biSiOs0kNWLRL3+pNv/qkaBf5SAYLamvO5g4xM3Sx7WzA847h2pKeIhtBWJbCbOVX2MZ7NNOuTPF46Xi9ujFXQT8xWboOxlTS1wqRoanwNZg==""#;
        let expected: HashMap<String, String> = [
            ("keyId", "http://mastodon.local/users/admin#main-key"),
            ("algorithm", "rsa-sha256"),
            ("headers", "(request-target) host date digest content-type"),
            ("signature", "NUjxhg5p9ijTHEFbMv71TC6btXIKOCJbMLeiKIJio0oY6Ij3Dj9nRkDl+gBVZawJhPmOO6b3ZLRNpV6IJWKRDrD80Rl4Jp+Sajtengur8TRa99TRN8dhTKBMJ6sS4grpiTfMDSqE3ToXQh+iHhpm1T8/+nEtOEl0xZHi8mbGwJ4pIBF+MwyoGbB0ktePQP64iATmHTTfZCKXqXKhoAHSAIWmqtwpEhwwoX/tcvD0t1biSiOs0kNWLRL3+pNv/qkaBf5SAYLamvO5g4xM3Sx7WzA847h2pKeIhtBWJbCbOVX2MZ7NNOuTPF46Xi9ujFXQT8xWboOxlTS1wqRoanwNZg==")
        ].iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect();
        assert_eq!(parse_signature(signature), Some(expected));
    }

    #[test]
    fn test_remove_dquotes() {
        assert_eq!(remove_dquotes("\"Inside dquotes\""), Some("Inside dquotes"));
        assert_eq!(remove_dquotes("\"Half-open"), None);
    }
}
