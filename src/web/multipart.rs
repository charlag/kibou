use crate::env;

use actix_web::{web};
use actix_multipart::Field;
use futures::{StreamExt};
use std::io::Write;

pub struct SavedFile {
    pub passed_name: Option<String>,
    pub assigned_name: String,
    pub content_type: String,
}

pub enum FileSaveError {
    TooBig,
}

pub async fn save_multipart_file(field: &mut Field) -> Result<SavedFile, FileSaveError> {
    let disposition = field.content_disposition().unwrap();
    let content_type = field.content_type().to_string();
    let passed_file_name = disposition.get_filename();
    let file_extension = passed_file_name
        .and_then(|f| f.split(".").last())
        .unwrap_or("");
    let file_name = format!(
        "{}.{}",
        chrono::Utc::now().timestamp_millis().to_string(),
        file_extension
    );
    let mut filepath = std::path::PathBuf::new();
    filepath.push(env::get_value("media.path"));
    filepath.push(&file_name);
    let path = filepath.clone();
    let mut file = web::block(|| std::fs::File::create(path)).await.unwrap();

    let mut copied: u64 = 0;

    while let Some(chunk) = field.next().await {
        let data = chunk.unwrap();
        // Move file into closure and then out of it to be able to use it
        // again. Rust's not smart enough to figure out that we await for it
        // before using file again.
        let chunk_size = data.len();
        file = web::block(move || file.write_all(&data).map(|_| file))
            .await
            .unwrap();
        copied += chunk_size as u64;
        if copied > 8_000_000 {
            let path = filepath.clone();
            if let Err(e) = web::block(move || std::fs::remove_file(path)).await {
                error!("Failed to remove uploaded file: {:?}", e);
            };
            return Err(FileSaveError::TooBig);
        }
    }
    let saved_file = SavedFile {
        passed_name: passed_file_name.map(|s| s.into()),
        assigned_name: file_name,
        content_type: content_type,
    };
    Ok(saved_file)
}

pub async fn read_multipart_string(field: &mut actix_multipart::Field) -> Option<String> {
    let mut buffer: Vec<u8> = Vec::new();
    // TODO: size limit
    while let Some(chunk) = field.next().await {
        let data = chunk.unwrap();
        buffer.write_all(&data).ok()?;
    }
    String::from_utf8(buffer).ok()
}
