use std::fmt;

pub fn from_plain(plain: &str) -> String {
    // I don't know how to rewrite or use Escape more efficiently
    format!("<p>{}</p>", Escape(plain)).replace("\n", "<br>")
}

pub fn sanitize(input: &str) -> String {
    // Mostly relevant Microformats classes plus mention
    let allowed_classes_list = &[
        "u-photo", "u-uid", "u-url", "mention", "hashtag", "p-author", "p-name", "h-card",
    ];
    ammonia::Builder::default()
        .add_allowed_classes("a", allowed_classes_list)
        .add_allowed_classes("span", allowed_classes_list)
        .clean(input)
        .to_string()
}
/// Wrapper struct which will emit the HTML-escaped version of the contained
/// string when passed to a format string.
pub struct Escape<'a>(pub &'a str);

impl<'a> fmt::Display for Escape<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Because the internet is always right, turns out there's not that many
        // characters to escape: http://stackoverflow.com/questions/7381974
        let Escape(s) = *self;
        let pile_o_bits = s;
        let mut last = 0;
        for (i, ch) in s.bytes().enumerate() {
            match ch as char {
                '<' | '>' | '&' | '\'' | '"' => {
                    fmt.write_str(&pile_o_bits[last..i])?;
                    let s = match ch as char {
                        '>' => "&gt;",
                        '<' => "&lt;",
                        '&' => "&amp;",
                        '\'' => "&#39;",
                        '"' => "&quot;",
                        _ => unreachable!(),
                    };
                    fmt.write_str(s)?;
                    last = i + 1;
                }
                _ => {}
            }
        }

        if last < s.len() {
            fmt.write_str(&pile_o_bits[last..])?;
        }
        Ok(())
    }
}

pub fn add_links(text: &str) -> String {
    let finder = linkify::LinkFinder::new();
    let spans = finder.spans(text);
    let mut res = String::new();
    for span in spans {
        if span.kind() == Some(&linkify::LinkKind::Url) {
            res.push_str("<a href=\"");
            res.push_str(span.as_str());
            res.push_str("\">");
            res.push_str(span.as_str());
            res.push_str("</a>");
        } else {
            res.push_str(span.as_str());
        }
    }
    res
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_sanitize() {
        assert_eq!("<p>Test</p>", sanitize("<p>Test</p>"));
        assert_eq!("", sanitize("<script>Test</script>"));
        assert_eq!(
            r#"<a rel="noopener noreferrer">Test</a>"#,
            sanitize("<a onclick=\"malicious_stuff\">Test</a>")
        );
        assert_eq!(
            "<p>Test</p><p></p>",
            sanitize(r#"<p>Test<script>alert()</script><p>"#)
        );
        assert_eq!("&lt;", sanitize(r#"<<p < Test script </p"#));

        assert_eq!(
            r#"<p><span class="h-card">
<a class="u-url mention" href="http://mastodon.local/users/admin" rel="noopener noreferrer">@
<span>admin</span></a></span> mention 25</p>"#,
            sanitize(
                r#"<p><span class="h-card">
<a class="u-url mention" href="http://mastodon.local/users/admin" >@
<span>admin</span></a></span> mention 25</p>"#
            )
        );
    }

    #[test]
    fn test_from_plain() {
        assert_eq!(
            r#"<p>&lt; oats<br> &gt;clucks<br><br><br> &amp; coyote &#39; &quot;</p>"#,
            from_plain("< oats\n >clucks\n\n\n & coyote ' \"")
        )
    }

    #[test]
    fn test_add_links() {
        let text = "https://example.com and then text".to_owned();
        assert_eq!(
            add_links(&text),
            "<a href=\"https://example.com\">https://example.com</a> and then text"
        );
        assert_eq!(
            add_links(&text),
            "<a href=\"https://example.com\">https://example.com</a> and then text"
        );
    }

    #[test]
    fn test_add_links_with_parens() {
        let text = "https://en.wikipedia.org/wiki/Discord_(software)";
        assert_eq!(
            add_links(text),
            format!("<a href=\"{}\">{}</a>", text, text)
        );
    }
}
