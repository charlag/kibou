extern crate time;

use crate::models::actor::Actor;
use crate::web::http;
use crate::web::http_signatures::Signature;
use serde_json;
use serde_json::Value as JsonValue;
use url::Url;
use anyhow::anyhow;
use openssl::hash::{hash, MessageDigest};

pub fn enqueue(actor: &Actor, activity: JsonValue, inboxes: Vec<String>) {
    let actor = actor.clone();
    actix_rt::spawn(deliver_to_inboxes(actor, inboxes, activity));
}

async fn deliver_to_inboxes(
    actor: Actor,
    inboxes: Vec<String>,
    activity: JsonValue,
) {
    let client = actix_web::client::Client::new();
    for inbox in inboxes {
        if let Err(e) = do_deliver(&client, &actor, &inbox, &activity).await {
            error!("Delivery to {} failed: {:?}", inbox, e);
        }
    }
}

async fn do_deliver(
    client: &actix_web::client::Client,
    actor: &Actor,
    inbox: &str,
    activity: &JsonValue,
) -> anyhow::Result<()> {
    println!("Federating activity to inbox: {}, {}", inbox, activity);

    let url = Url::parse(&inbox)?;
    let host = url.host_str().ok_or_else(|| anyhow!("URL host invalid: {}", url))?;
    let date = time::now_utc();

    let body_string = serde_json::to_string(&activity).unwrap();
    let digest_bytes = hash(MessageDigest::sha256(), &body_string.as_bytes()).unwrap();
    let digest_b64 = base64::encode(&digest_bytes);

    let mut signature = Signature::new(
        &format!("{}#main-key", &actor.actor_uri),
        url.path(),
        host,
        date,
        digest_b64.clone(),
    );

    signature.sign(actor);

    let mut response = client.post(inbox)
        .header(actix_web::http::header::DATE, http::format_http_date(date))
        .header(actix_web::http::header::HOST, host)
        .header("Signature", signature.build_header())
        .header("Digest", format!("SHA-256={}", digest_b64))
        .send_body(body_string)
        .await
        .map_err(|e| anyhow!("Error sending federation request: {:?}", e))?;

    return if response.status().is_success() {
        info!("Federator: success to {}", inbox);
        Ok(())
    } else {
        let text = response.body()
            .await
            .map_err(anyhow::Error::new)
            .and_then(|b| String::from_utf8(b.to_vec()).map_err(anyhow::Error::new))
            .unwrap_or_else(|e| e.to_string());
        Err(anyhow!("Federator failure to {} with status code {}: {}", inbox, response.status(), text))
    };
}
