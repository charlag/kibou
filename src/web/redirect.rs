use actix_web::{http, HttpRequest, HttpResponse, Responder, ResponseError};
use futures::future::{Ready, ok};
use std::fmt;

/// Declarative way to return redirect responses like in Rocket
#[derive(Debug)]
pub struct Redirect {
    path: String,
    code: http::StatusCode
}

impl fmt::Display for Redirect {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Redirect to {} with code {}", self.path, self.code)
    }
}

impl<'a> Redirect {
    pub fn to<T: Into<String>>(path: T) -> Self {
        Redirect {
            path: path.into(),
            code: http::StatusCode::TEMPORARY_REDIRECT,
        }
    }

    pub fn found<T: Into<String>>(path: T) -> Self {
        Redirect {
            path: path.into(),
            code: http::StatusCode::FOUND,
        }
    }

    pub fn see_other<T: Into<String>>(path: T) -> Self {
        Redirect {
            path: path.into(),
            code: http::StatusCode::SEE_OTHER,
        }
    }

    pub fn response(&self) -> HttpResponse {
        HttpResponse::TemporaryRedirect()
            .header(http::header::LOCATION, self.path.clone())
            .status(self.code)
            .finish()
    }
}

impl Responder for Redirect {
    type Error = ();
    type Future = Ready<Result<HttpResponse, ()>>;

    fn respond_to(self, _: &HttpRequest) -> Self::Future {
        ok(self.response())
    }
}

/// Simple wrapper around Redirect to use in Result
#[derive(Debug)]
pub struct RedirectError {
    redirect: Redirect,
    error: Option<anyhow::Error>
}

impl RedirectError {
    pub fn new(redirect: Redirect, error: anyhow::Error) -> Self {
        RedirectError {
            redirect,
            error: Some(error),
        }
    }
}

impl fmt::Display for RedirectError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Redirect error {:?} to: {}", self.error, self.redirect)
    }
}

impl From<Redirect> for RedirectError {
    fn from(redirect: Redirect) -> Self {
        RedirectError { redirect, error: None, }
    }
}

impl ResponseError for RedirectError {
    fn error_response(&self) -> HttpResponse {
        self.redirect.response()
    }
}
