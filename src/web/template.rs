use serde::Serialize;
use futures::{
    Future,
};
use actix_web::{web, Responder, HttpRequest, HttpResponse, FromRequest};
use tera::Tera;

use std::pin::Pin;

pub struct Template {
    name: &'static str,
    context: tera::Context,
}

impl Template {
    pub fn render<T: Serialize>(name: &'static str, context: T) -> Self {
        Template {
            name,
            context: tera::Context::from_serialize(context).unwrap(),
        }
    }

    pub fn with_context(name: &'static str, context: tera::Context) -> Self {
        Template { name, context }
    }
}

impl Responder for Template {
    type Error = actix_web::Error;
    type Future = Pin<Box<dyn Future<Output = Result<HttpResponse, Self::Error>>>>;

    fn respond_to(self, req: &HttpRequest) -> Self::Future {
        let tera_fut = web::Data::<Tera>::from_request(req, &mut actix_web::dev::Payload::None);
        Box::pin(async move {
            let tera_instance = tera_fut.await.unwrap().into_inner();
            tera_instance
                .render(self.name, &self.context)
                .map(|c| HttpResponse::Ok()
                     .content_type("text/html")
                     .body(c))
                .map_err(|e| {
                    error!("Failure during template rendering: {:?}", e);
                    actix_web::error::ErrorInternalServerError(e)
                })
        })
    }
}
