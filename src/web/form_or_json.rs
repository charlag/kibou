use actix_web::web::{Json, Form};
use crate::web::Either;

pub type FormOrJson<T> = Either<Json<T>, Form<T>>;


impl<T> FormOrJson<T> {
    pub fn into_inner(self) -> T {
        match self {
            Either::Left(l) => l.into_inner(),
            Either::Right(r) => r.into_inner(),
        }
    }
}
