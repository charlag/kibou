mod auth_user;
pub mod controller;
pub mod routes;
mod status;

use crate::activitypub;
use crate::database::{self, PooledConnection};
use crate::env;
use crate::models::{
    activity::{count_ap_notes_for_actor, get_ap_object_by_id, Activity, ReactedActivity},
    actor::{count_followees, Actor},
};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json;
use std::sync::{Arc, Mutex};
use url::Url;

pub use auth_user::AuthenticatedUser;
pub use status::Status;

#[derive(Serialize, Deserialize, Clone)]
pub struct Account {
    // Properties according to
    // - https://docs.joinmastodon.org/api/entities/#account
    pub id: String,
    pub username: String,
    pub acct: String,
    pub display_name: String,
    pub locked: bool,
    pub created_at: String,
    pub followers_count: i64,
    pub following_count: i64,
    pub statuses_count: i64,
    pub note: String,
    pub url: String,
    pub avatar: String,
    pub avatar_static: String,
    pub header: String,
    pub header_static: String,
    pub emojis: Vec<Emoji>,
    pub source: Option<Source>,
}

#[derive(Serialize, Deserialize)]
pub struct ApplicationForm {
    // Properties according to
    // - https://docs.joinmastodon.org/api/entities/#application
    // - https://docs.joinmastodon.org/api/rest/apps/#post-api-v1-apps
    pub client_name: String,
    pub redirect_uris: String,
    pub scopes: String,
    pub website: Option<String>,
}

#[derive(Serialize)]
pub struct CreateApplicationResponse {
    pub id: String,
    pub name: String,
    pub website: Option<String>,
    pub redirect_uri: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Serialize, Deserialize)]
pub struct Attachment {
    pub id: String,
    #[serde(rename = "type")]
    pub _type: String,
    pub url: String,
    pub remote_url: Option<String>,
    pub preview_url: String,
    pub text_url: Option<String>,
    pub meta: Option<serde_json::Value>,
    pub description: Option<String>,
}

#[derive(Debug)]
pub struct AuthorizationHeader(pub String);

#[derive(Serialize, Deserialize, Clone)]
pub struct Emoji {
    pub shortcode: String,
    pub static_url: String,
    pub url: String,
    pub visible_in_picker: bool,
}

#[derive(Deserialize)]
pub struct HomeTimeline {
    pub max_id: Option<i64>,
    pub since_id: Option<i64>,
    pub min_id: Option<i64>,
    pub limit: Option<i64>,
}

#[derive(Deserialize)]
pub struct AccountTimeline {
    pub max_id: Option<i64>,
    pub since_id: Option<i64>,
    pub min_id: Option<i64>,
    pub limit: Option<i64>,
    pub exclude_replies: Option<bool>,
    pub pinned: Option<bool>,
}

#[derive(Serialize, Deserialize)]
pub struct Instance {
    // Properties according to
    // - https://docs.joinmastodon.org/api/entities/#instance
    pub uri: String,
    pub title: String,
    pub description: String,
    pub email: String,
    pub version: String,
    pub thumbnail: Option<String>,
    pub urls: serde_json::Value,
    pub stats: serde_json::Value,
    pub languages: Vec<String>,
    pub contact_account: Option<Account>,
    pub max_toot_chars: Option<i64>
}

#[derive(Serialize, Deserialize)]
pub struct Mention {
    pub url: String,
    pub username: String,
    pub acct: String,
    pub id: String,
}

#[derive(Serialize, Deserialize)]
pub struct Notification {
    pub id: String,
    #[serde(rename = "type")]
    pub _type: String,
    pub created_at: String,
    pub account: Account,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<Status>,
}

#[derive(Deserialize)]
pub struct NotificationsParams {
    max_id: Option<i64>,
    since_id: Option<i64>,
    min_id: Option<i64>,
    limit: Option<i64>,
}

impl Default for NotificationsParams {
    fn default() -> Self {
        NotificationsParams {
            max_id: None,
            since_id: None,
            min_id: None,
            limit: None,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct PublicTimeline {
    pub local: Option<bool>,
    pub only_media: Option<bool>,
    pub max_id: Option<i64>,
    pub since_id: Option<i64>,
    pub min_id: Option<i64>,
    pub limit: Option<i64>,
}

#[derive(Deserialize)]
pub struct RegistrationForm {
    // Properties acctording to
    // - https://docs.joinmastodon.org/api/rest/accounts/#post-api-v1-accounts
    pub username: String,
    pub email: String,
    pub password: String,
    // Optional values in Kibou, as they're not used by the backend (yet?)
    pub agreement: Option<String>,
    pub locale: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Relationship {
    // Properties according to
    // - https://docs.joinmastodon.org/api/entities/#relationship
    pub id: String,
    pub following: bool,
    pub followed_by: bool,
    pub blocking: bool,
    pub muting: bool,
    pub muting_notifications: bool,
    pub requested: bool,
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Source {
    pub note: String,
    pub fields: Vec<Field>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub privacy: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sensitive: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub language: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct StatusForm {
    pub status: Option<String>,
    pub in_reply_to_id: Option<String>,
    pub media_ids: Option<FormVec<String>>,
    pub sensitive: Option<bool>,
    pub spoiler_text: Option<String>,
    pub visibility: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct FormVec<T>(Vec<T>);

#[derive(Serialize, Deserialize)]
pub struct Tag {
    pub name: String,
    pub url: String,
    pub history: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Field {
    pub name: String,
    pub value: String,
}

#[derive(Serialize, Deserialize)]
pub struct Context {
    pub ancestors: Vec<Status>,
    pub descendants: Vec<Status>,
}

#[derive(Serialize)]
pub struct Filter {
    pub id: String,
    pub phrase: String,
    pub context: Vec<String>,
    pub expires_at: DateTime<Utc>,
    pub irreversible: bool,
    pub whole_word: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Hashtag {
    pub name: String,
}

#[derive(Serialize, Deserialize)]
pub struct SearchResult {
    pub accounts: Vec<Account>,
    pub statuses: Vec<Status>,
    pub hashtags: Vec<Hashtag>,
}

// Entities throw an empty error if they fail serializing. Entities should only fail silently, as
// these errors do not show up on the API. Furthermore should serializing a Mastodon-API entity
// never panic.

impl Account {
    pub fn acct(&self) -> String {
        let url = Url::parse(&self.url).unwrap();
        format!(
            "{username}@{host}",
            username = self.username,
            host = url.host_str().unwrap()
        )
    }
    pub fn from_actor(db: &PooledConnection, actor: Actor, include_source: bool) -> Account {
        let followees = count_followees(&db, actor.id).unwrap_or_else(|_| 0) as i64;
        let followers: Vec<serde_json::Value> =
            serde_json::from_value(actor.followers["activitypub"].to_owned())
                .unwrap_or_else(|_| Vec::new());

        let statuses = count_ap_notes_for_actor(&db, &actor.actor_uri).unwrap_or_else(|_| 0) as i64;

        let mut new_account = Account {
            id: actor.id.to_string(),
            username: actor.preferred_username.clone(),
            acct: actor.get_acct(),
            display_name: actor.username.unwrap_or_else(|| String::from("")),
            locked: false,
            created_at: actor.created.to_string(),
            followers_count: followers.len() as i64,
            following_count: followees,
            statuses_count: statuses,
            note: actor.summary.unwrap_or_else(|| String::from("")),
            url: actor.url.unwrap_or(actor.actor_uri),
            avatar: actor.icon.clone().unwrap_or_else(|| {
                format!(
                    "{}://{}/static/assets/default_avatar.png",
                    env::get_value("endpoint.base_scheme"),
                    env::get_value("endpoint.base_domain")
                )
            }),
            avatar_static: actor.icon.unwrap_or_else(|| {
                format!(
                    "{}://{}/static/assets/default_avatar.png",
                    env::get_value("endpoint.base_scheme"),
                    env::get_value("endpoint.base_domain")
                )
            }),
            header: format!(
                "{}://{}/static/assets/default_banner.png",
                env::get_value("endpoint.base_scheme"),
                env::get_value("endpoint.base_domain")
            ),
            header_static: format!(
                "{}://{}/static/assets/default_banner.png",
                env::get_value("endpoint.base_scheme"),
                env::get_value("endpoint.base_domain")
            ),
            emojis: vec![],
            source: None,
        };

        if include_source {
            new_account.source = Some(Source {
                privacy: None,
                sensitive: None,
                language: None,
                note: new_account.note.clone(),
                fields: vec![],
            });
        }

        return new_account;
    }
}

impl Notification {
    pub fn try_from(id: i64, activity: Activity, actor: &str) -> Result<Self, anyhow::Error> {
        let db = &PooledConnection(database::POOL.get().unwrap());

        return Notification::from_activitystreams(db, id, activity, actor);
    }

    fn from_activitystreams(
        db: &PooledConnection,
        id: i64,
        activity: Activity,
        actor: &str,
    ) -> Result<Self, anyhow::Error> {
        let ap_activity: activitypub::activity::Activity =
            serde_json::from_value(activity.data.clone()).expect("inner activity invalid");
        let created_at = activity.created;

        let account = controller::cached_account(db, &activity.actor)?;

        // An activity without an actor should never occur, but if it does,
        // this function needs to fail.
        let notification_type = match ap_activity._type.as_str() {
            "Follow" => String::from("follow"),
            "Create" => String::from("mention"),
            "Announce" => String::from("reblog"),
            "Like" => String::from("favourite"),
            _ => String::from(""),
        };
        let mut status: Option<Status> = None;

        if !notification_type.is_empty() {
            if &notification_type == "reblog" || &notification_type == "favourite" {
                status = controller::status_by_id(
                    db,
                    get_ap_object_by_id(db, ap_activity.object.as_str().unwrap())
                        .unwrap()
                        .id,
                    Some(actor),
                );
            } else if &notification_type == "mention" {
                // TODO: we should get it properly
                let reacted = ReactedActivity {
                    activity,
                    liked: false,
                    announced: false,
                };
                status = Some(
                    Status::try_from(db, reacted, Some(actor))
                        .expect("Failed to deserialize mention"),
                )
            }

            return Ok(Notification {
                id: id.to_string(),
                _type: notification_type,
                created_at: DateTime::<Utc>::from_utc(created_at, Utc).to_rfc3339(),
                account: account,
                status: status,
            });
        } else {
            Err(anyhow::anyhow!("Invalid empty notification type"))
        }
    }
}

impl ToString for AuthorizationHeader {
    fn to_string(&self) -> String {
        format!("{:?}", &self)
    }
}

lazy_static! {
    static ref MASTODON_API_ACCOUNT_CACHE: Arc<Mutex<lru::LruCache<String, Account>>> =
        Arc::new(Mutex::new(lru::LruCache::new(400)));
}

pub fn parse_authorization_header(header: &str) -> String {
    let header_vec: Vec<&str> = header.split(" ").collect();

    return header_vec[1].replace("\")", "").to_string();
}

/// Little wrapper around actix_web::web::block which always succeeds.
pub async fn block_ok<F, I>(f: F) -> I
where
    F: FnOnce() -> I + Send + 'static,
    I: Send + 'static,
{
    actix_web::web::block::<_, _, ()>(|| Ok(f())).await.unwrap()
}
