fn generate_id_with_namespace(timestamp_ms: i64, namespace: u16) -> i64 {
    let shifted_timestamp = timestamp_ms << 2;
    shifted_timestamp + (namespace as i64)
}

// Generate pseudo-unique time-depdendant identifier.
// We need it to order activities by time and at the same time
// we want to avoid collisions for the same time. We do roughly the
// same thing as Mastodon snowflakes where we take lower part of the
// timestamp and add hash part ot it. Right now you can guess it.
// TODO should probably use better hash with salt, otherwise we are
// too easy to attack because ids are predictable
pub fn generate_id(timestamp_ms: i64, namespace: &str) -> i64 {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    let mut hasher = DefaultHasher::new();

    namespace.hash(&mut hasher);
    let hash: u64 = hasher.finish();
    let namespace_num = hash as u16;
    generate_id_with_namespace(timestamp_ms, namespace_num)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_id() {
        let seq: u16 = 0;
        let timestamp: i64 = 1587733277761;

        assert_eq!(generate_id_with_namespace(timestamp, seq), 6350933111044)
    }
}
