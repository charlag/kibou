use crate::database::{
    models::{InsertActivity, QueryActivity, QueryActivityId},
    runtime_escape,
    schema::{activities, activities::dsl::*},
};
use crate::env;
use crate::models::id::generate_id;

use chrono::NaiveDateTime;
use diesel::{
    pg::PgConnection,
    query_dsl::{QueryDsl, RunQueryDsl},
    sql_query,
    sql_types::{Bool, Text},
    ExpressionMethods, QueryResult,
    OptionalExtension,
};

#[derive(Clone, Debug)]
pub struct Activity {
    pub id: i64,
    pub data: serde_json::Value,
    pub actor: String,
    pub created: NaiveDateTime,
}

impl From<QueryActivity> for Activity {
    fn from(query_activity: QueryActivity) -> Self {
        serialize_activity(query_activity)
    }
}

// Beware: This module depends on a lot of raw queries, which we should deprecate in the future. The
// only reason they're being used is because Diesel.rs does not support JSONB operators that are
// needed:
// {->, ->>, @>, ?}
//
// Related issue: https://git.cybre.club/kibouproject/kibou/issues/32
// Diesel.rs issue: https://github.com/diesel-rs/diesel/issues/44

pub fn count_ap_object_replies_by_id(
    db_connection: &PgConnection,
    object_id: &str,
) -> Result<usize, diesel::result::Error> {
    match sql_query(format!(
        "SELECT id FROM activities WHERE data @> '{{\"object\": {{\"inReplyTo\": \"{}\"}}}}';",
        runtime_escape(object_id)
    ))
    .load::<QueryActivityId>(db_connection)
    {
        Ok(activity_arr) => Ok(activity_arr.len()),
        Err(e) => Err(e),
    }
}

pub fn count_ap_object_reactions_by_id(
    db_connection: &PgConnection,
    object_id: &str,
    reaction: &str,
) -> Result<usize, diesel::result::Error> {
    match sql_query(format!(
        "SELECT id FROM activities WHERE data @> '{{\"type\": \"{reaction_type}\"}}' \
         AND data @> '{{\"object\": \"{id}\"}}';",
        reaction_type = runtime_escape(reaction),
        id = runtime_escape(object_id)
    ))
    .load::<QueryActivityId>(db_connection)
    {
        Ok(activity_arr) => Ok(activity_arr.len()),
        Err(e) => Err(e),
    }
}

pub fn count_ap_notes_for_actor(
    db_connection: &PgConnection,
    actor: &str,
) -> Result<usize, diesel::result::Error> {
    match sql_query(format!(
        "SELECT id \
         FROM activities \
         WHERE data @> '{{\"actor\": \"{actor}\", \
         \"type\": \"Create\", \
         \"object\": {{\"type\": \"Note\"}}}}' \
         AND ((data->>'to')::jsonb ? 'https://www.w3.org/ns/activitystreams#Public' \
         OR (data->>'cc')::jsonb ? 'https://www.w3.org/ns/activitystreams#Public');",
        actor = runtime_escape(actor)
    ))
    .load::<QueryActivityId>(db_connection)
    {
        Ok(activity_arr) => Ok(activity_arr.len()),
        Err(e) => Err(e),
    }
}

pub fn count_local_ap_notes(db_connection: &PgConnection) -> Result<usize, diesel::result::Error> {
    match sql_query(format!(
        "SELECT id \
         FROM activities \
         WHERE data->>'type' = 'Create' \
         AND data->'object'->>'type' = 'Note' \
         AND data->>'actor' LIKE '{base_scheme}://{base_domain}/%' \
         AND ((data->>'to')::jsonb ? 'https://www.w3.org/ns/activitystreams#Public' \
         OR (data->>'cc')::jsonb ? 'https://www.w3.org/ns/activitystreams#Public')",
        base_scheme = env::get_value("endpoint.base_scheme"),
        base_domain = env::get_value("endpoint.base_domain")
    ))
    .clone()
    .load::<QueryActivityId>(db_connection)
    {
        Ok(activity_arr) => Ok(activity_arr.len()),
        Err(e) => Err(e),
    }
}

pub fn get_activities_by_id(
    db_connection: &PgConnection,
    ids: Vec<i64>,
) -> Result<Vec<Activity>, diesel::result::Error> {
    let parsed_ids: Vec<String> = ids.iter().map(|num| num.to_string()).collect();
    sql_query(format!(
        "SELECT * FROM activities WHERE id = ANY(ARRAY[{}]);",
        parsed_ids.join(", ")
    ))
    .load::<QueryActivity>(db_connection)
    .map(|loaded_acivities| {
        loaded_acivities
            .iter()
            .map(|activity| serialize_activity(activity.clone()))
            .collect()
    })
}

// I tried to have one type and use `embed` together with
// `deserialize_as` but it doesn't work in either order.
#[derive(QueryableByName)]
struct ReactedQueryActivity {
    #[diesel(embed)]
    activity: QueryActivity,
    #[sql_type = "Bool"]
    liked: bool,
    #[sql_type = "Bool"]
    announced: bool,
}

pub struct ReactedActivity {
    pub activity: Activity,
    pub liked: bool,
    pub announced: bool,
}

impl From<ReactedQueryActivity> for ReactedActivity {
    fn from(query_activity: ReactedQueryActivity) -> Self {
        ReactedActivity {
            activity: query_activity.activity.into(),
            liked: query_activity.liked,
            announced: query_activity.announced,
        }
    }
}

pub fn get_activities_with_reacted_status(
    db_connection: &PgConnection,
    ids: Vec<i64>,
    actor: Option<&str>,
) -> Result<Vec<ReactedActivity>, diesel::result::Error> {
    let parsed_ids: Vec<String> = ids.iter().map(|num| num.to_string()).collect();
    sql_query(format!(
        "SELECT a.*, (f.id IS NOT NULL) as liked, (b.id IS NOT NULL) as announced \
         FROM activities a \
         LEFT JOIN activities f ON f.data->>'object' = (a.data->'object'->>'id'::text) \
         AND f.data->>'actor' = '{actor}' AND f.data->>'type' = 'Like' \
         LEFT JOIN activities b ON b.data->>'object' = (a.data->'object'->>'id'::text) \
         AND b.data->>'actor' = '{actor}' AND b.data->>'type' = 'Announce' \
         WHERE a.id = ANY(ARRAY[{ids}]::int8[]) \
         ORDER BY a.id DESC;",
        actor = runtime_escape(actor.unwrap_or("")),
        ids = parsed_ids.join(", ")
    ))
    .load::<ReactedQueryActivity>(db_connection)
    .map(serialize_reacted)
}

fn serialize_reacted(loaded: Vec<ReactedQueryActivity>) -> Vec<ReactedActivity> {
    loaded.into_iter().map(|a| a.into()).collect()
}

pub fn get_activity(
    db_connection: &PgConnection,
    activity_id: i64,
) -> Option<Activity> {
    activities.find(activity_id)
        .first::<QueryActivity>(db_connection)
        .map(serialize_activity)
        .optional()
        .unwrap()
}

pub fn get_activity_by_id(
    db_connection: &PgConnection,
    activity_id: i64,
) -> Result<Activity, diesel::result::Error> {
    match activities
        .filter(id.eq(activity_id))
        .limit(1)
        .first::<QueryActivity>(db_connection)
    {
        Ok(activity) => Ok(serialize_activity(activity)),
        Err(e) => Err(e),
    }
}

pub fn get_ap_activity_by_id(
    db: &PgConnection,
    activity_id: &str,
) -> Result<Activity, diesel::result::Error> {
    sql_query("SELECT * FROM activities WHERE data->>'id' = $1 LIMIT 1;")
        .bind::<Text, _>(activity_id)
        .get_result::<QueryActivity>(db)
        .map(serialize_activity)
}

pub fn get_reacted_activity_by_object_id(
    db: &PgConnection,
    object_id: &str,
    actor: Option<&str>,
    activty_type: &str,
) -> Result<ReactedActivity, diesel::result::Error> {
    sql_query(
        "SELECT a.*, (f.id IS NOT NULL) as liked, (b.id IS NOT NULL) as announced \
         FROM activities a \
         LEFT JOIN activities f ON f.data->>'object' = (a.data->'object'->>'id'::text) \
         AND f.data->>'actor' = $1 AND f.data->>'type' = 'Like' \
         LEFT JOIN activities b ON b.data->>'object' = (a.data->'object'->>'id'::text) \
         AND b.data->>'actor' = $1 AND b.data->>'type' = 'Announce' \
         WHERE a.data->'object'->>'id' = $2 AND a.data->>'type' = $3 \
         LIMIT 1;",
    )
    .bind::<Text, _>(actor.unwrap_or(""))
    .bind::<Text, _>(object_id)
    .bind::<Text, _>(activty_type)
    .get_result::<ReactedQueryActivity>(db)
    .map(|a| a.into())
}

pub fn get_ap_object_by_id(
    db_connection: &PgConnection,
    object_id: &str,
) -> Result<Activity, diesel::result::Error> {
    match sql_query(format!(
        "SELECT * FROM activities WHERE data @> '{{\"object\": {{\"id\": \"{}\"}}}}' LIMIT 1;",
        runtime_escape(object_id)
    ))
    .load::<QueryActivity>(db_connection)
    {
        Ok(activity) => {
            if !activity.is_empty() {
                let new_activity = std::borrow::ToOwned::to_owned(&activity[0]);
                Ok(serialize_activity(new_activity))
            } else {
                Err(diesel::result::Error::NotFound)
            }
        }
        Err(e) => Err(e),
    }
}

pub fn get_ap_activity_by_object_id(
    db_connection: &PgConnection,
    object_id: &str,
    actitivity_type: &str,
) -> Result<Activity, diesel::result::Error> {
    sql_query(
        r#"SELECT * FROM activities
WHERE (data->>'object' = $1 OR data->'object'->>'id' = $1)
AND data->>'type' = $2
LIMIT 1;"#,
    )
    .bind::<Text, _>(object_id)
    .bind::<Text, _>(actitivity_type)
    .load::<QueryActivity>(db_connection)
    .and_then(|activity| {
        if !activity.is_empty() {
            let new_activity = std::borrow::ToOwned::to_owned(&activity[0]);
            Ok(serialize_activity(new_activity))
        } else {
            Err(diesel::result::Error::NotFound)
        }
    })
}

pub fn get_activity_by_actor_by_object_id(
    db_connection: &PgConnection,
    uri_of_actor: &str,
    object_id: &str,
    actitivity_type: &str,
) -> Result<Activity, diesel::result::Error> {
    sql_query(
        r#"SELECT * FROM activities
WHERE (data->>'object' = $1 OR data->'object'->>'id' = $1)
AND data->>'type' = $2
AND actor_uri = $3
LIMIT 1;"#,
    )
    .bind::<Text, _>(object_id)
    .bind::<Text, _>(actitivity_type)
    .bind::<Text, _>(uri_of_actor)
    .load::<QueryActivity>(db_connection)
    .and_then(|activity| {
        if !activity.is_empty() {
            let new_activity = std::borrow::ToOwned::to_owned(&activity[0]);
            Ok(serialize_activity(new_activity))
        } else {
            Err(diesel::result::Error::NotFound)
        }
    })
}

pub fn get_ap_object_replies_by_id(
    db_connection: &PgConnection,
    object_id: &str,
    actor: Option<&str>,
) -> Result<Vec<ReactedActivity>, diesel::result::Error> {
    sql_query(format!(
        "SELECT a.*, (f.id IS NOT NULL) as liked, (b.id IS NOT NULL) as announced \
         FROM activities a \
         LEFT JOIN activities f ON f.data->>'object' = (a.data->'object'->>'id'::text) \
         AND f.data->>'actor' = '{actor}' AND f.data->>'type' = 'Like' \
         LEFT JOIN activities b ON b.data->>'object' = (a.data->'object'->>'id'::text) \
         AND b.data->>'actor' = '{actor}' AND b.data->>'type' = 'Announce' \
         WHERE a.data->'object'->>'inReplyTo' = '{in_reply_to}';",
        actor = runtime_escape(actor.unwrap_or("")),
        in_reply_to = runtime_escape(object_id)
    ))
    .load::<ReactedQueryActivity>(db_connection)
    .map(serialize_reacted)
}

pub fn type_exists_for_object_id(
    db_connection: &PgConnection,
    _type: &str,
    actor: &str,
    object_id: &str,
) -> Result<bool, diesel::result::Error> {
    // Should use bind, first(), optional() or count but probably this whole
    // method makes no sense and we should remove it.
    sql_query(format!(
        "SELECT * FROM activities WHERE data @> '{{\"type\": \"{}\", \"actor\": \"{}\", \"object\": \"{}\"}}' LIMIT 1;",
        _type, runtime_escape(actor), runtime_escape(object_id)
    ))
        .load::<QueryActivity>(db_connection)
        .map(|activity| !activity.is_empty())
}

pub fn serialize_activity(sql_activity: QueryActivity) -> Activity {
    Activity {
        id: sql_activity.id,
        data: sql_activity.data,
        actor: sql_activity.actor_uri,
        created: sql_activity.created,
    }
}

pub fn deserialize_activity(activity: &Activity) -> InsertActivity {
    InsertActivity {
        id: generate_id(activity.created.timestamp_millis(), &activity.actor),
        data: &activity.data,
        actor_uri: &activity.actor,
    }
}

pub fn insert_activity(db_connection: &PgConnection, activity: Activity) -> Activity {
    let new_activity = deserialize_activity(&activity);

    serialize_activity(
        diesel::insert_into(activities::table)
            .values(&new_activity)
            .get_result(db_connection)
            .expect("Error creating activity"),
    )
}

pub fn delete_ap_activity_by_id(
    db_connection: &PgConnection,
    activity_id: &str,
) -> QueryResult<()> {
    sql_query(format!(
        "DELETE FROM activities WHERE data->>'id' = '{}';",
        runtime_escape(&activity_id)
    ))
    .execute(db_connection)?;
    Ok(())
}

pub fn delete_ap_object_by_id(db_connection: &PgConnection, object_id: &str) -> QueryResult<()> {
    sql_query("DELETE FROM activities WHERE data->'object'->>'id' = $1;")
        .bind::<Text, _>(object_id)
        .execute(db_connection)?;
    Ok(())
}

pub fn increase_like(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, like_count}', \
         (COALESCE(data->>'like_count','0')::int + 1)::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
    .bind::<Text, _>(object_id)
    .execute(db)?;
    Ok(())
}

pub fn decrease_like(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, like_count}', \
         (GREATEST(0, (data->>'like_count')::int - 1))::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
    .bind::<Text, _>(object_id)
    .execute(db)?;
    Ok(())
}

pub fn increase_announce(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, announce_count}', \
         (COALESCE(data->>'announce_count','0')::int + 1)::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
    .bind::<Text, _>(object_id)
    .execute(db)?;
    Ok(())
}

pub fn decrease_announce(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, announce_count}', \
         (GREATEST(0, (data->>'announce_count')::int - 1))::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
    .bind::<Text, _>(object_id)
    .execute(db)?;
    Ok(())
}

pub fn increase_reply(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, reply_count}', \
         (COALESCE(data->>'reply_count','0')::int + 1)::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
        .bind::<Text, _>(object_id)
        .execute(db)?;
    Ok(())
}

pub fn decrease_reply(db: &PgConnection, object_id: &str) -> Result<(), diesel::result::Error> {
    sql_query(
        "UPDATE activities SET \
         data = jsonb_set(data, '{object, reply_count}', \
         (GREATEST(0, (data->>'reply_count')::int - 1))::text::jsonb) \
         WHERE data->'object'->>'id' = $1 AND data->>'type' = 'Create';",
    )
        .bind::<Text, _>(object_id)
        .execute(db)?;
    Ok(())
}

/// Get all activities which target specified one and have specified type.
pub fn get_all_activities_with_target(
    db: &PgConnection,
    target_object_id: &str,
    activities_type: &str,
) -> Result<Vec<Activity>, diesel::result::Error> {
    let results = sql_query(
        "SELECT * FROM activities \
         WHERE (data->>'object' = $1 OR data->'object'->>'id' = $1) AND data->>'type' = $2",
    )
    .bind::<Text, _>(target_object_id)
    .bind::<Text, _>(activities_type)
    .load::<QueryActivity>(db)?
    .into_iter()
    .map(serialize_activity)
    .collect();
    Ok(results)
}
