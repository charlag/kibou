use crate::env;
use crate::activitypub;
use crate::database;

pub fn file_url(file_name: &str) -> String {
    format!(
        "{}://{}/public/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        file_name
    )
}

impl Into<activitypub::Attachment> for database::models::MediaAttachment {
    fn into(self) -> activitypub::Attachment {
        activitypub::Attachment {
            _type: activitypub::attachment_type(&self.content_type).expect("Unknown type for attachment"),
            content: None,
            url: file_url(&self.file_name),
            name: self.description, // this sucks but that's how Mastodon does it
            mediaType: Some(self.content_type),
        }
    }
}
