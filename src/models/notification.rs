use crate::database::{models::InsertNotification, schema::notifications};
use crate::models::{
    actor::Actor,
    activity::Activity,
};

use chrono::{NaiveDateTime, Utc};
use diesel::{
    sql_query,
    sql_types::{BigInt, Jsonb, Text, Timestamp},
    PgConnection, RunQueryDsl,
};
use serde_json::Value as JsonValue;

#[derive(PartialEq, Clone)]
pub struct Notification {
    id: i64,
    activity_id: i64,
    actor_id: i64,
    created: NaiveDateTime,
    modified: NaiveDateTime,
}

impl Notification {
    pub fn new(activity_id: i64, actor_id: i64) -> Notification {
        Notification {
            id: 0,
            activity_id: activity_id,
            actor_id: actor_id,
            created: Utc::now().naive_utc(),
            modified: Utc::now().naive_utc(),
        }
    }
}

#[derive(QueryableByName, Debug)]
pub struct QueryNotification {
    #[sql_type = "BigInt"]
    pub id: i64,
    #[sql_type = "BigInt"]
    pub activity_id: i64,
    #[sql_type = "Jsonb"]
    pub data: JsonValue,
    #[sql_type = "Text"]
    pub actor: String,
    #[sql_type = "Timestamp"]
    pub created: NaiveDateTime
}

impl Into<Activity> for QueryNotification {
    fn into(self) -> Activity {
        Activity {
            id: self.activity_id,
            data: self.data,
            actor: self.actor,
            created: self.created,
        }
    }
}

pub fn notifications_for_actor(
    db_connection: &PgConnection,
    actor: &Actor,
    max_id: Option<i64>,
    since_id: Option<i64>,
    min_id: Option<i64>,
    limit: Option<i64>,
) -> Result<Vec<QueryNotification>, diesel::result::Error> {
    // This probably can be implemented with diesel dsl
    let id_ord = if let Some(min_id) = min_id {
        format!("AND notifications.id > {} ORDER BY id ASC", min_id)
    } else {
        let mut cond = String::new();
        if let Some(max_id) = max_id {
            cond.push_str(&format!("AND notifications.id < {} ", max_id))
        }
        if let Some(since_id) = since_id {
            cond.push_str(&format!("AND notifications.id > {} ", since_id))
        }
        cond.push_str("ORDER BY notifications.id DESC");
        cond
    };

    // TODO: we probably need to reverse if min_id
    let query = format!(
        "SELECT notifications.id as id, activities.id as activity_id, \
         activities.data, activities.actor_uri as actor, activities.created \
         FROM notifications \
         LEFT JOIN activities ON activities.id = notifications.activity_id
         WHERE \
         notifications.actor_id = '{actor_id}' \
         {id_ord} \
         LIMIT {limit};",
        actor_id = actor.id,
        id_ord = id_ord,
        limit = limit.unwrap_or_else(|| 20).to_string()
    );
    sql_query(query)
        .load::<QueryNotification>(db_connection)
}

pub fn insert(db_connection: &PgConnection, notification: Notification) {
    diesel::insert_into(notifications::table)
        .values(InsertNotification {
            activity_id: notification.activity_id,
            actor_id: notification.actor_id,
            created: notification.created,
            modified: notification.modified,
        })
        .execute(db_connection)
        .expect("Error creating notification");
}
