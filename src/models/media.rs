use crate::models::actor::Actor;
use crate::database::{self, models::MediaAttachment, schema::media_attachments, PooledConnection};

use diesel::prelude::*;

pub fn save_media(
    db: &PooledConnection,
    account: &Actor,
    content_type: Option<&str>,
    _passed_file_name: Option<&str>,
    file_name: &str,
) -> Result<MediaAttachment, anyhow::Error> {
    let content_type = content_type.as_ref().unwrap().to_string();
    // TODO: use passed file name too
    let media_attachment = database::models::NewMediaAttachment {
        actor_id: account.id,
        content_type: &content_type,
        file_name,
        size: 0, // TODO
        description: "".into(),
    };

    let inserted_attachment: database::models::MediaAttachment =
        diesel::insert_into(media_attachments::table)
            .values(&media_attachment)
            .get_result(&db.0)
            .unwrap();
    Ok(inserted_attachment)
}
