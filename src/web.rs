pub mod federator;
pub mod http_signatures;
pub mod multipart;
pub mod html;

mod http;
mod redirect;
mod template;
mod form_or_json;
mod either;

pub use redirect::Redirect;
pub use redirect::RedirectError;
pub use template::Template;
pub use form_or_json::FormOrJson;
pub use either::Either;

use actix_web::client::{Client};
use anyhow::anyhow;

pub async fn fetch_remote_object(url: &str) -> Result<String, anyhow::Error> {
    let client = Client::default();

    let mut response = client.get(url)
        .header("Accept", "application/activity+json")
        .send()
        .await
        .map_err(|e| anyhow!("Error fetching {}: {}", url, e))?;

    let body = response.body().await
        .map_err(|e| anyhow!("Error getting body for {}: {}", url, e))?;

    let string = String::from_utf8(body.to_vec()).unwrap();
    Ok(string)
}
