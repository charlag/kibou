use crate::models::actor;
use crate::database::{
    self,
    models::QueryOAuthAuthorization,
    schema::{oauth_authorizations, oauth_authorizations::dsl::*},
};
use crate::oauth::application::get_application_by_client_id;
use crate::web::{Redirect, Template};

use chrono::{prelude::*, Duration, NaiveDateTime};
use diesel::{pg::PgConnection, ExpressionMethods, QueryDsl, RunQueryDsl};
use openssl::bn::BigNum;
use openssl::bn::MsbOption;
use serde::{Deserialize, Serialize};

pub struct Authorization {
    pub application: i64,
    pub actor: String,
    pub code: String,
    pub valid_until: String,
}

#[derive(Serialize, Deserialize)]
pub struct UserForm {
    pub username: String,
    pub password: String,
}

fn serialize_authorization(sql_authorization: QueryOAuthAuthorization) -> Authorization {
    Authorization {
        application: sql_authorization.application,
        actor: sql_authorization.actor,
        code: sql_authorization.code,
        valid_until: sql_authorization.valid_until.to_string(),
    }
}

pub fn get_authorization_by_code(
    db_connection: &PgConnection,
    _code: String,
) -> Result<Authorization, diesel::result::Error> {
    match oauth_authorizations
        .filter(code.eq(_code))
        .limit(1)
        .first::<QueryOAuthAuthorization>(db_connection)
    {
        Ok(authorization) => Ok(serialize_authorization(authorization)),
        Err(e) => Err(e),
    }
}

pub fn handle_user_authorization(
    user_form: UserForm,
    client_id: Option<String>,
    _response_type: Option<String>,
    redirect_uri: Option<String>,
    state: Option<String>,
) -> Result<Redirect, Template> {
    let db_connection = database::establish_connection();

    if client_id.is_some() && redirect_uri.is_some() {
        match actor::authorize(&db_connection, &user_form.username, user_form.password) {
            Ok(true) => match get_application_by_client_id(&db_connection, client_id.unwrap()) {
                Ok(serialized_application) => {
                    let redirect_uri = redirect_uri.unwrap();
                    let auth_code =
                        authorize_application(user_form.username, serialized_application.id);
                    let state = match state {
                        Some(value) => format!("&state={}", value),
                        None => String::from(""),
                    };
                    let symbol = if redirect_uri.contains("?") {
                        "&".to_string()
                    } else {
                        "?".to_string()
                    };

                    Ok(Redirect::found(format!(
                        "{uri}{symbol}code={code}{state}",
                        uri = redirect_uri,
                        symbol = symbol,
                        code = auth_code,
                        state = state
                    )))
                }
                Err(_) => Err(user_authorization_error("Invalid application credentials")),
            },
            _ => Err(user_authorization_error("Invalid login data!")),
        }
    } else {
        Err(user_authorization_error("Invalid OAuth parameters"))
    }
}

fn user_authorization_error(error_context: &str) -> Template {
    let mut context = tera::Context::new();
    context.insert("error_context", error_context);

    Template::with_context("oauth_authorization.html.tera", context)
}

pub fn authorize_application(_actor: String, application_id: i64) -> String {
    let db_connection = database::establish_connection();
    let mut hex_num: BigNum = BigNum::new().unwrap();
    let utc_time: chrono::DateTime<Utc> = Utc::now();
    let expiration_date: chrono::DateTime<Utc> = utc_time + Duration::days(30);
    hex_num
        .rand(256, MsbOption::MAYBE_ZERO, true)
        .expect("Error generating authorization code");

    let new_authorization = Authorization {
        application: application_id,
        actor: _actor,
        code: hex_num.to_string(),
        valid_until: expiration_date.timestamp().to_string(),
    };

    insert(&db_connection, &new_authorization);
    return new_authorization.code;
}

fn insert(db_connection: &PgConnection, authorization: &Authorization) {
    let parsed_expiration_date: NaiveDateTime =
        chrono::NaiveDateTime::from_timestamp(authorization.valid_until.parse::<i64>().unwrap(), 0);

    let new_authorization = (
        application.eq(&authorization.application),
        actor.eq(&authorization.actor),
        code.eq(&authorization.code),
        valid_until.eq(&parsed_expiration_date),
    );

    diesel::insert_into(oauth_authorizations::table)
        .values(new_authorization)
        .execute(db_connection)
        .expect("Error creating oauth authorization");
}
