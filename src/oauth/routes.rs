use crate::oauth::authorization::{handle_user_authorization, UserForm};
use crate::oauth::token::{get_token, TokenForm};
use crate::web::{Redirect, Template, FormOrJson};

use actix_web::{get, post, web::{self, Json}, Either};

#[get("/oauth/authorize")]
pub async fn authorize() -> Template {
    Template::with_context("oauth_authorization.html.tera", tera::Context::new())
}

#[derive(serde::Deserialize)]
pub struct AuthorizeQuery {
    client_id: Option<String>,
    response_type: Option<String>,
    redirect_uri: Option<String>,
    state: Option<String>,
    #[allow(dead_code)]
    scope: Option<String>,
}

// TODO: scope
#[post("/oauth/authorize")]
pub async fn authorize_result(
    query: web::Query<AuthorizeQuery>,
    form: FormOrJson<UserForm>,
) -> Either<Redirect, Template> {
    let AuthorizeQuery {
        client_id,
        response_type,
        redirect_uri,
        state,
        ..
    } = query.into_inner();
    match handle_user_authorization(
        form.into_inner(),
        client_id,
        response_type,
        redirect_uri,
        state,
    ) {
        Ok(redirect) => Either::A(redirect),
        Err(template) => Either::B(template),
    }
}

#[post("/oauth/token")]
pub async fn token(form: FormOrJson<TokenForm>) -> Json<serde_json::Value> {
    Json(get_token(form.into_inner()))
}
