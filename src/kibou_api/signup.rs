use regex::Regex;
use crate::mastodon_api::RegistrationForm;
use crate::models::actor;
use crate::database;
use crate::env;
use chrono::Utc;
use std::fmt;

#[derive(Debug)]
pub enum RegisterError {
    InvalidData(String),
    Other
}

impl fmt::Display for RegisterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::InvalidData(msg) => write!(f, "Register error: invalid data: {}", msg),
            Self::Other => write!(f, "Register error: other"),
        }
    }
}

impl std::error::Error for RegisterError {
}

pub fn register_account(form: &RegistrationForm) -> Result<actor::Actor, RegisterError> {
    let email_regex = Regex::new(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$").unwrap();
    let username_regex = Regex::new(r"^[A-Za-z0-9_]{1,32}$").unwrap();

    if !username_regex.is_match(&form.username) {
        return Err(RegisterError::InvalidData("username is invalid".to_owned()));
    }
    if !email_regex.is_match(&form.email) {
        return Err(RegisterError::InvalidData("email is invalid".to_owned()));
    }

    let database = database::establish_connection();
    let uri = format!(
        "{base_scheme}://{base_domain}/actors/{username}",
        base_scheme = env::get_value("endpoint.base_scheme"),
        base_domain = env::get_value("endpoint.base_domain"),
        username = form.username
    );
    let mut new_actor = actor::Actor {
        id: 0,
        email: Some(form.email.to_string()),
        password: Some(form.password.to_string()),
        actor_uri: uri.clone(),
        username: Some(form.username.to_string()),
        preferred_username: form.username.to_string(),
        summary: None,
        followers: serde_json::json!({"activitypub": []}),
        inbox: None,
        icon: None,
        local: true,
        keys: serde_json::json!({}),
        created: Utc::now().naive_utc(),
        modified: Utc::now().naive_utc(),
        url: Some(uri),
    };

    actor::create_actor(&database, &mut new_actor, false);

    match actor::get_local_actor_by_preferred_username(&database, &form.username) {
        Ok(actor) => Ok(actor),
        Err(_) => Err(RegisterError::Other),
    }
}
