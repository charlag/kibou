//! Kibou_API provides a common application layer between data reprentations of modules such as
//! ActivityPub, Mastodon_API and internal reprentations.
//!
//! Furthermore Kibou_API will implement endpoints which are unique to the Kibou backend.
//!
pub mod timeline;
pub mod signup;

use crate::models::{
    activity::{
        get_activity_by_actor_by_object_id, get_activity_by_id, get_all_activities_with_target,
        get_ap_activity_by_id, get_ap_activity_by_object_id, get_ap_object_by_id, increase_reply,
        type_exists_for_object_id,
    },
    actor::{
        get_actor_by_acct, get_actor_by_id, get_actor_by_uri, get_actors_by_uri,
        is_actor_followed_by, Actor,
    },
    notification::{self, Notification},
};
use crate::activitypub::{
    self,
    activity::{serialize_from_internal_activity, Tag},
    actor::{add_follow, remove_follow},
    controller as ap_controller,
};
use crate::database::{self, models::MediaAttachment, PooledConnection};
use crate::mastodon_api;
use crate::web::{federator, html};

use diesel::PgConnection;
use regex::Regex;
use serde_json::{json, Value as JsonValue};

pub fn follow(sender: &str, receipient: &str) {
    let database = database::establish_connection();
    let serialized_actor: Actor = get_actor_by_uri(&database, &sender).unwrap();

    if sender != receipient {
        match get_actor_by_uri(&database, &receipient) {
            Ok(followee) => {
                if !is_actor_followed_by(&database, &followee, &sender).unwrap() {
                    let activitypub_activity_follow =
                        ap_controller::follow(&database, sender, receipient);

                    if !followee.local {
                        federator::enqueue(
                            &serialized_actor,
                            serde_json::json!(&activitypub_activity_follow),
                            vec![followee.inbox.unwrap()],
                        );
                    } else {
                        add_follow(receipient, sender, &activitypub_activity_follow.id);
                        // TODO: bad but we need to refactor all the places where we create activities
                        let internal_activity = get_ap_activity_by_id(
                            &database,
                            &activitypub_activity_follow.id,
                        )
                            .unwrap();
                        let new_notification = Notification::new(internal_activity.id, followee.id);
                        notification::insert(&database, new_notification);
                        debug!("Inserted notification");
                    }
                }
            }
            Err(_) => (),
        }
    }
}

pub fn react(actor: i64, _type: &str, object_id: &str) {
    let database = database::establish_connection();
    let serialized_actor: Actor = get_actor_by_id(&database, actor).expect("Actor should exist!");

    if !type_exists_for_object_id(&database, _type, &serialized_actor.actor_uri, object_id).unwrap()
    {
        match get_ap_object_by_id(&database, object_id) {
            Ok(activity) => {
                let ap_activity = serialize_from_internal_activity(activity);

                let mut to: Vec<String> = ap_activity.to.clone();
                let mut cc: Vec<String> = ap_activity.cc.clone();
                let mut inboxes: Vec<String> = Vec::new();

                to.retain(|x| x != &format!("{}/followers", &ap_activity.actor));
                cc.retain(|x| x != &format!("{}/followers", &ap_activity.actor));

                if to.contains(&"https://www.w3.org/ns/activitystreams#Public".to_string()) {
                    cc.push(format!("{}/followers", serialized_actor.actor_uri));
                    inboxes = handle_follower_inboxes(&database, &serialized_actor.followers);
                } else if cc.contains(&"https://www.w3.org/ns/activitystreams#Public".to_string()) {
                    to.push(format!("{}/followers", serialized_actor.actor_uri));
                    inboxes = handle_follower_inboxes(&database, &serialized_actor.followers);
                }

                match get_actor_by_uri(&database, &ap_activity.actor) {
                    Ok(foreign_actor) => {
                        if !foreign_actor.local {
                            to.push(foreign_actor.actor_uri);
                            inboxes.push(foreign_actor.inbox.unwrap());
                        }
                    }
                    Err(_) => eprintln!(
                        "Error: Actor '{}' should exist in order to create a reaction!",
                        &ap_activity.actor
                    ),
                }

                to.dedup();
                cc.dedup();
                inboxes.dedup();

                match _type {
                    "Announce" => {
                        let new_activity = ap_controller::announce(
                            &database,
                            &serialized_actor.actor_uri,
                            object_id,
                            to,
                            cc,
                        );
                        federator::enqueue(
                            &serialized_actor,
                            serde_json::json!(&new_activity),
                            inboxes,
                        );
                    }
                    "Like" => {
                        let new_activity = ap_controller::like(
                            &database,
                            &serialized_actor.actor_uri,
                            object_id,
                            to,
                            cc,
                        );
                        federator::enqueue(
                            &serialized_actor,
                            serde_json::json!(&new_activity),
                            inboxes,
                        );
                    }
                    _ => (),
                }
            }
            Err(e) => {
                eprintln!("Did not find activity to react: {}", e);
            }
        }
    } else {
        eprintln!("Already reacted to {:?} with {:?}", object_id, _type);
    }
}

pub fn undo_react(db: &PooledConnection, actor_id: i64, _type: &str, object_id: &str) {
    let actor: Actor = get_actor_by_id(&db, actor_id).expect("Actor should exist!");

    let reaction_activity =
        match get_activity_by_actor_by_object_id(&db, &actor.actor_uri, object_id, _type) {
            Ok(activity) => activity,
            Err(diesel::result::Error::NotFound) => {
                println!("Nothing to undo!");
                return;
            }
            Err(e) => panic!("Error while fetching reaction activity: {:?}", e),
        };

    // We need to find not the reaction activity but acitivty the the original
    // activity it was reacted to (actually we just need an object). That's why
    // we search for "create" activity as this is the only one you can react to.
    let reacted_activity = match get_ap_activity_by_object_id(&db, object_id, "Create") {
        Ok(activity) => activity,
        Err(_) => {
            println!("Don't have object to undo reaction on: {}", object_id);
            return;
        }
    };
    let reaction_ap_activity = serialize_from_internal_activity(reaction_activity);

    match _type {
        "Like" => {
            let new_activity = ap_controller::undo_like(db, &actor.actor_uri, reaction_ap_activity);
            match get_actor_by_uri(&db, &reacted_activity.actor) {
                Ok(original_actor) if !original_actor.local => {
                    if let Some(inbox) = original_actor.inbox {
                        federator::enqueue(&actor, serde_json::json!(&new_activity), vec![inbox]);
                    } else {
                        println!("Actor has no inbox");
                    }
                }
                _ => {
                    println!("Original actor is local orcannot be found, skipping federation");
                }
            }
        }
        "Announce" => {
            let new_activity =
                ap_controller::undo_announce(db, &actor.actor_uri, reaction_ap_activity);
            let reacted_ap_activity = serialize_from_internal_activity(reacted_activity);

            // Collecting original author, mentions and anyone who announced it too
            let mut recipients: Vec<_> = reacted_ap_activity
                .to
                .into_iter()
                .chain(reacted_ap_activity.cc.into_iter())
                .collect();
            recipients.push(reacted_ap_activity.actor);

            let reblogs = get_all_activities_with_target(&db, object_id, "Announce")
                .unwrap_or_else(|e| {
                    eprintln!("Failed to get reblogs: {:?}", e);
                    vec![]
                });
            for reblog in reblogs {
                recipients.push(reblog.actor);
            }
            recipients.dedup();
            let host = crate::env::get_value("endpoint.host");
            activitypub::controller::retain_remote_actors(&mut recipients, &host);

            let recipient_inboxes = get_actors_by_uri(&db, recipients)
                .unwrap()
                .into_iter()
                .filter_map(|a| a.inbox)
                .collect();
            federator::enqueue(&actor, serde_json::json!(&new_activity), recipient_inboxes);
        }
        _ => eprintln!("Cannot undo reaction: {}", _type),
    }
}

pub fn public_activities(pooled_connection: &PooledConnection, actor: Option<&str>) -> JsonValue {
    match timeline::public_activities(pooled_connection) {
        Ok(activities) => {
            let statuses =
                mastodon_api::controller::hydrated_statuses(pooled_connection, activities, actor);
            json!(statuses)
        }
        Err(_) => json!({"error": "An error occured while querying public activities"}),
    }
}

pub fn status_build(
    db: &PooledConnection,
    actor: &str,
    dirty_content: String,
    visibility: &str,
    in_reply_to: Option<String>,
    media_ids: Vec<i64>,
    content_warning: Option<String>,
) -> i64 {
    use crate::database::schema::media_attachments::dsl::*;
    use diesel::prelude::*;

    let database = database::establish_connection();
    let serialized_actor: Actor = get_actor_by_uri(&database, actor).unwrap();

    let mut direct_receipients: Vec<String> = Vec::new();
    let in_reply_to_id: Option<String>;

    let ParsedContent {
        mut receipients,
        tags,
        mut inboxes,
        processed_content,
    } = parse_content(db, &dirty_content);

    drop(dirty_content); // So that it is not used anymore for sure

    match visibility {
        "public" => {
            direct_receipients.push("https://www.w3.org/ns/activitystreams#Public".to_string());
            receipients.push(format!("{}/followers", actor));
            inboxes.extend(handle_follower_inboxes(
                &database,
                &serialized_actor.followers,
            ));
        }

        "unlisted" => {
            direct_receipients.push(format!("{}/followers", actor));
            receipients.push("https://www.w3.org/ns/activitystreams#Public".to_string());
            inboxes.extend(handle_follower_inboxes(
                &database,
                &serialized_actor.followers,
            ));
        }

        "private" => {
            direct_receipients.push(format!("{}/followers", actor));
            inboxes.extend(handle_follower_inboxes(
                &database,
                &serialized_actor.followers,
            ));
        }

        _ => (),
    }

    if in_reply_to.is_some() {
        match get_activity_by_id(&database, in_reply_to.unwrap().parse::<i64>().unwrap()) {
            Ok(activity) => {
                let object_id = activity.data["object"]["id"].as_str().unwrap().to_string();
                increase_reply(&database, &object_id).unwrap();
                in_reply_to_id = Some(object_id);

                match get_actor_by_uri(
                    &database,
                    activity.data["object"]["attributedTo"].as_str().unwrap(),
                ) {
                    Ok(actor) => {
                        direct_receipients.push(actor.actor_uri);
                        if !actor.local {
                            inboxes.push(actor.inbox.unwrap());
                        }
                    }
                    Err(_) => (),
                }
            }
            Err(_) => in_reply_to_id = None,
        }
    } else {
        in_reply_to_id = None;
    }

    direct_receipients.dedup();
    receipients.dedup();
    inboxes.dedup();

    // TODO: use single query here with WHERE id in []
    let attachments: Vec<activitypub::Attachment> = media_ids
        .iter()
        .map(|at_id| {
            media_attachments
                .find(at_id)
                .get_result::<MediaAttachment>(&database.0)
                .expect("Failed to load attachments")
        })
        .map(|a| a.into())
        .collect();

    let activitypub_note = ap_controller::note(
        &actor,
        in_reply_to_id,
        processed_content,
        direct_receipients.clone(),
        receipients.clone(),
        tags,
        attachments,
        content_warning,
    );
    let activitypub_activity_create = ap_controller::create(
        &database,
        &actor,
        serde_json::to_value(&activitypub_note).unwrap(),
        direct_receipients,
        receipients,
    );
    federator::enqueue(
        &serialized_actor,
        serde_json::json!(&activitypub_activity_create),
        inboxes,
    );

    return get_ap_object_by_id(&database, &activitypub_note.id)
        .unwrap()
        .id;
}

pub fn unfollow(actor: &str, object: String) {
    let database = database::establish_connection();
    let serialized_actor: Actor = get_actor_by_uri(&database, &actor).unwrap();

    match get_actor_by_uri(&database, &object) {
        Ok(followee) => {
            if is_actor_followed_by(&database, &followee, actor).unwrap() {
                let activitypub_followers: Vec<serde_json::Value> =
                    serde_json::from_value(followee.followers["activitypub"].to_owned()).unwrap();

                let activitypub_follow_index = activitypub_followers
                    .iter()
                    .position(|ref follow| follow["href"].as_str().unwrap() == actor)
                    .unwrap();
                let activitypub_follow_id: String = serde_json::from_value(
                    activitypub_followers[activitypub_follow_index]["activity_id"].to_owned(),
                )
                    .unwrap();

                let activitypub_activity_unfollow = ap_controller::undo(
                    &database,
                    &actor,
                    get_ap_activity_by_id(&database, &activitypub_follow_id)
                        .unwrap()
                        .data,
                    vec![followee.actor_uri],
                    vec![],
                );
                remove_follow(&object, &actor);

                if !followee.local {
                    federator::enqueue(
                        &serialized_actor,
                        serde_json::json!(&activitypub_activity_unfollow),
                        vec![followee.inbox.unwrap()],
                    );
                }
            }
        }
        Err(_) => (),
    }
}

fn handle_follower_inboxes(
    db_connection: &PgConnection,
    followers: &serde_json::Value,
) -> Vec<String> {
    let ap_followers = serde_json::from_value(followers["activitypub"].clone());
    let follow_data: Vec<serde_json::Value> = ap_followers.unwrap();
    let mut inboxes: Vec<String> = vec![];

    for follower in follow_data {
        match get_actor_by_uri(db_connection, follower["href"].as_str().unwrap()) {
            Ok(actor) => {
                if !actor.local {
                    inboxes.push(actor.inbox.unwrap());
                }
            }
            Err(_) => (),
        }
    }
    return inboxes;
}

struct ParsedContent {
    receipients: Vec<String>,
    inboxes: Vec<String>,
    tags: Vec<Tag>,
    processed_content: String,
}

fn parse_content(db: &PooledConnection, content: &str) -> ParsedContent {
    let acct_regex = Regex::new(r"@[a-zA-Z0-9._-]+(@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+\w)?").unwrap();

    let mut receipients: Vec<String> = vec![];
    let mut inboxes: Vec<String> = vec![];
    let mut tags: Vec<Tag> = vec![];
    // Have to do escaping first because we will generate markup later
    let mut processed_content = html::from_plain(content);
    processed_content = html::add_links(&processed_content);

    for mention in acct_regex.captures_iter(&content) {
        match get_actor_by_acct(
            db,
            &mention.get(0).unwrap().as_str().to_string().split_off(1),
        ) {
            Ok(actor) => {
                let href = actor.url.as_ref().unwrap_or(&actor.actor_uri).clone();
                let tag = Tag {
                    _type: String::from("Mention"),
                    href: href.clone(),
                    name: mention.get(0).unwrap().as_str().to_string(),
                };

                if !actor.local {
                    inboxes.push(actor.inbox.unwrap());
                }
                receipients.push(actor.actor_uri.clone());
                tags.push(tag);
                processed_content = str::replace(
                    &processed_content,
                    mention.get(0).unwrap().as_str(),
                    &format!(
                        r#"<span class="h-card"><a class="u-url mention" href="{uri}">@<span>{acct}</span></a></span>"#,
                        uri = href,
                        acct = actor.preferred_username
                    ),
                );
            }
            Err(_) => (),
        }
    }
    ParsedContent {
        receipients,
        inboxes,
        tags,
        processed_content,
    }
}
