use super::{
    Account, AccountTimeline, Attachment, Context as StatusContext, CreateApplicationResponse,
    Emoji, Filter, Instance, Notification, NotificationsParams, Relationship, SearchResult, Status,
};
use crate::database::{self, PooledConnection};
use super::{
    auth_user::AuthenticatedUser,
    controller::{self, StatusNotFound},
    ApplicationForm, HomeTimeline, PublicTimeline, StatusForm,
};
use crate::oauth::application::Application;
use crate::web::{
    multipart::{save_multipart_file, FileSaveError},
    FormOrJson,
};
use actix_multipart::Multipart;
use actix_web::{
    delete, get,
    http::StatusCode,
    post, put,
    web::{self, HttpResponse, Json, Query},
};
use crate::models::media::save_media;
use futures::{future, TryStreamExt};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::fmt::{self, Display};

#[derive(Deserialize)]
pub struct Id(i64);

#[get("/api/v1/accounts/{id}")]
pub async fn get_account(
    pooled_connection: PooledConnection,
    path: web::Path<Id>,
) -> Result<Json<Account>, ErrorResponse> {
    controller::account(&pooled_connection, path.0.0)
        .map(as_json)
        .ok_or_else(|| ErrorResponse {
            code: StatusCode::NOT_FOUND,
            error: "User not found",
        })
}

#[post("/api/v1/accounts/{id}/follow")]
pub async fn account_follow(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Json<Relationship> {
    let relationship = controller::follow(&db, &auth_user.into_inner(), id.0.0);
    Json(relationship)
}

#[get("/api/v1/accounts/{id}/statuses")]
pub async fn account_statuses(
    pooled_connection: PooledConnection,
    auth_user: Option<AuthenticatedUser>,
    id: web::Path<Id>,
    timeline_properties: web::Query<AccountTimeline>,
) -> Result<Json<Vec<Status>>, ErrorResponse> {
    controller::account_statuses_by_id(
        &pooled_connection,
        auth_user.as_ref().map(|user| user.0.actor_uri.as_str()),
        id.0.0,
        timeline_properties.into_inner(),
    )
    .map(as_json)
    .ok_or_else(|| ErrorResponse {
        code: StatusCode::NOT_FOUND,
        error: "User not found",
    })
}

#[get("/api/v1/lists")]
// placeholder singature
pub async fn lists() -> Json<Vec<String>> {
    // TODO
    Json(vec![])
}

#[post("/api/v1/accounts/{id}/unfollow")]
pub async fn account_unfollow(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Relationship>, ErrorResponse> {
    match controller::unfollow(&db, &auth_user.0, id.0.0) {
        Ok(relationship) => Ok(Json(relationship)),
        Err(controller::UserNotFound) => Err(ErrorResponse {
            code: StatusCode::NOT_FOUND,
            error: "User not found",
        }),
    }
}

#[get("/api/v1/accounts/verify_credentials")]
pub async fn account_verify_credentials(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
) -> Json<Account> {
    let account =
        super::block_ok(move || Account::from_actor(&db, auth_user.into_inner(), true)).await;
    Json(account)
}

#[post("/api/v1/apps")]
pub async fn application(
    pooled_connection: PooledConnection,
    form: FormOrJson<ApplicationForm>,
) -> Json<CreateApplicationResponse> {
    let form_data: ApplicationForm = form.into_inner();
    let response = controller::application_create(
        &pooled_connection,
        Application {
            id: 0,
            client_name: Some(form_data.client_name),
            client_id: String::new(),
            client_secret: String::new(),
            redirect_uris: form_data.redirect_uris,
            scopes: form_data.scopes,
            website: form_data.website,
        },
    );
    Json(response)
}

#[get("/api/v1/custom_emojis")]
pub async fn custom_emojis() -> Json<Vec<Emoji>> {
    Json(vec![])
}

#[get("/api/v1/filters")]
pub async fn filters() -> Json<Vec<Filter>> {
    Json(vec![])
}

#[get("/api/v1/instance")]
pub async fn instance(db: PooledConnection) -> Json<Instance> {
    Json(controller::instance_info(&db))
}

#[get("/api/v1/notifications")]
pub async fn notifications(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    params: web::Query<NotificationsParams>,
) -> Result<Json<Vec<Notification>>, ErrorResponse> {
    let result_notfications = controller::notifications(
        &pooled_connection,
        &auth_user.into_inner(),
        params.into_inner(),
    );
    Ok(Json(result_notfications))
}

#[get("/api/v1/statuses/{id}")]
pub async fn get_status(
    pooled_connection: PooledConnection,
    id: web::Path<Id>,
    auth_user: Option<AuthenticatedUser>,
) -> Result<Json<Status>, ErrorResponse> {
    let status = controller::status_by_id(
        &pooled_connection,
        id.0.0,
        auth_user.as_ref().map(|user| user.actor_uri()),
    );
    status
        .ok_or_else(|| ErrorResponse::not_found("Status not found"))
        .map(as_json)
}

#[get("/api/v1/statuses/{id}/context")]
pub async fn status_context(
    pooled_connection: PooledConnection,
    id: web::Path<Id>,
    auth_user: Option<AuthenticatedUser>,
) -> Result<Json<StatusContext>, ErrorResponse> {
    controller::status_context(
        &pooled_connection,
        id.0.0,
        auth_user.as_ref().map(|u| u.actor_uri()),
    )
    .ok_or_else(|| ErrorResponse::not_found("Status not found"))
    .map(as_json)
}

#[post("/api/v1/statuses/{id}/favourite")]
pub async fn status_favourite(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Status>, ErrorResponse> {
    let status =
        controller::favourite(&pooled_connection, &auth_user.0, id.0.0).map_err(|e| match e {
            StatusNotFound => ErrorResponse::not_found("Status not found"),
        })?;
    Ok(Json(status))
}

#[post("/api/v1/statuses/{id}/unfavourite")]
pub async fn status_unfavourite(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Status>, ErrorResponse> {
    let status =
        controller::unfavourite(&pooled_connection, &auth_user.0, id.0.0).map_err(|e| match e {
            StatusNotFound => ErrorResponse::not_found("Status not found"),
        })?;
    Ok(Json(status))
}

#[post("/api/v1/statuses")]
pub async fn status_post(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    form: FormOrJson<StatusForm>,
) -> Result<Json<Status>, ErrorResponse> {
    let status = controller::status_post(&pooled_connection, form.into_inner(), &auth_user.0);
    Ok(Json(status))
}

#[post("/api/v1/statuses/{id}/reblog")]
pub async fn status_reblog(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Status>, ErrorResponse> {
    let status =
        controller::reblog(&pooled_connection, &auth_user.0, id.0.0).map_err(|e| match e {
            StatusNotFound => ErrorResponse::not_found("Status not found"),
        })?;
    Ok(Json(status))
}

#[post("/api/v1/statuses/{id}/unreblog")]
pub async fn status_unreblog(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Status>, ErrorResponse> {
    let status = controller::unreblog(&db, &auth_user.0, id.0.0).map_err(|e| match e {
        StatusNotFound => ErrorResponse::not_found("Status not found"),
    })?;
    Ok(Json(status))
}

#[delete("/api/v1/statuses/{id}")]
pub async fn status_delete(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Path<Id>,
) -> Result<Json<Status>, ErrorResponse> {
    use controller::DeleteOutcome;

    match controller::status_delete(&db, &auth_user.0, id.0.0) {
        DeleteOutcome::Ok(deleted) => Ok(Json(deleted)),
        DeleteOutcome::NotAuthorized => Err(ErrorResponse::forbidden(
            "Not authorized to delete this status",
        )),
        DeleteOutcome::NotFound => Err(ErrorResponse::not_found("Status not found")),
    }
}

#[get("/api/v1/timelines/home")]
pub async fn home_timeline(
    pooled_connection: PooledConnection,
    auth_user: AuthenticatedUser,
    timeline_params: Query<HomeTimeline>,
) -> Json<Vec<Status>> {
    controller::home_timeline(
        &pooled_connection,
        timeline_params.into_inner(),
        &auth_user.into_inner(),
    )
    .map(|s| Json(s))
    .unwrap()
}

#[get("/api/v1/timelines/public")]
pub async fn public_timeline(
    timeline_params: Query<PublicTimeline>,
    auth_user: Option<AuthenticatedUser>,
    db: PooledConnection,
) -> Result<Json<Vec<Status>>, ErrorResponse> {
    controller::public_timeline(
        &db,
        timeline_params.into_inner(),
        auth_user.as_ref().map(|u| u.actor_uri()),
    )
    .map(|r| Json(r))
    .map_err(ErrorResponse::internal)
}

#[derive(Deserialize)]
pub struct SearchQuery {
    q: String,
    resolve: Option<bool>,
    min_id: Option<i64>,
    max_id: Option<i64>,
    limit: Option<i64>,
}

#[get("/api/v2/search")]
pub async fn search(
    pooled_connection: PooledConnection,
    _auth_user: AuthenticatedUser,
    query: web::Query<SearchQuery>,
) -> Json<SearchResult> {
    let query = query.into_inner();
    let result = controller::search(
        &pooled_connection,
        &query.q,
        query.resolve,
        query.min_id,
        query.max_id,
        query.limit,
    ).await;
    Json(result)
}

#[get("/api/v1/accounts/relationships")]
pub async fn relationships(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    id: web::Query<Id>,
) -> Json<Vec<Relationship>> {
    let relationships = controller::relationships(&db, &auth_user.0, vec![id.into_inner().0]);
    Json(relationships)
}

#[post("/api/v1/media")]
pub async fn upload_media(
    db: PooledConnection,
    auth_user: AuthenticatedUser,
    payload: Multipart,
) -> Result<Json<Attachment>, ErrorResponse> {
    match payload
        .try_filter(|f| future::ready(f.content_disposition().unwrap().get_name() == Some("file")))
        .try_next()
        .await
    {
        Ok(Some(mut field)) => {
            // Important: moving this field out of payload does not work, file
            // stream fails to copy somehow. Doing this inside the match branch
            // works.
            let saved_file = save_multipart_file(&mut field)
                .await
                .map_err(|e| match e {
                    FileSaveError::TooBig => ErrorResponse::bad_request("File is too big"),
                })?;

            let media_attachment = save_media(
                &db,
                &auth_user.0,
                Some(&saved_file.content_type),
                saved_file.passed_name.as_ref().map(String::as_str),
                &saved_file.assigned_name,
            )
            .unwrap();
            Ok(Json(media_attachment.into()))
        }
        _ => return Err(ErrorResponse::bad_request("No file")),
    }
}

#[derive(Deserialize)]
pub struct UpdateMediaPut {
    description: Option<String>,
    #[allow(dead_code)]
    focus: Option<String>,
}

#[put("/api/v1/media/{media_id}")]
pub async fn update_media(
    media_id: web::Path<Id>,
    user: AuthenticatedUser,
    form: FormOrJson<UpdateMediaPut>,
    db: PooledConnection,
) -> Result<Json<Attachment>, ErrorResponse> {
    use crate::database::models::MediaAttachment;
    use crate::database::schema::media_attachments::dsl::*;
    use diesel::prelude::*;

    let media_attachment_result = media_attachments
        .find(media_id.0.0)
        .get_result::<MediaAttachment>(&db.0)
        .optional()
        .unwrap();

    let media_attachment = match media_attachment_result {
        Some(attach) => attach,
        None => {
            return Err(ErrorResponse::not_found("No attachment"));
        }
    };
    if media_attachment.actor_id != user.0.id {
        return Err(ErrorResponse::forbidden("Not own attachment"));
    }
    // TODO: add status check
    // TODO: add focus
    let new_media_attachment = diesel::update(media_attachments.find(id))
        .set(description.eq(form.into_inner().description.clone()))
        .get_result::<MediaAttachment>(&db.0)
        .expect("Updating attachment failed");

    Ok(Json(new_media_attachment.into()))
}

impl Into<Attachment> for database::models::MediaAttachment {
    fn into(self) -> Attachment {
        use crate::models::attachment;

        let url = attachment::file_url(&self.file_name);
        Attachment {
            id: self.id.to_string(),
            _type: self.content_type,
            url: url.clone(),
            remote_url: None,
            preview_url: url,
            text_url: None,
            meta: None, // TODO focus point
            description: self.description,
        }
    }
}

#[derive(Debug)]
pub struct ErrorResponse {
    error: &'static str,
    code: StatusCode,
}

impl ErrorResponse {
    fn internal(error: &'static str) -> Self {
        ErrorResponse {
            code: StatusCode::INTERNAL_SERVER_ERROR,
            error,
        }
    }

    fn not_found(message: &'static str) -> Self {
        ErrorResponse {
            code: StatusCode::NOT_FOUND,
            error: message,
        }
    }

    fn forbidden(message: &'static str) -> Self {
        ErrorResponse {
            code: StatusCode::FORBIDDEN,
            error: message,
        }
    }

    fn bad_request(message: &'static str) -> Self {
        ErrorResponse {
            code: StatusCode::BAD_REQUEST,
            error: message,
        }
    }
}

impl Display for ErrorResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "error: {}", self.error)
    }
}

impl actix_web::error::ResponseError for ErrorResponse {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.code).json(json!({"error": self.error}))
    }
}

fn as_json<T: Serialize>(value: T) -> Json<T> {
    Json(value)
}
