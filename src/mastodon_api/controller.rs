use super::{
    Account, AccountTimeline, Context as StatusContext, CreateApplicationResponse, HomeTimeline,
    Instance, Notification, NotificationsParams, PublicTimeline, RegistrationForm, Relationship,
    SearchResult, Status, StatusForm, MASTODON_API_ACCOUNT_CACHE,
};
use crate::activitypub::{self, actor::fetch_actor};
use crate::database::PooledConnection;
use crate::env;
use crate::kibou_api::{
    self,
    signup::{self, RegisterError},
    timeline::{self, home_timeline as get_home_timeline, public_timeline as get_public_timeline},
};
use crate::models::{
    activity::{
        self, get_activities_with_reacted_status, get_ap_object_replies_by_id, ReactedActivity,
    },
    actor::{self, get_actor_by_id_opt, Actor},
    notification::{self, notifications_for_actor},
};
use crate::oauth::{self, application::Application as OAuthApplication, token::verify_token};
use crate::web::federator;
use crate::well_known::webfinger::{poke, WebfingerResponse};

use chrono::{self, Utc};

pub fn account(pooled_connection: &PooledConnection, id: i64) -> Option<Account> {
    match actor::get_actor_by_id(pooled_connection, id) {
        Ok(actor) => {
            let account = Account::from_actor(pooled_connection, actor, false);
            Some(account)
        }
        Err(diesel::result::Error::NotFound) => None,
        Err(e) => panic!("{:?}", e),
    }
}

pub fn actor_by_oauth_token(db: &PooledConnection, token: &str) -> Option<Actor> {
    let token = verify_token(db, token).ok()?;
    actor::get_local_actor_by_preferred_username(db, &token.actor).ok()
}

pub fn account_create(form: &RegistrationForm) -> Result<oauth::token::Token, RegisterError> {
    signup::register_account(form)
        .map(|_| oauth::token::create(&form.username))
}

pub fn account_statuses_by_id(
    pooled_connection: &PooledConnection,
    account: Option<&str>,
    id: i64,
    AccountTimeline {
        max_id,
        since_id,
        min_id,
        limit,
        pinned,
        ..
    }: AccountTimeline,
) -> Option<Vec<Status>> {
    // TODO: maintain pinned statuses collection
    if let Some(true) = pinned {
        return Some(vec![]);
    }
    match actor::get_actor_by_id(pooled_connection, id) {
        Ok(actor) => {
            let ids =
                timeline::user_timeline(pooled_connection, actor, max_id, since_id, min_id, limit)
                    .unwrap();
            let statuses = hydrated_statuses(pooled_connection, ids, account);
            Some(statuses)
        }
        Err(_) => None,
    }
}

pub fn application_create(
    pooled_connection: &PooledConnection,
    application: OAuthApplication,
) -> CreateApplicationResponse {
    let oauth_app: OAuthApplication = oauth::application::create(pooled_connection, application);
    CreateApplicationResponse {
        id: oauth_app.id.to_string(),
        name: oauth_app.client_name.unwrap_or_default(),
        website: oauth_app.website,
        client_id: oauth_app.client_id,
        client_secret: oauth_app.client_secret,
        redirect_uri: oauth_app.redirect_uris,
    }
}

pub fn cached_account(
    pooled_connection: &PooledConnection,
    uri: &str,
) -> Result<Account, diesel::result::Error> {
    let mut account_cache = MASTODON_API_ACCOUNT_CACHE.lock().unwrap();

    match account_cache.get(&uri.to_string()) {
        Some(account) => Ok(account.clone()),
        None => actor::get_actor_by_uri(pooled_connection, uri).map(|actor| {
            let account = Account::from_actor(pooled_connection, actor, false);
            account_cache.put(uri.to_string(), account.clone());
            account
        }),
    }
}

pub fn to_masto_notifications(
    notifications: Vec<notification::QueryNotification>,
    actor: &str,
) -> Vec<Notification> {
    notifications
        .into_iter()
        .filter_map(|n| Notification::try_from(n.id, n.into(), actor).ok())
        .collect()
}

pub fn hydrated_statuses(
    pooled_connection: &PooledConnection,
    ids: Vec<i64>,
    actor: Option<&str>,
) -> Vec<Status> {
    let mut statuses: Vec<Status> = Vec::new();

    let activities = get_activities_with_reacted_status(pooled_connection, ids, actor).unwrap();
    for activity in activities {
        let id = activity.activity.id;
        match Status::try_from(pooled_connection, activity, actor) {
            Ok(status) => {
                statuses.push(status);
            }
            Err(e) => {
                eprintln!("Failed to deserialize status {}: {}", id, e);
            }
        }
    }
    statuses
}

pub fn status_context(
    pooled_connection: &PooledConnection,
    id: i64,
    actor: Option<&str>,
) -> Option<StatusContext> {
    activity::get_activity_by_id(&pooled_connection, id).ok()?;

    let mut ancestors = status_parents_for_id(pooled_connection, id, true, actor);
    let mut descendants = status_children_for_id(pooled_connection, id, true, actor);
    ancestors.sort_by(|status_a, status_b| {
        chrono::DateTime::parse_from_rfc3339(&status_a.created_at)
            .unwrap_or_else(|_| {
                chrono::DateTime::parse_from_rfc3339(&Utc::now().to_rfc3339()).unwrap()
            })
            .timestamp()
            .cmp(
                &chrono::DateTime::parse_from_rfc3339(&status_b.created_at)
                    .unwrap_or_else(|_| {
                        chrono::DateTime::parse_from_rfc3339(&Utc::now().to_rfc3339()).unwrap()
                    })
                    .timestamp(),
            )
    });
    descendants.sort_by(|status_a, status_b| {
        chrono::DateTime::parse_from_rfc3339(&status_a.created_at)
            .unwrap_or_else(|_| {
                chrono::DateTime::parse_from_rfc3339(&Utc::now().to_rfc3339()).unwrap()
            })
            .timestamp()
            .cmp(
                &chrono::DateTime::parse_from_rfc3339(&status_b.created_at)
                    .unwrap_or_else(|_| {
                        chrono::DateTime::parse_from_rfc3339(&Utc::now().to_rfc3339()).unwrap()
                    })
                    .timestamp(),
            )
    });
    let context = StatusContext {
        ancestors,
        descendants,
    };
    Some(context)
}

pub fn favourite(
    db: &PooledConnection,
    auth_user: &Actor,
    id: i64,
) -> Result<Status, StatusNotFound> {
    let activity = activity::get_activity(db, id).ok_or(StatusNotFound)?;
    kibou_api::react(
        auth_user.id,
        "Like",
        activity.data["object"]["id"].as_str().unwrap(),
    );
    status_by_id(db, id, Some(&auth_user.actor_uri)).ok_or(StatusNotFound)
}

pub fn unfavourite(
    db: &PooledConnection,
    auth_user: &Actor,
    id: i64,
) -> Result<Status, StatusNotFound> {
    let activity = activity::get_activity(db, id).ok_or(StatusNotFound)?;
    kibou_api::undo_react(
        db,
        auth_user.id,
        "Like",
        activity.data["object"]["id"].as_str().unwrap(),
    );
    status_by_id(db, id, Some(&auth_user.actor_uri)).ok_or(StatusNotFound)
}

pub fn follow(db: &PooledConnection, actor: &Actor, to_follow: i64) -> Relationship {
    let followee = actor::get_actor_by_id(db, to_follow).unwrap();
    kibou_api::follow(&actor.actor_uri, &followee.actor_uri);

    // TODO: this should be a real relationship
    Relationship {
        id: followee.id.to_string(),
        following: true,
        followed_by: false,
        blocking: false,
        muting: false,
        muting_notifications: false,
        requested: false,
    }
}

pub fn home_timeline(
    pooled_connection: &PooledConnection,
    parameters: HomeTimeline,
    actor: &Actor,
) -> Result<Vec<Status>, anyhow::Error> {
    let status_ids = get_home_timeline(
        pooled_connection,
        &actor.actor_uri,
        parameters.max_id,
        parameters.since_id,
        parameters.min_id,
        parameters.limit,
    )?;
    let statuses = hydrated_statuses(pooled_connection, status_ids, Some(&actor.actor_uri));
    Ok(statuses)
}

pub fn instance_info(database: &PooledConnection) -> Instance {
    Instance {
        uri: format!(
            "{base_scheme}://{base_domain}",
            base_scheme = env::get_value("endpoint.base_scheme"),
            base_domain = env::get_value("endpoint.base_domain")
        ),
        title: env::get_value("node.name"),
        description: env::get_value("node.description"),
        email: env::get_value("node.contact_email"),
        version: String::from("2.3.0 (compatible; Kibou 0.1)"),
        thumbnail: None,
        // Kibou does not support Streaming_API yet, but this value is not nullable according to
        // Mastodon-API's specifications, so that is why it is showing an empty value instead
        urls: serde_json::json!({"streaming_api": ""}),
        // `domain_count` always stays 0 as Kibou does not keep data about remote nodes
        stats: serde_json::json!({"user_count": actor::count_local_actors(&database).unwrap_or_else(|_| 0),
        "status_count": activity::count_local_ap_notes(&database).unwrap_or_else(|_| 0),
        "domain_count": 0}),
        languages: vec![],
        contact_account: None,
        max_toot_chars: env::get_number("node.max_status_chars"),
    }
}

pub fn notifications(
    db: &PooledConnection,
    actor: &Actor,
    NotificationsParams {
        max_id,
        min_id,
        since_id,
        limit,
    }: NotificationsParams,
) -> Vec<Notification> {
    let query_notifications =
        notifications_for_actor(db, actor, max_id, since_id, min_id, limit).unwrap();
    to_masto_notifications(query_notifications, &actor.actor_uri)
}

pub fn public_timeline(
    pooled_connection: &PooledConnection,
    parameters: PublicTimeline,
    actor: Option<&str>,
) -> Result<Vec<Status>, &'static str> {
    get_public_timeline(
        pooled_connection,
        parameters.local.unwrap_or_else(|| false),
        parameters.only_media.unwrap_or_else(|| false),
        parameters.max_id,
        parameters.since_id,
        parameters.min_id,
        parameters.limit,
    )
    .map(|statuses| hydrated_statuses(pooled_connection, statuses, actor))
    .map_err(|_| "An error occured while generating timeline.")
}

pub fn relationships(db: &PooledConnection, actor: &Actor, ids: Vec<i64>) -> Vec<Relationship> {
    let mut relationships: Vec<Relationship> = Vec::new();
    let activitypub_followers: Vec<serde_json::Value> =
        serde_json::from_value(actor.followers["activitypub"].to_owned()).unwrap();
    let activitypub_followees = actor::get_actor_followees(&db, &actor.actor_uri).unwrap();

    for id in ids {
        let follower_actor = match get_actor_by_id_opt(&db, id) {
            Some(actor) => actor,
            None => continue,
        };

        let followed_by = activitypub_followers
            .iter()
            .any(|ref follower| follower["href"].as_str().unwrap() == follower_actor.actor_uri);
        relationships.push(Relationship {
            id: id.to_string(),
            following: false,
            followed_by,
            blocking: false,
            muting: false,
            muting_notifications: false,
            requested: false,
        });

        if activitypub_followees
            .iter()
            .any(|ref followee| followee.id == id)
        {
            match relationships
                .iter_mut()
                .find(|ref follower| follower.id == id.to_string())
            {
                Some(rship) => {
                    rship.following = true;
                }
                None => {
                    relationships.push(Relationship {
                        id: id.to_string(),
                        following: true,
                        followed_by: false,
                        blocking: false,
                        muting: false,
                        muting_notifications: false,
                        requested: false,
                    });
                }
            }
        }
    }
    relationships
}

pub fn reblog(db: &PooledConnection, auth_user: &Actor, id: i64) -> Result<Status, StatusNotFound> {
    let activity = activity::get_activity(db, id).ok_or(StatusNotFound)?;
    kibou_api::react(
        auth_user.id,
        "Announce",
        activity.data["object"]["id"].as_str().unwrap(),
    );
    status_by_id(db, id, Some(&auth_user.actor_uri)).ok_or(StatusNotFound)
}

pub fn unreblog(
    db: &PooledConnection,
    auth_user: &Actor,
    id: i64,
) -> Result<Status, StatusNotFound> {
    let activity = activity::get_activity(db, id).ok_or(StatusNotFound)?;
    kibou_api::undo_react(
        db,
        auth_user.id,
        "Announce",
        activity.data["object"]["id"].as_str().unwrap(),
    );
    status_by_id(db, id, Some(&auth_user.actor_uri)).ok_or(StatusNotFound)
}

pub fn status_by_id(
    pooled_connection: &PooledConnection,
    id: i64,
    actor: Option<&str>,
) -> Option<Status> {
    hydrated_statuses(pooled_connection, vec![id], actor)
        .into_iter()
        .nth(0)
}

pub fn status_post(
    pooled_connection: &PooledConnection,
    form: StatusForm,
    actor: &Actor,
) -> Status {
    use crate::mastodon_api::FormVec;

    // TODO: content length check

    let ids = form
        .media_ids
        .unwrap_or_else(|| FormVec(vec![]))
        .0
        .iter()
        .map(|id| id.parse().unwrap()) // TODO
        .collect();
    let status_id = kibou_api::status_build(
        pooled_connection,
        &actor.actor_uri,
        form.status.unwrap_or(String::new()),
        &form.visibility.unwrap_or(String::from("public")),
        form.in_reply_to_id,
        ids,
        form.spoiler_text,
    );

    status_by_id(pooled_connection, status_id, Some(&actor.actor_uri)).unwrap()
}

pub enum DeleteOutcome {
    Ok(Status),
    NotAuthorized,
    NotFound,
}

pub fn status_delete(db: &PooledConnection, user: &Actor, id: i64) -> DeleteOutcome {
    let status_activity = match activity::get_activity_by_id(db, id) {
        Ok(activity) => {
            if activity.data["type"].as_str() == Some("Create") && activity.actor == user.actor_uri
            {
                activity
            } else {
                return DeleteOutcome::NotAuthorized;
            }
        }
        Err(diesel::result::Error::NotFound) => return DeleteOutcome::NotFound,
        Err(e) => panic!("{:?}", e),
    };
    let mut recipients = vec![];
    let object_id = status_activity.data["object"]["id"].as_str().unwrap();
    // Send it to followers, all the people who announced or replied to it (maybe liked too?)
    let announces = activity::get_all_activities_with_target(db, object_id, "Announce").unwrap();
    for announce in announces {
        recipients.push(announce.actor);
    }
    // TODO: get replies
    let followers = user.followers().unwrap();
    for follower in followers {
        recipients.push(follower.href);
    }
    recipients.dedup();
    let host = crate::env::get_value("endpoint.host");
    activitypub::controller::retain_remote_actors(&mut recipients, &host);

    let recipient_inboxes = actor::get_actors_by_uri(&db, recipients)
        .unwrap()
        .into_iter()
        .filter_map(|a| a.inbox)
        .collect();

    if let Some(in_reply_to) = status_activity.data["object"]["inReplyTo"].as_str() {
        if let Err(e) = activity::decrease_reply(&db, in_reply_to) {
            eprintln!(
                "Failed to decrease reply count for {}, {:?}",
                in_reply_to, e
            )
        }
    }
    let delete_activity =
        activitypub::controller::delete(db, user, &status_activity.clone().into());
    federator::enqueue(
        &user,
        serde_json::to_value(delete_activity).unwrap(),
        recipient_inboxes,
    );

    let reacted_activity = ReactedActivity {
        activity: status_activity,
        liked: false,
        announced: false,
    };
    let status = Status::try_from(db, reacted_activity, None).unwrap();
    DeleteOutcome::Ok(status)
}

pub struct UserNotFound;
pub struct StatusNotFound;

pub fn unfollow(
    db: &PooledConnection,
    actor: &Actor,
    target_id: i64,
) -> Result<Relationship, UserNotFound> {
    let followee = actor::get_actor_by_id_opt(db.conn(), target_id).ok_or_else(|| UserNotFound)?;

    kibou_api::unfollow(&actor.actor_uri, followee.actor_uri);
    let relationship = Relationship {
        id: followee.id.to_string(),
        following: false,
        followed_by: false,
        blocking: false,
        muting: false,
        muting_notifications: false,
        requested: false,
    };
    Ok(relationship)
}

fn status_children_for_id(
    pooled_connection: &PooledConnection,
    id: i64,
    resolve_children: bool,
    actor: Option<&str>,
) -> Vec<Status> {
    let mut statuses: Vec<Status> = vec![];

    let status = status_by_id(pooled_connection, id, actor).unwrap();
    let replies = get_ap_object_replies_by_id(pooled_connection, &status.uri, actor).unwrap();
    if !replies.is_empty() {
        for reply in replies {
            if resolve_children {
                let mut child_statuses =
                    status_children_for_id(pooled_connection, reply.activity.id, true, actor);
                statuses.append(&mut child_statuses);
            }

            statuses.push(Status::try_from(pooled_connection, reply, actor).unwrap());
        }
    }
    statuses
}

fn status_parents_for_id(
    pooled_connection: &PooledConnection,
    id: i64,
    is_head: bool,
    actor: Option<&str>,
) -> Vec<Status> {
    let mut statuses: Vec<Status> = vec![];

    let head_status = status_by_id(pooled_connection, id, actor);
    match head_status {
        Some(status) => {
            if status.in_reply_to_id.is_some() {
                statuses.append(&mut status_parents_for_id(
                    pooled_connection,
                    status
                        .in_reply_to_id
                        .clone()
                        .unwrap()
                        .parse::<i64>()
                        .unwrap(),
                    false,
                    actor,
                ));
            }

            if !is_head {
                statuses.append(&mut status_children_for_id(
                    pooled_connection,
                    id,
                    false,
                    actor,
                ));
                statuses.dedup_by_key(|ref mut s| s.id == status.id);
                statuses.push(status);
            }
        }
        None => eprintln!("Error while fetching parents"),
    }

    return statuses;
}

pub async fn search(
    pooled_connection: &PooledConnection,
    query: &str,
    _resolve: Option<bool>,
    _min_id: Option<i64>,
    _max_id: Option<i64>,
    _limit: Option<i64>,
) -> SearchResult {
    println!("Search with query: {}", query);
    let result: SearchResult = match Username::parse(query) {
        Some(username) => {
            println!("Parsed username: {:?}", &username);
            let mut accounts = vec![];
            match get_account(pooled_connection, &username).await {
                Ok(account) => accounts.push(account),
                Err(err) => error!("{}", err),
            }
            SearchResult {
                accounts,
                statuses: vec![],
                hashtags: vec![],
            }
        }
        None => SearchResult {
            accounts: vec![],
            statuses: vec![],
            hashtags: vec![],
        },
    };
    result
}

async fn get_account(
    pooled_connection: &PooledConnection,
    username: &Username,
) -> anyhow::Result<Account> {
    if username.server == env::get_value("endpoint.base_domain") {
        let actor =
            actor::get_local_actor_by_preferred_username(&pooled_connection, &username.local)?;
        return Ok(Account::from_actor(&pooled_connection, actor, false));
    }
    let acct = format!("{}@{}", &username.local, &username.server);
    let webfinger_response: Result<_, _> = poke(&username.server, &acct).await;
    println!("Webfinger response: {:?}", &webfinger_response);
    let ok_response = webfinger_response?;
    let profile_link = find_profile_link(&ok_response);
    match profile_link {
        Some(uri) => {
            let actor = fetch_actor(uri).await?;
            println!("Fetched actor: {:?}", actor);
            let account = Account::from_actor(pooled_connection, actor, false);
            Ok(account)
        }
        None => Err(anyhow::anyhow!("Did not find profile link")),
    }
}

#[derive(Debug, PartialEq)]
struct Username {
    local: String,
    server: String,
}

impl Username {
    fn parse(full_username: &str) -> Option<Username> {
        let parts: Vec<_> = full_username.split("@").collect();
        if parts.len() == 3 {
            let username = Username {
                local: parts[1].to_string(),
                server: parts[2].to_string(),
            };
            Some(username)
        } else {
            None
        }
    }
}

fn find_profile_link(response: &WebfingerResponse) -> Option<&str> {
    response
        .links
        .iter()
        .find(|link| {
            link.rel == "self" && link.content_type == Some("application/activity+json".to_string())
        })
        .as_ref()
        .map(|link| link.href.as_ref().expect("No href on link").as_str())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_username() {
        assert_eq!(
            Username::parse("@local@domain.com"),
            Some(Username {
                local: "local".to_string(),
                server: "domain.com".to_string()
            })
        )
    }

    #[test]
    fn test_find_profile_link() {
        let data = r#"
{
  "subject": "acct:charlag@birb.site",
  "aliases": [
    "https://birb.site/@charlag",
    "https://birb.site/users/charlag"
  ],
  "links": [
    {
      "rel": "http://webfinger.net/rel/profile-page",
      "type": "text/html",
      "href": "https://birb.site/@charlag"
    },
    {
      "rel": "self",
      "type": "application/activity+json",
      "href": "https://birb.site/users/charlag"
    },
    {
      "rel": "http://ostatus.org/schema/1.0/subscribe",
      "template": "https://birb.site/authorize_interaction?uri={uri}"
    }
  ]
}
"#;
        let response: WebfingerResponse = serde_json::from_str(data).unwrap();
        let link = find_profile_link(&response);
        assert_eq!(link, Some("https://birb.site/users/charlag"));
    }
}
