use crate::models::actor::{get_local_actor_by_preferred_username, Actor};
use crate::database;
use crate::oauth;
use actix_web::{dev, error::ErrorUnauthorized, Error, FromRequest, HttpRequest};
use futures::future::{err, ok, Ready};

#[derive(Debug)]
pub struct AuthenticatedUser(pub Actor);

impl AuthenticatedUser {
    pub fn into_inner(self) -> Actor {
        return self.0;
    }

    pub fn actor_uri(&self) -> &str {
        self.0.actor_uri.as_str()
    }
}

impl FromRequest for AuthenticatedUser {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(request: &HttpRequest, _: &mut dev::Payload) -> Self::Future {
        let token = match request
            .headers()
            .get("Authorization")
            .and_then(|h| h.to_str().ok())
        {
            Some(header) => super::parse_authorization_header(header),
            None => return err(ErrorUnauthorized("Invalid Authorization header")),
        };
        let db = database::establish_connection();

        let actor = oauth::token::verify_token(&db, &token)
            .and_then(|token| get_local_actor_by_preferred_username(&db, &token.actor));
        match actor {
            Ok(actor) => ok(AuthenticatedUser(actor)),
            Err(_) => err(ErrorUnauthorized("Invalid token"))
        }
    }
}
