use super::{controller::cached_account, Account, Attachment, Emoji, Mention, Tag};
use crate::models::activity::{
    get_ap_object_by_id,
    ReactedActivity,
    get_reacted_activity_by_object_id,
};
use crate::activitypub;
use crate::database::PooledConnection;

use anyhow::anyhow;
use chrono::Utc;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::{Hash, Hasher};

#[derive(Serialize, Deserialize)]
pub struct Status {
    // Properties according to
    // - https://docs.joinmastodon.org/api/entities/#status
    pub id: String,
    pub uri: String,
    pub url: Option<String>,
    pub account: Account,
    pub in_reply_to_id: Option<String>,
    pub in_reply_to_account_id: Option<String>,
    pub reblog: Option<serde_json::Value>,
    pub content: String,
    pub created_at: String,
    pub emojis: Vec<Emoji>,
    pub replies_count: i64,
    pub reblogs_count: i64,
    pub favourites_count: i64,
    pub reblogged: bool,
    pub favourited: bool,
    pub muted: Option<bool>,
    pub sensitive: bool,
    pub spoiler_text: String,
    pub visibility: String,
    pub media_attachments: Vec<Attachment>,
    pub mentions: Vec<Mention>,
    pub tags: Vec<Tag>,
    pub application: serde_json::Value,
    pub language: Option<String>,
    pub pinned: Option<bool>,
}

impl Status {
    // TODO: it cries "add tests to me" but we need to abstract loading of things for that
    // (cache and db alike)
    pub fn try_from(
        db: &PooledConnection,
        activity: ReactedActivity,
        actor: Option<&str>,
    ) -> Result<Self, anyhow::Error> {
        let ap_activity: activitypub::activity::Activity =
            serde_json::from_value(activity.activity.data.clone())?;

        Status::from_activitystreams(db, activity, ap_activity, actor)
    }

    fn from_activitystreams(
        db: &PooledConnection,
        reacted_activity: ReactedActivity,
        activity: activitypub::activity::Activity,
        actor: Option<&str>,
    ) -> Result<Self, anyhow::Error> {
        let account: Account = cached_account(db, &activity.actor)?;

        // We dedup mentioned accounts based on Id, map would require cloning ids
        let mut mentioned_accounts: HashSet<IdAccount> = HashSet::new();

        // Primary and secondary audience deduped
        let audience: HashSet<&String> = activity.to.iter().chain(activity.cc.iter()).collect();

        for actor in &audience {
            let mention_account = cached_account(db, &actor);
            // Just unwrapping every account would mean that an entire status fails serializing,
            // because of one invalid account.
            if let Ok(mention_account) = mention_account {
                mentioned_accounts.insert(mention_account.into());
            };
        }

        // The 'Public' and 'Unlisted' scope can be easily determined by the existence of
        // `https://www.w3.org/ns/activitystreams#Public` in either the 'to' or 'cc' field.
        //
        // Note that different formats like `as:Public` have already been normalized to
        // `https://www.w3.org/ns/activitystreams#Public` in activitypub::validator.
        let visibility = if activity
            .to
            .contains(&"https://www.w3.org/ns/activitystreams#Public".to_string())
        {
            String::from("public")
        } else if activity
            .cc
            .contains(&"https://www.w3.org/ns/activitystreams#Public".to_string())
        {
            String::from("unlisted")
        // XX - This might cause issues, as the 'Followers' endpoint of remote actors might differ
        // from Kibou's schema. But as of now Kibou does not keep track of that endpoint.
        } else if activity.to.contains(&format!("{}/followers", &account.url)) {
            String::from("private")
        } else {
            String::from("direct")
        };

        match activity._type.as_str() {
            "Create" => {
                let inner_object: activitypub::activity::Object =
                    serde_json::from_value(activity.object.clone())?;
                for tag in inner_object.tag.unwrap_or_else(Vec::new) {
                    // We should search by url too
                    let account = cached_account(db, &tag.href);
                    // We should've got remote account here when we received activity,
                    // now it's too late and it probably doesn't exists.
                    if let Ok(account) = account {
                        mentioned_accounts.insert(account.into());
                    }
                }
                let mut in_reply_to: Option<String> = None;
                let mut in_reply_to_account: Option<String> = None;
                if inner_object.inReplyTo.is_some() {
                    in_reply_to = match get_ap_object_by_id(db, &inner_object.inReplyTo.unwrap()) {
                        Ok(parent_activity) => {
                            in_reply_to_account = match cached_account(
                                db,
                                &parent_activity.data["actor"].as_str().unwrap(),
                            ) {
                                Ok(parent_actor) => Some(parent_actor.id.to_string()),
                                Err(_) => None,
                            };
                            Some(parent_activity.id.to_string())
                        }
                        Err(_) => None,
                    };
                }

                let mut media_attachments: Vec<Attachment> = Vec::new();
                match activity.object.get("attachment") {
                    Some(_attachments) => {
                        let attachments: Vec<activitypub::Attachment> =
                            serde_json::from_value(activity.object["attachment"].to_owned())
                                .unwrap_or_else(|e| {
                                    eprintln!("Failed to deserialize atachments, {}", e);
                                    Vec::new()
                                });

                        for attachment in attachments {
                            media_attachments.push(Attachment {
                                id: attachment
                                    .name
                                    .clone()
                                    .unwrap_or_else(|| String::from("Unnamed attachment")),
                                _type: String::from("image"),
                                url: attachment.url.clone(),
                                remote_url: Some(attachment.url.clone()),
                                preview_url: attachment.url,
                                text_url: None,
                                meta: None,
                                description: attachment.name,
                            });
                        }
                    }
                    None => (),
                }

                let mentions: Vec<Mention> = mentioned_accounts
                    .into_iter()
                    .map(|IdAccount(account)| Mention {
                        id: account.id.to_string(),
                        // TODO: account.url is account_uri currently which is not always the case
                        url: account.url.clone(),
                        username: account.username.clone(),
                        acct: account.acct(),
                    })
                    .collect();
                return Ok(Status {
                    id: reacted_activity.activity.id.to_string(),
                    uri: inner_object.id.clone(),
                    url: Some(inner_object.id.clone()),
                    account: account,
                    in_reply_to_id: in_reply_to,
                    in_reply_to_account_id: in_reply_to_account,
                    reblog: None,
                    content: inner_object.content,
                    created_at: inner_object.published,
                    emojis: vec![],
                    replies_count: inner_object.reply_count,
                    reblogs_count: inner_object.announce_count,
                    favourites_count: inner_object.like_count,
                    reblogged: reacted_activity.announced,
                    favourited: reacted_activity.liked,
                    muted: None,
                    sensitive: inner_object.sensitive.unwrap_or(false),
                    spoiler_text: inner_object.summary.unwrap_or_else(String::new),
                    visibility: visibility,
                    media_attachments: media_attachments,
                    mentions: mentions,
                    tags: vec![],
                    application: serde_json::json!({"name": "Web", "website": null}),
                    language: None,
                    pinned: None,
                });
            }
            "Announce" => {
                let object_id = activity
                    .object
                    .as_str()
                    .ok_or_else(|| anyhow!("Announce but object is not url"))?;
                let reblog = get_reacted_activity_by_object_id(&db, object_id, actor, "Create")?;
                let serialized_reblog = Status::try_from(db, reblog, actor)?;
                return Ok(Status {
                    id: reacted_activity.activity.id.to_string(),
                    uri: activity.id.clone(),
                    url: Some(activity.id.clone()),
                    account: account,
                    in_reply_to_id: None,
                    in_reply_to_account_id: None,
                    reblog: Some(serde_json::to_value(serialized_reblog).unwrap()),
                    content: String::from("reblog"),
                    created_at: activity
                        .published
                        .unwrap_or_else(|| Utc::now().to_rfc3339().to_string()),
                    emojis: vec![],
                    replies_count: 0,
                    reblogs_count: 0,
                    favourites_count: 0,
                    reblogged: false,
                    favourited: false,
                    muted: Some(false),
                    sensitive: false,
                    spoiler_text: String::new(),
                    visibility: visibility,
                    media_attachments: vec![],
                    mentions: vec![],
                    tags: vec![],
                    application: serde_json::json!({"name": "Web", "website": null}),
                    language: None,
                    pinned: None,
                });
            }
            t => Err(anyhow!("Unknown activity type to serialize: {:?}", t)),
        }
    }
}

/// Newtype which implements Eq and Hash in terms of identity so that we don't
/// risk screwing identity for Account in general.
struct IdAccount(Account);

impl PartialEq for IdAccount {
    fn eq(&self, other: &Self) -> bool {
        self.0.id == other.0.id
    }
}

impl Eq for IdAccount {}

impl Hash for IdAccount {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.id.hash(state)
    }
}

impl From<Account> for IdAccount {
    fn from(account: Account) -> Self {
        IdAccount(account)
    }
}
