use actix_web::{web::ServiceConfig};

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate async_recursion;

pub mod activitypub;
pub mod database;
pub mod env;
pub mod kibou_api;
pub mod workers;
pub mod models;

mod mastodon_api;
mod oauth;
pub mod raito_fe;
mod web;
mod well_known;

pub fn configure_mastodon_api(cfg: &mut ServiceConfig) {
    use mastodon_api::routes::*;

    // To not rewrite all routes now we will start without scoping
    cfg.service(public_timeline)
        .service(instance)
        .service(account_verify_credentials)
        .service(home_timeline)
        .service(get_account)
        .service(account_follow)
        .service(account_statuses)
        .service(lists)
        .service(account_unfollow)
        .service(account_verify_credentials)
        .service(get_account)
        .service(account_follow)
        .service(account_statuses)
        .service(lists)
        .service(account_unfollow)
        .service(custom_emojis)
        .service(filters)
        .service(notifications)
        .service(get_status)
        .service(status_context)
        .service(status_favourite)
        .service(status_unfavourite)
        .service(status_delete)
        .service(status_reblog)
        .service(status_unreblog)
        .service(relationships)
        .service(search)
        .service(status_post)
        .service(application)
        .service(upload_media)
        .service(update_media);
}

pub fn get_tera() -> tera::Tera {
    // TODO: templates are needed for oauth as well, should probably move it elsewhere
    let templates_dir = "templates/**/*";
    let mut tera = tera::Tera::new(templates_dir).unwrap();
    tera.autoescape_on(vec![".html", ".tera"]);
    tera
}

pub fn configure_raito_fe(cfg: &mut ServiceConfig) {
    use raito_fe::routes::*;

    cfg
        .service(about)
        .service(index)
        .service(login)
        .service(object)
        .service(actor)
        .service(login_post)
        .service(home_timeline)
        .service(public_timeline)
        .service(global_timeline)
        .service(account)
        .service(settings_profile)
        .service(settings_profile_post)
        .service(settings)
        .service(view_status)
        .service(register)
        .service(account_follow)
        .service(account_unfollow)
        .service(status_draft)
        .service(status_compose)
        .service(search);
}

pub fn configure_oauth(cfg: &mut ServiceConfig) {
    use oauth::routes;

    cfg.service(routes::authorize)
        .service(routes::authorize_result)
        .service(routes::token);
}

pub fn configure_wellknown(cfg: &mut ServiceConfig) {
    use well_known::{nodeinfo, webfinger};

    if should_mount_nodeinfo() {
        cfg.service(nodeinfo::nodeinfo)
            .service(nodeinfo::nodeinfo_v2);
    }
    cfg.service(webfinger::webfinger);
}

pub fn configure_activitypub(cfg: &mut ServiceConfig) {
    use activitypub::routes;

    cfg.service(routes::actor)
        .service(routes::activity)
        .service(routes::actor_inbox)
        .service(routes::inbox)
        .service(routes::get_object)
        .service(routes::following)
        .service(routes::followers);
}

// Returns whether nodeinfo routes should be mounted based
// on the value of a configuration entry.
fn should_mount_nodeinfo() -> bool {
    let value: String = env::get_value("nodeinfo.enabled").to_lowercase();

    // Nodeinfo will only be disabled if its configuration value is
    // explicitly set to false, "false", or any uppercase equivalent.
    value.as_str() != "false"
}
