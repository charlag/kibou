use config::Config;

lazy_static! {
    // This isn't perfect, we should inject where necessary but this prevents
    // reading file on each config access.
    static ref APP_CONFIG: Config = init_config();
}

pub fn get_value(key: &str) -> String {
    match APP_CONFIG.get_str(&key) {
        Ok(value) => value,
        Err(_) => {
            eprintln!("Key '{}' not found in config", &key);
            return String::from("");
        }
    }
}

pub fn get_number(key: &str) -> Option<i64> {
    APP_CONFIG.get_int(key).ok()
}

fn set_default_config_values(c: &mut Config) {
    // Serve nodeinfo by default, but provide admins with a way
    // to disable it in the config file.
    c.set_default("nodeinfo.enabled", true).unwrap();
}

fn init_config() -> Config {
    let mut config = Config::default();

    let environment = if cfg!(debug_assertions) {
        "development"
    } else {
        "production"
    };

    set_default_config_values(&mut config);

    config
        .merge(config::File::with_name(&format!(
            "env.{}.toml",
            environment
        )))
        .expect("Environment config not found!");

    config
}
