use actix_files as afs;
use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use kibou::{configure_activitypub, configure_mastodon_api, configure_oauth, configure_raito_fe, configure_wellknown, database, env, workers, get_tera};
use std::io;
use log::info;

#[macro_use]
extern crate diesel_migrations;

embed_migrations!();

#[actix_rt::main]
async fn main() -> io::Result<()> {
    let domain = env::get_value("endpoint.base_domain");
    let host = env::get_value("endpoint.host");
    let port = env::get_value("endpoint.port");
    let addr = format!("{}:{}", host, port);

    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
        .init();

    let is_debug = cfg!(debug_assertions);
    if is_debug {
        info!("Running in DEBUG mode");
    } else {
        info!("Running in PRODUCTION mode");
    }

    let pool: &database::Pool = &database::POOL;

    embedded_migrations::run_with_output(&pool.get().unwrap(), &mut std::io::stdout()).unwrap();


    let session_key = base64::decode(&env::get_value("endpoint.secret_key")).unwrap();
    // We do this once and not per worker.
    let tera = get_tera();

    let app = HttpServer::new(move || {
        let media_path = env::get_value("media.path");
        let ssl = env::get_value("endpoint.base_scheme") == "https";

        App::new()
            .app_data(pool)
            .data(tera.clone())
            .wrap(actix_cors::Cors::permissive())
            .wrap(
                actix_session::CookieSession::private(&session_key)
                    .name("kibou_session")
                    .secure(ssl),
            )
            .service(afs::Files::new("/public", media_path))
            .service(afs::Files::new("/static", "./static"))
            .default_service(web::route().to(|| {
                HttpResponse::NotFound()
                    .body("Oops, the thing is not here and it doesn't seem like it should be.")
            }))
            .wrap(middleware::Logger::default())
            .configure(configure_mastodon_api)
            // Mount AP before raito_fe because they use the same route but AP
            // routes have guard for Content-Type
            .configure(configure_activitypub)
            .configure(configure_raito_fe)
            .configure(configure_oauth)
            .configure(configure_wellknown)
    })
        .server_hostname(&domain)
        .workers(env::get_value("endpoint.workers").parse().expect("Invalid value of endpoint.workers"))
        .bind(&addr)?;

    workers::refresh_profiles();

    app.run().await
}