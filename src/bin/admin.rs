/// Binary to invoke various admin-related functions.

use kibou::{workers, database};

use getopts::Options;

#[actix_rt::main]
async fn main() {
    let args: Vec<String> = std::env::args().collect();

    let mut opts = Options::new();
    opts.optopt(
        "r",
        "refresh_profiles",
        "Refresh all remote profiles manually",
        "",
    );
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };
    if matches.opt_defined("r") {
        println!("Starting refresh of all remote profiles");
        let database = database::establish_connection();
        workers::refetch_profiles(&database, &chrono::Utc::now().naive_utc()).await;
    } else {
        print!("{}", opts.usage("admin [OPERATION]"));
    }
}
