use crate::database::PooledConnection;
use crate::env;
use crate::mastodon_api::{Account, RegistrationForm, StatusForm};
use crate::models::{activity, actor};
use crate::raito_fe::{self, Configuration, LoginForm};
use crate::web::{Redirect, RedirectError, Template};
use actix_session::Session;
use actix_web::Either;
use actor::Actor;

pub fn index(configuration: Configuration) -> Either<Redirect, Template> {
    match &configuration.account {
        Some(_) => Either::A(Redirect::to("/timeline/home")),
        None => {
            let mut context = tera::Context::new();
            context.extend(configuration.context);
            context.insert("repo_url", env!("CARGO_PKG_REPOSITORY"));
            context.insert(
                "registrations_enabled",
                &(env::get_value("node.registrations_enabled") == "true"),
            );
            Either::B(Template::with_context(
                "raito_fe/infoscreen.html.tera",
                context,
            ))
        }
    }
}

pub fn about(config: Configuration) -> Template {
    Template::with_context("raito_fe/about.html.tera", config.context)
}

pub fn account_by_local_id(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    id: String,
) -> Option<Template> {
    let account = match raito_fe::api_controller::get_account(pooled_connection, &id) {
        Ok(account) => account,
        _ => return None,
    };

    let following = configuration
        .actor
        .as_ref()
        .map(|actor| {
            let relationsips = raito_fe::api_controller::relationships_by_token(
                pooled_connection,
                actor,
                vec![id.parse::<i64>().unwrap()],
            )
            .unwrap();
            relationsips[0].following
        })
        .unwrap_or(false);

    let mut context = tera::Context::new();
    // Should add the whole relationship here probably
    context.insert("following", &following);
    context.insert("account", &account);

    let account_timeline = raito_fe::api_controller::get_user_timeline(
        pooled_connection,
        id,
        configuration.actor.as_ref(),
    )
    .unwrap_or_else(|_| Vec::new());

    add_config(&mut context, configuration);
    context.insert("account_statuses", &account_timeline);

    let template = Template::with_context("raito_fe/account.html.tera", context);
    Some(template)
}

pub fn account_by_username(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    username: String,
) -> Option<Template> {
    match actor::get_local_actor_by_preferred_username(pooled_connection, &username) {
        Ok(actor) => account_by_local_id(pooled_connection, configuration, actor.id.to_string()),
        Err(_) => None,
    }
}

pub fn account_follow(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    id: i64,
    unfollow: bool,
) -> Option<Redirect> {
    match &configuration.actor {
        Some(actor) => {
            // It's a mess, this calls into raito_fe which calls into mastodon
            // controller which calls into kibou controller.
            if unfollow {
                raito_fe::api_controller::unfollow(pooled_connection, actor, id).unwrap();
            } else {
                raito_fe::api_controller::follow(pooled_connection, actor, id).unwrap();
            }

            Some(redirect_to_account(id))
        }
        None => Some(redirect_to_account(id)),
    }
}

fn redirect_to_account(account_id: i64) -> Redirect {
    Redirect::see_other(format!("/account/{}", account_id))
}

pub fn compose(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    in_reply_to: Option<i64>,
) -> Result<Template, RedirectError> {
    if configuration.account.is_none() {
        Err(Redirect::see_other("/").into())
    } else {
        let mut context = tera::Context::new();

        if let Some(head_status_id) = in_reply_to {
            let reply_status = raito_fe::api_controller::get_status(
                pooled_connection,
                head_status_id.to_string(),
                configuration.account.as_ref().map(|a| a.url.as_ref()),
            );
            if let Ok(status) = reply_status {
                context.insert("head_status", &status);
            }
        }
        add_config(&mut context, configuration);
        Ok(Template::with_context(
            "raito_fe/status_post.html.tera",
            context,
        ))
    }
}

pub fn compose_post(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    form: StatusForm,
) -> Redirect {
    match &configuration.actor {
        Some(actor) => {
            raito_fe::api_controller::post_status(pooled_connection, form, actor);
            Redirect::see_other("/timeline/home")
        }
        None => Redirect::see_other("/"),
    }
}

pub fn context_notifications(
    pooled_connection: &PooledConnection,
    actor: &Option<Actor>,
) -> tera::Context {
    let mut context = tera::Context::new();

    if let Some(ref actor) = actor {
        if let Ok(notifications) = raito_fe::api_controller::notifications(pooled_connection, actor)
        {
            context.insert("notifications", &notifications);
        }
    }

    context
}

pub fn conversation(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    id: String,
) -> Option<Template> {
    let account_uri = configuration.account.as_ref().map(|a| a.url.as_ref());

    let status =
        match raito_fe::api_controller::get_status(pooled_connection, id.clone(), account_uri) {
            Ok(status) => status,
            _ => return None,
        };

    let status_context =
        raito_fe::api_controller::get_status_context(pooled_connection, id, account_uri).unwrap();

    let mut context = tera::Context::new();
    add_config(&mut context, configuration);
    context.insert("timeline_name", "Conversation");
    context.insert("parent_statuses", &status_context.ancestors);
    context.insert("main_status", &status);
    context.insert("child_statuses", &status_context.descendants);

    let template = Template::render("raito_fe/conversation.html.tera", context.into_json());
    Some(template)
}

// Note: This case only occurs if the Raito-FE is set as the main UI of Kibou
pub fn conversation_by_uri(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    id: String,
) -> Option<Template> {
    let object_id = format!(
        "{}://{}/objects/{}",
        env::get_value("endpoint.base_scheme"),
        env::get_value("endpoint.base_domain"),
        id
    );

    match activity::get_ap_object_by_id(pooled_connection, &object_id) {
        Ok(activity) => conversation(pooled_connection, configuration, activity.id.to_string()),
        Err(_) => None,
    }
}

pub fn home_timeline(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
) -> Result<Template, RedirectError> {
    let actor = match configuration.actor {
        Some(ref auth_actor) => auth_actor,
        None => return Err(Redirect::to("/login").into()),
    };

    let statuses = raito_fe::api_controller::home_timeline(pooled_connection, actor).unwrap();

    let mut context = tera::Context::new();
    add_config(&mut context, configuration);
    context.insert("timeline_name", "Home Timeline");
    context.insert("statuses", &statuses);

    let template = Template::with_context("raito_fe/timeline.html.tera", context);
    Ok(template)
}

pub fn login(configuration: Configuration) -> Either<Template, Redirect> {
    if configuration.account.is_none() {
        Either::A(Template::with_context(
            "raito_fe/login.html.tera",
            configuration.context,
        ))
    } else {
        Either::B(Redirect::to("/"))
    }
}

pub fn login_post(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    session: Session,
    form: LoginForm,
) -> Result<Redirect, RedirectError> {
    if configuration.account.is_none() {
        match raito_fe::api_controller::login(pooled_connection, form) {
            Some(token) => {
                session.set("oauth_token", token).unwrap();
                return Ok(Redirect::see_other("/timeline/home"));
            }
            None => {
                let redirect = Redirect::to("/login");
                Err(redirect.into())
            }
        }
    } else {
        return Ok(Redirect::see_other("/timeline/home"));
    }
}

pub fn public_timeline(
    pooled_connection: &PooledConnection,
    configuration: Configuration,
    local: bool,
) -> Template {
    let mut context = tera::Context::new();
    let Configuration {
        actor,
        context: config_context,
        ..
    } = configuration;
    context.extend(config_context);

    if local {
        context.insert("timeline_name", "Public Timeline");
    } else {
        context.insert("timeline_name", "Global Timeline");
    }

    let statuses =
        raito_fe::api_controller::get_public_timeline(pooled_connection, local, actor.as_ref())
            .unwrap();

    context.insert("statuses", &statuses);
    Template::with_context("raito_fe/timeline.html.tera", context)
}

pub fn register_post(
    configuration: Configuration,
    session: Session,
    form: RegistrationForm,
) -> Result<Redirect, RedirectError> {
    if configuration.account.is_none() {
        match raito_fe::api_controller::register(form) {
            Ok(token) => {
                session.set("oauth_token", token.access_token).unwrap();
                Ok(Redirect::see_other("/timeline/home"))
            }
            Err(e) => {
                debug!("Error during signup: {}", e);
                let err = RedirectError::new(Redirect::to("/"), e);
                Err(err)
            }
        }
    } else {
        Ok(Redirect::see_other("/timeline/home"))
    }
}

pub fn settings(configuration: Configuration) -> Either<Template, Redirect> {
    if configuration.actor.is_none() {
        return Either::B(Redirect::to("/"));
    }
    let template = Template::with_context("raito_fe/settings.html.tera", configuration.context);
    Either::A(template)
}

pub fn profile_settings(config: Configuration) -> Either<Template, Redirect> {
    let account = match config.account {
        Some(ref account) => account,
        None => return Either::B(Redirect::to("/")),
    };

    Either::A(render_profile_settings(config.context, account))
}

fn render_profile_settings(mut context: tera::Context, account: &Account) -> Template {
    context.insert("authenticated_account_bio", &account.note);
    Template::with_context("raito_fe/settings_profile.html.tera", context)
}

pub fn profile_settings_post(config: Configuration, updated_account: &Account) -> Template {
    let Configuration { mut context, .. } = config;
    // This should probably live elsewhere
    context.insert(
        "authenticated_account_display_name",
        &updated_account.display_name,
    );
    context.insert("authenticated_account_bio", &updated_account.note);
    context.insert("profile_updated", &"true");
    context.insert("authenticated_account_avatar", &updated_account.avatar);
    render_profile_settings(context, updated_account)
}

fn add_config(tera_context: &mut tera::Context, config: Configuration) {
    tera_context.extend(config.context);
}


pub async fn search(db: &PooledConnection, config: Configuration, query: String) -> Either<Template, Redirect> {
    if config.actor.is_none() {
        return Either::B(Redirect::to("/"));
    }
    let search_result = raito_fe::api_controller::search(db, query).await.unwrap();
    let mut context = config.context;
    context.try_insert("accounts", &search_result.accounts).unwrap();
    let template = Template::with_context("raito_fe/search.html.tera", context);
    Either::A(template)
}