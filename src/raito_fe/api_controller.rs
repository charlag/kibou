use crate::models::actor::{self, Actor};
use crate::database::PooledConnection;
use crate::mastodon_api::{controller, Account, Context, HomeTimeline, Notification, NotificationsParams, PublicTimeline, RegistrationForm, Relationship, Status, StatusForm, SearchResult};
use crate::oauth;
use crate::raito_fe::{LoginForm};
use crate::env;

pub fn follow(db: &PooledConnection, actor: &Actor, id: i64) -> Result<Relationship, ()> {
    Ok(controller::follow(db, actor, id))
}

pub fn unfollow(db: &PooledConnection, actor: &Actor, id: i64) -> Result<Relationship, ()> {
    controller::unfollow(db, actor, id)
        .map_err(|_| ())
}

pub fn notifications(
    pooled_connection: &PooledConnection,
    auth_user: &Actor,
) -> Result<Vec<Notification>, ()> {
    let result =
        controller::notifications(pooled_connection, auth_user, NotificationsParams::default());
    Ok(result)
}

pub fn get_account(pooled_connection: &PooledConnection, id: &str) -> Result<Account, ()> {
    controller::account(pooled_connection, id.parse::<i64>().unwrap()).ok_or(())
}

pub fn get_status(
    pooled_connection: &PooledConnection,
    id: String,
    actor: Option<&str>,
) -> Result<Status, ()> {
    controller::status_by_id(pooled_connection, id.parse::<i64>().unwrap(), actor).ok_or(())
}

pub fn get_status_context(
    pooled_connection: &PooledConnection,
    id: String,
    actor: Option<&str>,
) -> Result<Context, ()> {
    let id: i64 = id.parse().map_err(|_| ())?;
    controller::status_context(pooled_connection, id, actor).ok_or(())
}

pub fn home_timeline(
    pooled_connection: &PooledConnection,
    actor: &Actor,
) -> Result<Vec<Status>, ()> {
    controller::home_timeline(
        &pooled_connection,
        HomeTimeline {
            max_id: None,
            since_id: None,
            min_id: None,
            limit: Some(40),
        },
        actor,
    )
        .map_err(|_| ())
}

pub fn get_public_timeline(
    pooled_connection: &PooledConnection,
    local: bool,
    actor: Option<&Actor>,
) -> Result<Vec<Status>, ()> {
    controller::public_timeline(
        &pooled_connection,
        PublicTimeline {
            local: Some(local),
            only_media: None,
            max_id: None,
            since_id: None,
            min_id: None,
            limit: Some(40),
        },
        actor.as_ref().map(|a| a.actor_uri.as_str()),
    )
        .map_err(|_| ())
}

pub fn get_user_timeline(
    pooled_connection: &PooledConnection,
    id: String,
    actor: Option<&Actor>,
) -> Result<Vec<Status>, ()> {
    controller::account_statuses_by_id(
        pooled_connection,
        actor.as_ref().map(|a| a.actor_uri.as_str()),
        id.parse::<i64>().unwrap(),
        crate::mastodon_api::AccountTimeline {
            max_id: None,
            since_id: None,
            min_id: None,
            limit: Some(40),
            exclude_replies: Some(false),
            pinned: Some(false),
        },
    )
        .ok_or(())
}

// This approach is not optimal, as it skips the internal OAuth flow and should definitely be
// reworked. From a security perspective, this approach is safe, as the backend has no reason not
// to trust the internal front-end. On the other hand, this approach does not work if Raito-FE
// is run in standalone.
//
// TODO: Rework
pub fn login(pooled_connection: &PooledConnection, form: LoginForm) -> Option<String> {
    match actor::authorize(pooled_connection, &form.username, form.password) {
        Ok(true) => Some(oauth::token::create(&form.username).access_token),
        Ok(false) => None,
        Err(_) => None,
    }
}

pub fn post_status(
    pooled_connection: &PooledConnection,
    form: StatusForm,
    auth_user: &Actor,
) {
    controller::status_post(pooled_connection, form, auth_user);
}

// TODO: Rework
// (same as in line 129)
pub fn register(form: RegistrationForm) -> Result<oauth::token::Token, anyhow::Error> {
    if env::get_value("node.registrations_enabled") == "true" {
        controller::account_create(&form)
            .map_err(anyhow::Error::new)
    } else {
        Err(anyhow::anyhow!("Signup is disabled"))
    }
}

pub fn relationships_by_token(
    db: &PooledConnection,
    actor: &Actor,
    ids: Vec<i64>,
) -> Option<Vec<Relationship>> {
        Some(controller::relationships(db, actor, ids))
}

pub async fn search(
    db: &PooledConnection,
    query: String) -> Result<SearchResult, anyhow::Error> {
    let result = controller::search(db, &query, Some(true), None, None, None).await;
    Ok(result)
}