use crate::models::{attachment, media::save_media, actor::update_profile};
use crate::database::PooledConnection;
use crate::mastodon_api::{RegistrationForm, StatusForm};
use crate::raito_fe::{renderer, Configuration, LoginForm};
use crate::web::{
    multipart::{read_multipart_string, save_multipart_file},
    Redirect, RedirectError, Template,
};
use actix_multipart::Multipart;
use actix_session::Session;
use actix_web::{get, post, web, Either, HttpResponse, Responder};
use futures::TryStreamExt;
use serde::Deserialize;
use std::fmt::{self, Display};

#[get("/")]
pub async fn index(configuration: Configuration) -> impl Responder {
    renderer::index(configuration)
}

#[get("/about")]
pub async fn about(config: Configuration) -> Template {
    renderer::about(config)
}

#[get("/account/{id}")]
pub async fn account(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    id: web::Path<String>,
) -> Result<Template, NotFoundResponse> {
    renderer::account_by_local_id(&pooled_connection, configuration, id.into_inner())
        .ok_or(NotFoundResponse)
}

#[post("/account/{id}/follow")]
pub async fn account_follow(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    id: web::Path<i64>,
) -> Result<Redirect, NotFoundResponse> {
    renderer::account_follow(&pooled_connection, configuration, id.into_inner(), false)
        .ok_or(NotFoundResponse)
}

#[post("/account/{id}/unfollow")]
pub async fn account_unfollow(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    id: web::Path<i64>,
) -> Result<Redirect, NotFoundResponse> {
    renderer::account_follow(&pooled_connection, configuration, id.into_inner(), true)
        .ok_or(NotFoundResponse)
}

#[get("/actors/{handle}")]
pub async fn actor(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    handle: web::Path<String>,
) -> Result<Template, NotFoundResponse> {
    renderer::account_by_username(&pooled_connection, configuration, handle.into_inner())
        .ok_or(NotFoundResponse)
}

#[derive(Deserialize)]
pub struct DraftQuery {
    in_reply_to: Option<i64>,
}

#[get("/compose")]
pub async fn status_draft(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    query: web::Query<DraftQuery>,
) -> Result<Template, RedirectError> {
    renderer::compose(
        &pooled_connection,
        configuration,
        query.into_inner().in_reply_to,
    )
}

#[post("/compose")]
pub async fn status_compose(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    form: web::Form<StatusForm>,
) -> Redirect {
    renderer::compose_post(&pooled_connection, configuration, form.into_inner())
}

#[get("/login")]
pub async fn login(configuration: Configuration) -> impl Responder {
    renderer::login(configuration)
}

#[post("/login")]
pub async fn login_post(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    form: web::Form<LoginForm>,
    session: Session,
) -> Result<Redirect, RedirectError> {
    renderer::login_post(
        &pooled_connection,
        configuration,
        session,
        form.into_inner(),
    )
}

#[get("/timeline/home")]
pub async fn home_timeline(
    pooled_connection: PooledConnection,
    configuration: Configuration,
) -> Result<Template, RedirectError> {
    renderer::home_timeline(&pooled_connection, configuration)
}

#[get("/objects/{id}")]
pub async fn object(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    id: web::Path<String>,
) -> Option<Template> {
    renderer::conversation_by_uri(&pooled_connection, configuration, id.into_inner())
}

#[post("/register")]
pub async fn register(
    configuration: Configuration,
    session: Session,
    form: web::Form<RegistrationForm>,
) -> Result<Redirect, RedirectError> {
    renderer::register_post(configuration, session, form.into_inner())
}

#[get("/settings")]
pub async fn settings(configuration: Configuration) -> impl Responder {
    renderer::settings(configuration)
}

#[get("/settings/profile")]
pub async fn settings_profile(config: Configuration) -> Either<Template, Redirect> {
    renderer::profile_settings(config)
}

#[post("/settings/profile")]
pub async fn settings_profile_post(
    db: PooledConnection,
    config: Configuration,
    mut payload: Multipart,
) -> Result<Template, RedirectError> {
    let auth_account = config.account.as_ref().ok_or_else(|| Redirect::to("/"))?;

    let mut display_name: Option<String> = None;
    let mut bio: Option<String> = None;
    let mut avatar: Option<String> = None;

    // TODO: need to tell user that data is invalid somehow

    while let Ok(Some(mut field)) = payload.try_next().await {
        let disposition = field.content_disposition().unwrap();
        let name = disposition.get_name().unwrap();
        match name {
            "display_name" => display_name = read_multipart_string(&mut field).await,
            "bio" => bio = read_multipart_string(&mut field).await,
            "avatar" => {
                let auth_actor = config.actor.as_ref().unwrap();
                avatar = save_multipart_file(&mut field)
                    .await
                    .ok()
                    .map(|saved_file| {
                        save_media(
                            &db,
                            auth_actor,
                            Some(&saved_file.content_type),
                            saved_file.passed_name.as_ref().map(String::as_str),
                            &saved_file.assigned_name,
                        )
                            .unwrap();
                        attachment::file_url(&saved_file.assigned_name)
                    })
            }
            _ => {}
        }
    }
    let name = display_name.as_ref().map(String::as_str).unwrap_or("");
    let summary = bio.as_ref().map(String::as_str).unwrap_or("");
    let icon_ref = avatar
        .as_ref()
        .map(String::as_str)
        .or_else(|| Some(auth_account.avatar.as_str()));
    let account_id: i64 = auth_account.id.parse().unwrap();

    let new_actor = update_profile(&db, account_id, name, summary, icon_ref);
    let new_account = crate::mastodon_api::Account::from_actor(&db, new_actor.clone(), false);
    crate::activitypub::actions::distribute_profile_update(db, new_actor);
    Ok(renderer::profile_settings_post(config, &new_account))
}

#[get("/status/{id}")]
pub async fn view_status(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    id: web::Path<String>,
) -> impl Responder {
    renderer::conversation(&pooled_connection, configuration, id.into_inner())
        .ok_or(NotFoundResponse)
}

#[get("/timeline/global")]
pub async fn global_timeline(
    pooled_connection: PooledConnection,
    configuration: Configuration,
) -> Template {
    renderer::public_timeline(&pooled_connection, configuration, false)
}

#[get("/timeline/public")]
pub async fn public_timeline(
    pooled_connection: PooledConnection,
    configuration: Configuration,
) -> Template {
    renderer::public_timeline(&pooled_connection, configuration, true)
}

#[derive(Deserialize)]
pub struct SearchQuery {
    q: Option<String>,
}

#[get("/search")]
pub async fn search(
    pooled_connection: PooledConnection,
    configuration: Configuration,
    query: web::Query<SearchQuery>,
) -> Either<Template, Redirect> {
    let q = query.0.q;
    renderer::search(&pooled_connection, configuration, q.unwrap_or(String::from(""))).await
}

#[derive(Debug)]
pub struct NotFoundResponse;

impl Display for NotFoundResponse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not found")
    }
}

impl actix_web::error::ResponseError for NotFoundResponse {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::NotFound().body("Oopsie, the thing is not there")
    }
}
