ALTER TABLE actors
  ADD url VARCHAR;

CREATE UNIQUE INDEX actors_url ON actors(url);
