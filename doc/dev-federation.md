Mastodon patches

Procfile.dev adjust port
lib/webfinger https -> http

---------

To test federation with Mastodon locally we will use two virtual machines. You can use containers instead but Mastodon has kinda-working dev preset for Vagrant so we might as well start there.

Let's modify Mastodons' Vagrantfile.
I could not build nokogiri as-is so I had to do this:

in provision:

```
+bundle config build.nokogiri --use-system-libraries
```

Then let's define network. Also in provision:

```
+sudo sh -c 'echo "192.168.42.43 kibou.local" >> /etc/hosts'
```

This let's us use domain names and not ports which can lead to problems

Then in the networking setup:
```
-    config.vm.network :private_network, ip: "192.168.42.42", nictype: "virtio"
+    config.vm.network :private_network, ip: "192.168.42.42", nictype: "virtio", virtbox__itnet: true
```

We will put interface on the internal network so that two virtual machines can talk to each other.

```
-  if config.vm.networks.any? { |type, options| type == :private_network }
-    config.vm.synced_folder ".", "/vagrant", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'actimeo=1']
-  else
-    config.vm.synced_folder ".", "/vagrant"
-  end
+  # if config.vm.networks.any? { |type, options| type == :private_network }
+  #   config.vm.synced_folder ".", "/vagrant", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'actimeo=1']
+  # else
+  config.vm.synced_folder ".", "/vagrant"
+  # end
```

Even though we will use virtual network, we won't access files over it so we don't need all this nfs fun.


The second problem to overcome was that I couldn't build cld3 (this is language
detection). I didn't really want to do this anyway:

in Gemfile:

```
-gem 'cld3', '~> 3.3.0'
```

because we remove the library, we need to stop trying to use it:

in app/lib/language_detector.rb:

```
   WORDS_THRESHOLD        = 4
   RELIABLE_CHARACTERS_RE = /[\p{Hebrew}\p{Arabic}\p{Syriac}\p{Thaana}\p{Nko}\p{Han}\p{Katakana}\p{Hiragana}\p{Hangul}]+/m
 
-  def initialize
-    @identifier = CLD3::NNetLanguageIdentifier.new(1, 2048)
-  end
 
   def detect(text, account)
     input_text = prepare_text(text)
 
     return if input_text.blank?
-
-    detect_language_code(input_text) || default_locale(account)
+    default_locale(account)
   end
 
   def language_names
-    @language_names = CLD3::TaskContextParams::LANGUAGE_NAMES.map { |name| iso6391(name.to_s).to_sym }.uniq
+    @language_names = []
   end
 
   private
```

The last thing was to disable SSL requirement for Webfinger requests because we don't want to jump into that hole.

In app/services/resolve_account_service.rb:

```
-    @webfinger                           = Goldfinger.finger("acct:#{uri}")
+    @webfinger                           = Goldfinger.finger("acct:#{uri}", {ssl: false})
```

This is it for Mastodon. Go on and run `vagrant up``. Oh, you need virtualbox
for that. And Vagrant. And their versions should be compatible (so you may be better off
downloading Vagrant from website if it complains). Now it should be able to
federate with kibou.local.


For kibou there's a separate Vagrantfile. It is intended that you compile it on
your machine and run in a box. There's a problem with password and Postgres, I set password manually with

```
$ vagrant ssh
$ psql --dbname=kibou.dev
$ ALTER USER vagrant WITH PASSWORD 'vagrant';
```

Before running vagrant you need to write this into `.env`:

```
DATABASE_URL=postgres://vagrant:vagrant@localhost/kibou_dev
```

This is so that Diesel can run migrations (it is done automatically during Vagrant provision).


Another thing you might want to do is to add this to `/etc/hosts` on your machine:

```
192.168.42.43  kibou.local 
192.168.42.42  mastodon.local
```

This way you can open these URLs in browser.
