FROM ubuntu:20.04 as build-dep

# Use bash for the shell
SHELL ["/bin/bash", "-c"]

# Install Node v12 (LTS)
ENV NODE_VER="12.21.0"
RUN ARCH= && \
    dpkgArch="$(dpkg --print-architecture)" && \
  case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac && \
    echo "Etc/UTC" > /etc/localtime && \
	apt-get update && \
	echo "Etc/UTC" > /etc/localtime && \
	apt-get install -y --no-install-recommends \
        wget \
        gnupg2 \
        software-properties-common \
        git-core \
        g++ \
        libpq-dev \
        libxml2-dev \
        libxslt1-dev \
        imagemagick \
        redis-server \
        redis-tools \
        protobuf-compiler \
        libicu-dev \
        libidn11-dev \
        libprotobuf-dev \
        libreadline-dev \
        libidn11-dev \
        libldap2-dev \
        curl \
        tini \
        shared-mime-info && \
	cd ~ && \
	curl -sS -O https://nodejs.org/download/release/v$NODE_VER/node-v$NODE_VER-linux-$ARCH.tar.gz && \
	tar xf node-v$NODE_VER-linux-$ARCH.tar.gz && \
	rm node-v$NODE_VER-linux-$ARCH.tar.gz && \
	mv node-v$NODE_VER-linux-$ARCH /opt/node


# Install Ruby
ENV RUBY_VER="2.7.2"
RUN apt-get update && apt-get install -y --no-install-recommends \
    bison \
    build-essential \
    libffi-dev \
    libgdbm-dev \
    libjemalloc-dev \
    libncurses5-dev \
    libreadline-dev \
    libssl-dev \
    libyaml-dev \
    zlib1g-dev && \
  cd ~ && \
  wget https://cache.ruby-lang.org/pub/ruby/${RUBY_VER%.*}/ruby-$RUBY_VER.tar.gz && \
  tar xf ruby-$RUBY_VER.tar.gz && rm ruby-$RUBY_VER.tar.gz && \
  cd ruby-$RUBY_VER && \
  ./configure --prefix=/opt/ruby \
    --with-jemalloc \
    --with-shared \
    --disable-install-doc && \
  make -j"$(nproc)" > /dev/null && \
  make install && \
  rm -rf /root/ruby-$RUBY_VER

ENV PATH="${PATH}:/opt/ruby/bin:/opt/node/bin"

WORKDIR /opt

# Clone repo
RUN git clone https://github.com/tootsuite/mastodon

WORKDIR /opt/mastodon

RUN npm install -g yarn && \
    gem install bundler foreman --no-document

ENV GEM_HOME=/opt/mastodon/.gems

RUN bundle install && \
   	yarn install --pure-lockfile

# Add more PATHs to the PATH
ENV PATH="${PATH}:/opt/mastodon/bin"

# Install mastodon runtime deps???
#RUN ln -s /opt/mastodon /mastodon

# Copy over mastodon source, and dependencies from building, and set permissions
#COPY --chown=mastodon:mastodon . /opt/mastodon
#COPY --from=build-dep --chown=mastodon:mastodon /opt/mastodon /opt/mastodon

# Run mastodon services in prod mode
ENV RAILS_ENV="development"
ENV NODE_ENV="development"
ENV BIND="0.0.0.0"

# Set the work dir and the container entry point
WORKDIR /opt/mastodon
ENTRYPOINT ["/usr/bin/tini", "--"]
EXPOSE 3000 4000