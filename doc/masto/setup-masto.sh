echo 'Hello, I will build mastodon pod for you without touching your system.'

echo 'Building mastodon image. This is the longest step.'
podman build -f Dockerfile --format docker --tag masto

echo 'Mastodon image build, now will just set up other containers'
# Create pod and containers for the pod. They will be on their own network.
# We use a trick here.
# We put three containers in a pod. This basically makes them one container, using same ports
# Then we connect the *pod* to the network. When we do that podman automatically creates a DNS
# alias for us. As pod is counted as a single container the actual alias which will be creates is
# pod name, not container name. That's why in this case pod name is important, not container name.
podman pod create --name masto --network cni-podman0 -p 8080:80 -p 3000:3000
podman create --name redis_masto --pod masto docker.io/library/redis:6.0-alpine
podman create --name masto_container --pod masto --env-file env -it masto bash
podman create --name postgres_masto -e POSTGRES_DB=masto_dev -e POSTGRES_USER=masto -e POSTGRES_PASSWORD=masto --pod masto docker.io/library/postgres:12

# Start the pod
echo 'Starting the pod...'
podman pod start masto

echo 'Good job! Now you can `podman exec -it masto_cont bash` and inside `bundle exec rails db:setup` and `foreman start`.'
echo 'You can stop container with `podman pod stop masto` and start again with `podman pod start masto`'