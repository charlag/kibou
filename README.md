# Kibou

## What is Kibou?
Kibou is a lightweight federated social networking server based on open protocols. It is not meant
for scaling infinitely but rather aims to scale down so that you can host it cheaply and easily.
Kibou is using [ActivityPub](https://activitypub.rocks) protocol.

Kibou implements [Mastodon's REST API](https://docs.joinmastodon.org/api), this means
that all applications for [Mastodon](https://joinmastodon.org) should also work with Kibou.

Kibou ships with it's own user interface called Raito-FE. In it's standard configuration it's
completely based on static components and does not use any JavaScript. Although dynamic
components (such as automatically refreshing timelines and dynamic routing) can optionally be
enabled. A `minimal mode` can also be enabled in it's settings which reduces network traffic and
only renders Raito-FE's core components.

![Kibou UI screenshot](https://git.cybre.club/attachments/ed7cacce-058e-47d3-8544-7584516a55d9)

## Try it out
* [List of Kibou nodes](https://fediverse.network/kibou)
* Setup a Kibou server: updated guide is TBA

## Federation with other software
Federation is known to work with [Mastodon](https://joinmastodon.org) and
[Pleroma](https://pleroma.social), which are also the main projects being tested against. But
federation with other software should work as well.

## Building
**Requirements**
 * rustc 1.52.1 (stable)
 * cargo 1.25.0
 * OpenSSL dev headers, see https://docs.rs/openssl/0.10.34/openssl/
 * Postgres client headers (libpq-devel or the like), see https://diesel.rs/
 * Postgress instance. For development it's easy to spin up a container with e.g. podman:

`podman run --name postgres -e POSTGRESQL_DATABASE=kibou_dev -e POSTGRESQL_USER=kibou -e POSTGRESQL_PASSWORD=kibou -p 5432:5432 postgresql`


**For development**

```sh
cargo install diesel_cli --no-default-features --features "postgres"
echo DATABASE_URL=postgres://kibou:kibou@localhost/kibou_dev > .env
diesel migration run
cargo run
```

Then server should start serving files on http://localhost:9000